<?php
session_start();
if (!isset($_SESSION['griapp_user'])) {
  header('Location: login');
}

require_once "dist/libs/conexion.php";

$ls_foros = '';
$ls_tareas = '';
$ls_cuestionarios = '';

$detalles_matricula = $db
  ->where('Id_md', $_REQUEST['cu'])
  ->objectBuilder()->get('matriculas_detalles');

if ($db->count > 0) {
  $actividades = $db
    ->where('Id_gr', $detalles_matricula[0]->grupo_md)
    ->objectBuilder()->get('modulos_actividades');

  if ($db->count > 0) {
    foreach ($actividades as $actividad) {
      $detalles = $db
        ->where('Id_ma', $actividad->Id_ma)
        ->objectBuilder()->get('modulos_actividades_detalle');

      if ($db->count > 0) {
        foreach ($detalles as $detalle) {
          $realizado = '';

          if ($detalle->tipo_mad == 1) {
            $tareas = $db
              ->where('Id_ma', $_SESSION['griapp_IDtipo'])
              ->where('Id_mad', $detalle->Id_mad)
              ->objectBuilder()->get('modulo_actividades_tareas_calificaciones');

            if ($db->count > 0) {
              $realizado = 'icon-checkmark';
            }

            $ls_tareas .= '<li><a href="estudiante-tarea?tr=' . $detalle->Id_mad . '""> <i class="' . $realizado . ' I-verde"> </i> ' . $detalle->nombre_mad . '</a></li>';
          }

          if ($detalle->tipo_mad == 3) {
            $foros = $db
              ->where('Id_ma', $actividad->Id_ma)
              ->where('Id_mad', $detalle->Id_mad)
              ->objectBuilder()->get('modulo_actividades_foros_calificaciones');

            if ($db->count > 0) {
              $realizado = 'icon-checkmark';
            }

            $ls_foros .= '<li><a href="estudiante-foro?fr=' . $detalle->Id_mad . '"> <i class="' . $realizado . ' I-verde"> </i> ' . $detalle->nombre_mad . '</a></li>';
          }

          if ($detalle->tipo_mad == 5) {

            $cuestionario = $db
              ->where('Id_ma', $actividad->Id_ma)
              ->where('Id_mad', $detalle->Id_mad)
              ->objectBuilder()->get('modulo_actividades_cuestionario_calificaciones');

            if ($db->count > 0) {
              $realizado = 'icon-checkmark';
            }

            $ls_cuestionarios .= '<li><a href="estudiante-cuestionario?cu=' . $detalle->Id_mad . '"> <i class="' . $realizado . ' I-verde"> </i> ' . $detalle->nombre_mad . '</a></li>';
          }
        }
      }
    }
  }
}

?>
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Dashboard Estudiante</title>
  <link rel="stylesheet" type="text/css" href="dist/css/fonts.css">
  <?php include("dist/libs/cssvariable/css-variables.php") ?>
  <link rel="stylesheet" type="text/css" href="dist/css/materialize.css">
  <link rel="stylesheet" type="text/css" href="dist/css/load.css">
  <link rel="stylesheet" type="text/css" href="dist/css/noty.css">
  <link rel="stylesheet" type="text/css" href="dist/css/relax.css">
  <link rel="stylesheet" type="text/css" href="dist/css/jquery.modal.css" />
  <link rel="stylesheet" type="text/css" href="dist/css/paginacion.css">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>

<body>
  <header>
    <div class="Admin-top">
      <?php include("dist/libs/includes-seccion/top-header.php") ?>
    </div>
  </header>
  <section>
    <div class="Contenedor-principal">
      <div class="Contenedor-principal-izq Contenedor-principal-izq-min">
        <?php include("dist/libs/includes-seccion/menu-izq-estudiantes.php"); ?>
      </div>
      <div class="Contenedor-principal-der">
        <div class="Contenedor-principal-der-int">

          <div class="Contenedor-dash">
            <div class="Contenedor-principal-titulo">
              <div class="Contenedor-principal-titulo-sec">
                <input type="hidden" id="idcurso" value="<?php echo $_REQUEST['cu'] ?>">
                <h2 class="Titulo-seccion">TRABAJO SEGURO EN ALTURAS NIVEL AVANZADO</h2><br>
              </div>
            </div>
            <div class="Contenedor-dash-int">
              <div class="Contenedor-dash-int-contenido">
                <div class="Contenedor-dash-interno">
                  <div class="Contenedor-dash-cursol1">
                    <div id="Listado-contenidos"></div>
                    <div class="listado-loader" style="text-align: center; margin-top: 50px;">
                      <div class="preloader-wrapper big active">
                        <div class="spinner-layer">
                          <div class="circle-clipper left">
                            <div class="circle"></div>
                          </div>
                          <div class="gap-patch">
                            <div class="circle"></div>
                          </div>
                          <div class="circle-clipper right">
                            <div class="circle"></div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="listado-paginacion">
                      <ul class="pagination">
                      </ul>
                    </div>
                  </div>

                  <div class="Contenedor-dash-cursol2">

                    <div class="Contenedor-bloque-actividades">
                      <div class="Contenedor-bloque-actividades-sec">
                        <div class="Contenedor-bloque-actividades-sup">
                          <i class="material-icons">record_voice_over</i><span>FOROS</span>
                        </div>
                        <div class="Contenedor-bloque-actividades-inf">
                          <ul>
                            <!-- <li><a href=""> <i class="icon-checkmark I-verde"> </i> Aca va el nombre del foro por ejemplo donde el nombre donde sea grande</a></li>
                            <li><a href=""><i class="icon-cross I-rojo"> </i> Este es el nombre de otro foro</a></li> -->
                            <?php echo $ls_foros; ?>
                          </ul>
                        </div>
                      </div>
                    </div>

                    <div class="Contenedor-bloque-actividades">
                      <div class="Contenedor-bloque-actividades-sec">
                        <div class="Contenedor-bloque-actividades-sup">
                          <i class="material-icons">import_contacts</i><span>ACTIVIDADES</span>
                        </div>
                        <div class="Contenedor-bloque-actividades-inf">
                          <ul>
                            <?php echo $ls_tareas; ?>
                          </ul>
                        </div>
                      </div>
                    </div>
                    <div class="Contenedor-bloque-actividades">
                      <div class="Contenedor-bloque-actividades-sec">
                        <div class="Contenedor-bloque-actividades-sup">
                          <i class="material-icons">format_list_numbered</i><span>CUESTIONARIOS</span>
                        </div>
                        <div class="Contenedor-bloque-actividades-inf">
                          <ul>
                            <?php echo $ls_cuestionarios; ?>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <script src="dist/js/jquery-1.11.1.min.js"></script>
  <script src="dist/js/noty.min.js"></script>
  <script src="dist/js/inicializar.js"></script>
  <script src="dist/js/materialize.min.js"></script>
  <script src="dist/js/noty.min.js"></script>
  <script src="dist/js/jquery.modal.min.js"></script>
  <script src="dist/js/menu-slide.js?v<?php echo date('YmdHis') ?>"></script>
  <script src="dist/js/estudiante-curso.js?v<?php echo date('YmdHis') ?>"></script>
</body>

</html>
