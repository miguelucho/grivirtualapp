<?php

require_once "conexion.php";
$data = $_REQUEST['grupo'];
$informacion = array();

switch ($data['opc']) {
    case 'Grupo-nuevo':
        $datos = array(
            'Id_pr' => $data['programa'],
            'Id_cu' => $data['curso'],
            'Id_do' => $data['docente'],
            'codigo_gr' => '',
            'estudiantes_gr' => $data['estudiantes'],
            'inicio_gr' => $data['inicio'],
            'fin_gr' => $data['fin'],
            'estado_gr' => $data['estado'],
        );

        $nuevo = $db
            ->insert('grupos', $datos);

        if ($nuevo) {
            $codigo = substr(date('Y'), -2) . '-' . $data['programa'] . '-' . $data['curso'] . '-' . $nuevo;

            $db
                ->where('Id_gr', $nuevo)
                ->update('grupos', ['codigo_gr' => $codigo]);

            $informacion['status'] = true;
            $informacion['msg'] = 'Grupo creado.';
        } else {
            $informacion['status'] = false;
            $informacion['msg'] = 'El grupo no se pudo crear.';
        }

        echo json_encode($informacion);
        break;
    case 'Asignados-listado':
        session_start();

        require_once 'Paginacion.php';
        $page = $data['pagina'];
        $results_pg = 50;
        $adjacent = 2;

        ($data['programa'] == '' ? $data['programa'] = '%' : '');
        ($data['curso'] == '' ? $data['curso'] = '%' : '');
        ($data['codigo'] == '' ? $data['codigo'] = '%' : '');


        $grupos = $db
            ->where('Id_pr', $data['programa'], 'LIKE')
            ->where('Id_cu', $data['curso'], 'LIKE')
            ->where('codigo_gr', $data['codigo'], 'LIKE')
            ->where('Id_do', $_SESSION['griapp_IDtipo'])
            ->where('estado_gr', 1)
            ->objectBuilder()->get('grupos');

        // print_r($db->getLastQuery());

        $numpgs = ceil($db->count / $results_pg);

        if ($numpgs >= 1) {
            $content = '';
            $db->pageLimit = $results_pg;

            $grupos = $db
                ->where('Id_pr', $data['programa'], 'LIKE')
                ->where('Id_cu', $data['curso'], 'LIKE')
                ->where('codigo_gr', $data['codigo'], 'LIKE')
                ->where('Id_do', $_SESSION['griapp_IDtipo'])
                ->where('estado_gr', 1)
                ->orderBy('Id_gr', 'DESC')
                ->objectBuilder()->paginate('grupos', $page);


            foreach ($grupos as $grupo) {
                $nm_programa = '';
                $nm_curso = '';
                $ls_modulos = '';

                $programas = $db
                    ->where('Id_pr', $grupo->Id_pr)
                    ->objectBuilder()->get('programas');

                if ($db->count > 0) {
                    $nm_programa = $programas[0]->nombre_pr;
                }

                $cursos = $db
                    ->where('Id_cu', $grupo->Id_cu)
                    ->objectBuilder()->get('cursos');

                if ($db->count > 0) {
                    $nm_curso = $cursos[0]->nombre_cu;
                }

                $modulos = $db
                    ->where('Id_pr', $grupo->Id_pr)
                    ->where('Id_cu', $grupo->Id_cu)
                    ->objectBuilder()->get('modulos');

                if ($db->count > 0) {
                    foreach ($modulos as $modulo) {
                        $ls_modulos .= '<span>' . $modulo->nombre_mo . '</span><br>';
                    }
                }

                $inicio = date_format(date_create($grupo->inicio_gr), 'd-m-Y');
                $fin = date_format(date_create($grupo->fin_gr), 'd-m-Y');

                $content .= '<tr>
                                <td>
                                    <span>' . $nm_programa . '</span><br>
                                    <span>' . $nm_curso . '</span>
                                </td>
                                <td>' . $ls_modulos . '</td>
                                <td style="white-space: nowrap;">' . $grupo->codigo_gr . '</td>
                                <td class="center">' . $grupo->estudiantes_gr . '</td>
                                <td class="center">0</td>
                                <td>' . $inicio . '</td>
                                <td>' . $fin . '</td>
                                <td><a href="administrar-lista-contenidos?gr=' . $grupo->Id_gr . '" data-target="editar" class="Btn-ver Btn-table-verde"><i class="icon-pencil"></i>Administrar</a></td>
                                <td><a href="administrar-modulos-interno?gr=' . $grupo->Id_gr . '" class="Btn-ver Btn-table-verde"><i class="icon-pencil"></i>Administrar</a></td>
                            </tr>';
            }

            $informacion['list'] = $content;
            $pagconfig = array(
                'pagina' => $page,
                'totalrows' => $db->totalPages,
                'ultima_pag' => $numpgs,
                'resultados_pag' => $results_pg,
                'adyacentes' => $adjacent
            );
            $paginate = new Paginacion($pagconfig);
            $informacion['pagination'] = $paginate->crearlinks();
        } else {
            $informacion['list'] = '<tr>
                                <td colspan="8">No hay registros</td>
                            </tr>';
            $informacion['pagination'] = '';
        }

        echo json_encode($informacion);
        break;
    case 'Grupo-info':
        $idgrupo = explode('-', $data['idgrupo']);

        $grupos = $db
            ->where('Id_gr', $idgrupo[1])
            ->objectBuilder()->get('grupos');

        if ($db->count > 0) {
            $informacion['info'] = $grupos[0];
            $informacion['status'] = true;
        } else {
            $informacion['status'] = false;
            $informacion['msg'] = 'El grupo no existe.';
        }

        echo json_encode($informacion);
        break;
    case 'Grupo-editar':
        $idgrupo = explode('-', $data['idgrupo']);
        $codigo = substr(date('Y'), -2) . '-' . $data['programa'] . '-' . $data['curso'] . '-' . $idgrupo[1];

        $datos = array(
            'Id_pr' => $data['programa'],
            'Id_cu' => $data['curso'],
            'Id_do' => $data['docente'],
            'codigo_gr' => $codigo,
            'estudiantes_gr' => $data['estudiantes'],
            'inicio_gr' => $data['inicio'],
            'fin_gr' => $data['fin'],
            'estado_gr' => $data['estado'],
        );

        $nuevo = $db
            ->where('Id_gr', $idgrupo[1])
            ->update('grupos', $datos);

        if ($nuevo) {
            $informacion['status'] = true;
            $informacion['msg'] = 'Grupo editado.';
        } else {
            $informacion['status'] = false;
            $informacion['msg'] = 'El grupo no se pudo editar.';
        }

        echo json_encode($informacion);
        break;
    case 'Asignados-calificaciones':
        session_start();

        require_once 'Paginacion.php';
        $page = $data['pagina'];
        $results_pg = 50;
        $adjacent = 2;

        ($data['programa'] == '' ? $data['programa'] = '%' : '');
        ($data['curso'] == '' ? $data['curso'] = '%' : '');
        ($data['codigo'] == '' ? $data['codigo'] = '%' : '');


        $grupos = $db
            ->where('Id_pr', $data['programa'], 'LIKE')
            ->where('Id_cu', $data['curso'], 'LIKE')
            ->where('codigo_gr', $data['codigo'], 'LIKE')
            ->where('Id_do', $_SESSION['griapp_IDtipo'])
            ->where('estado_gr', 1)
            ->objectBuilder()->get('grupos');

        $numpgs = ceil($db->count / $results_pg);

        if ($numpgs >= 1) {
            $content = '';
            $db->pageLimit = $results_pg;

            $grupos = $db
                ->where('Id_pr', $data['programa'], 'LIKE')
                ->where('Id_cu', $data['curso'], 'LIKE')
                ->where('codigo_gr', $data['codigo'], 'LIKE')
                ->where('Id_do', $_SESSION['griapp_IDtipo'])
                ->where('estado_gr', 1)
                ->orderBy('Id_gr', 'DESC')
                ->objectBuilder()->paginate('grupos', $page);


            foreach ($grupos as $grupo) {
                $nm_programa = '';
                $nm_curso = '';
                $ls_modulos = '';

                $programas = $db
                    ->where('Id_pr', $grupo->Id_pr)
                    ->objectBuilder()->get('programas');

                if ($db->count > 0) {
                    $nm_programa = $programas[0]->nombre_pr;
                }

                $cursos = $db
                    ->where('Id_cu', $grupo->Id_cu)
                    ->objectBuilder()->get('cursos');

                if ($db->count > 0) {
                    $nm_curso = $cursos[0]->nombre_cu;
                }

                $modulos = $db
                    ->where('Id_pr', $grupo->Id_pr)
                    ->where('Id_cu', $grupo->Id_cu)
                    ->objectBuilder()->get('modulos');

                if ($db->count > 0) {
                    foreach ($modulos as $modulo) {
                        $ls_modulos .= '<span>' . $modulo->nombre_mo . '</span><br>';
                    }
                }

                $inicio = date_format(date_create($grupo->inicio_gr), 'd-m-Y');
                $fin = date_format(date_create($grupo->fin_gr), 'd-m-Y');

                $content .= '<tr>
                                <td>
                                    <span>' . $nm_programa . '</span><br>
                                    <span>' . $nm_curso . '</span>
                                </td>
                                <td>' . $ls_modulos . '</td>
                                <td style="white-space: nowrap;">' . $grupo->codigo_gr . '</td>
                                <td class="center">' . $grupo->estudiantes_gr . '</td>
                                <td>' . $fin . '</td>
                                <td><a href="administrar-calificar?gr=' . $grupo->Id_gr . '" data-target="editar" class="Btn-ver Btn-table-verde"><i class="icon-list"></i>Calificar</a></td>
                            </tr>';
            }

            $informacion['list'] = $content;
            $pagconfig = array(
                'pagina' => $page,
                'totalrows' => $db->totalPages,
                'ultima_pag' => $numpgs,
                'resultados_pag' => $results_pg,
                'adyacentes' => $adjacent
            );
            $paginate = new Paginacion($pagconfig);
            $informacion['pagination'] = $paginate->crearlinks();
        } else {
            $informacion['list'] = '<tr>
                                <td colspan="8">No hay registros</td>
                            </tr>';
            $informacion['pagination'] = '';
        }

        echo json_encode($informacion);
        break;
    case 'Estudiante-calificaciones':
        session_start();

        require_once 'Paginacion.php';
        $page = $data['pagina'];
        $results_pg = 50;
        $adjacent = 2;

        $matriculas = $db
            ->where('Id_ma', $_SESSION['griapp_IDtipo'])
            ->objectBuilder()->get('matriculas_detalles');

        $numpgs = ceil($db->count / $results_pg);

        if ($numpgs >= 1) {
            $content = '';
            $db->pageLimit = $results_pg;

            $matriculas = $db
                ->where('Id_ma', $_SESSION['griapp_IDtipo'])
                ->orderBy('Id_md', 'DESC')
                ->objectBuilder()->paginate('matriculas_detalles', $page);


            foreach ($matriculas as $matricula) {
                $nm_programa = '';
                $nm_curso = '';
                $ls_modulos = '';
                $cod_grupo = '';
                $Id_grupo = '';

                $detalle = $db
                    ->where('Id_ma', $matricula->Id_ma)
                    ->objectBuilder()->get('matriculas');

                if ($db->count > 0) {
                    $programas = $db
                        ->where('Id_pr', $detalle[0]->programa_ma)
                        ->objectBuilder()->get('programas');

                    if ($db->count > 0) {
                        $nm_programa = $programas[0]->nombre_pr;
                    }

                    $cursos = $db
                        ->where('Id_cu', $matricula->curso_md)
                        ->objectBuilder()->get('cursos');

                    if ($db->count > 0) {
                        $nm_curso = $cursos[0]->nombre_cu;
                    }

                    $modulos = $db
                        ->where('Id_pr', $detalle[0]->programa_ma)
                        ->where('Id_cu', $matricula->curso_md)
                        ->objectBuilder()->get('modulos');

                    if ($db->count > 0) {
                        foreach ($modulos as $modulo) {
                            $ls_modulos .= '<span>' . $modulo->nombre_mo . '</span><br>';
                        }
                    }

                    $grupos = $db
                        ->where('Id_gr', $matricula->grupo_md)
                        ->objectBuilder()->get('grupos');

                    if ($db->count > 0) {
                        $cod_grupo = $grupos[0]->codigo_gr;
                        $Id_grupo =  $grupos[0]->Id_gr;
                    }
                }

                $content .= '<tr>
                                <td>
                                    <span>' . $nm_programa . '</span><br>
                                    <span>' . $nm_curso . '</span>
                                </td>
                                <td>' . $ls_modulos . '</td>
                                <td style="white-space: nowrap;">' . $cod_grupo . '</td>
                                <td><a href="estudiante-modulos-calificaciones?gr=' . $Id_grupo . '" data-target="editar" class="Btn-ver Btn-table-verde"><i class="icon-list"></i>Ver</a></td>
                            </tr>';
            }

            $informacion['list'] = $content;
            $pagconfig = array(
                'pagina' => $page,
                'totalrows' => $db->totalPages,
                'ultima_pag' => $numpgs,
                'resultados_pag' => $results_pg,
                'adyacentes' => $adjacent
            );
            $paginate = new Paginacion($pagconfig);
            $informacion['pagination'] = $paginate->crearlinks();
        } else {
            $informacion['list'] = '<tr>
                                <td colspan="8">No hay registros</td>
                            </tr>';
            $informacion['pagination'] = '';
        }

        echo json_encode($informacion);
        break;
}
