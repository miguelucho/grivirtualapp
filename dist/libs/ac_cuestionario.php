<?php

require_once "conexion.php";
$data = $_REQUEST['cuestionario'];
$informacion = array();

$folder = '../assets/documentos_tareas';

switch ($data['opc']) {
    case 'Administrar':
        $comprobar = $db
            ->where('Id_mad', $data['idtcuestionario'])
            ->objectBuilder()->get('modulo_actividades_cuestionario');

        if ($db->count == 0) {

            $datos = array(
                'Id_mad' => $data['idtcuestionario'],
                'fecha_inicio_mac' => $data['apertura_fecha'],
                'hora_inicio_mac' => date('H:i:s', strtotime($data['apertura_hora'])),
                'fecha_fin_mac' => $data['cierre_fecha'],
                'hora_fin_mac' => date('H:i:s', strtotime($data['cierre_hora'])),
            );

            $nuevo = $db
                ->insert('modulo_actividades_cuestionario', $datos);

            if ($nuevo) {
                foreach ($data['pregunta'] as $b => $c) {
                    $datos = array(
                        'Id_mac' => $nuevo,
                        'pregunta_macp' => $data['pregunta'][$b],
                        'tipo_macp' => $data['pregunta_tipo'][$b],
                        'valor_macp' => $data['pregunta_valor'][$b],
                    );

                    $pregunta = $db
                        ->insert('modulo_actividades_cuestionario_preguntas', $datos);

                    if ($pregunta) {
                        for ($i = 0; $i < count($data['respuesta'][$b]); $i++) {
                            $datos = array(
                                'Id_macp' => $pregunta,
                                'respuesta_macr' => $data['respuesta'][$b][$i],
                                'tipo_macr' => $data['respuesta_tipo'][$b][$i],
                            );

                            if (isset($data['palabra'][$b][$i])) {
                                $datos['palabra_macr'] = $data['palabra'][$b][$i];
                            }

                            $respuesta = $db
                                ->insert('modulo_actividades_cuestionario_respuestas', $datos);
                        }
                    }
                }


                $informacion['status'] = true;
                $informacion['msg'] = 'Actividad guardada.';
            } else {
                $informacion['status'] = false;
                $informacion['msg'] = 'La actividad no se pudo guardar';
            }
        } else {
            $datos = array(
                'fecha_inicio_mac' => $data['apertura_fecha'],
                'hora_inicio_mac' => date('H:i:s', strtotime($data['apertura_hora'])),
                'fecha_fin_mac' => $data['cierre_fecha'],
                'hora_fin_mac' => date('H:i:s', strtotime($data['cierre_hora'])),
            );

            $editar = $db
                ->where('Id_mad', $data['idtcuestionario'])
                ->update('modulo_actividades_cuestionario', $datos);

            if ($editar) {
                if (isset($data['pregunta'])) {
                    foreach ($data['pregunta'] as $b => $c) {
                        if (isset($data['pregunta-' . $b])) {
                            $pregunta = $db
                                ->where('Id_macp', $data['pregunta-' . $b])
                                ->objectBuilder()->get('modulo_actividades_cuestionario_preguntas');

                            if ($db->count > 0) {
                                $datos = array(
                                    'pregunta_macp' => $data['pregunta'][$b],
                                    'tipo_macp' => $data['pregunta_tipo'][$b],
                                    'valor_macp' => $data['pregunta_valor'][$b],
                                );

                                $pregunta = $db
                                    ->where('Id_macp', $data['pregunta-' . $b])
                                    ->update('modulo_actividades_cuestionario_preguntas', $datos);

                                if (isset($data['respuesta-' . $b])) {
                                    for ($i = 0; $i < count($data['respuesta'][$b]); $i++) {
                                        if (isset($data['respuesta-' . $b][$i])) {
                                            $datos = array(
                                                'respuesta_macr' => $data['respuesta'][$b][$i],
                                                'tipo_macr' => $data['respuesta_tipo'][$b][$i],
                                            );

                                            $respuesta = $db
                                                ->where('Id_macr', $data['respuesta-' . $b][$i])
                                                ->update('modulo_actividades_cuestionario_respuestas', $datos);
                                        } else {
                                            $datos = array(
                                                'Id_macp' => $data['pregunta-' . $b],
                                                'respuesta_macr' => $data['respuesta'][$b][$i],
                                                'tipo_macr' => $data['respuesta_tipo'][$b][$i],
                                            );

                                            if (isset($data['palabra'][$b][$i])) {
                                                $datos['palabra_macr'] = $data['palabra'][$b][$i];
                                            }


                                            $respuesta = $db
                                                ->insert('modulo_actividades_cuestionario_respuestas', $datos);
                                        }
                                    }
                                }
                            }
                        } else {
                            $datos = array(
                                'Id_mac' => $comprobar[0]->Id_mac,
                                'pregunta_macp' => $data['pregunta'][$b],
                                'tipo_macp' => $data['pregunta_tipo'][$b],
                                'valor_macp' => $data['pregunta_valor'][$b],
                            );

                            $pregunta = $db
                                ->insert('modulo_actividades_cuestionario_preguntas', $datos);

                            if ($pregunta) {
                                for ($i = 0; $i < count($data['respuesta'][$b]); $i++) {
                                    $datos = array(
                                        'Id_macp' => $pregunta,
                                        'respuesta_macr' => $data['respuesta'][$b][$i],
                                        'tipo_macr' => $data['respuesta_tipo'][$b][$i],
                                    );

                                    if (isset($data['palabra'][$b][$i])) {
                                        $datos['palabra_macr'] = $data['palabra'][$b][$i];
                                    }

                                    $respuesta = $db
                                        ->insert('modulo_actividades_cuestionario_respuestas', $datos);
                                }
                            }
                        }
                    }
                }

                if (isset($data['eliminar-pregunta'])) {
                    for ($i = 0; $i < count($data['eliminar-pregunta']); $i++) {
                        $eliminar = $db
                            ->where('Id_macp', $data['eliminar-pregunta'][$i])
                            ->delete('modulo_actividades_cuestionario_preguntas');

                        if ($eliminar) {
                            $respuestas = $db
                                ->where('Id_macp', $data['eliminar-pregunta'][$i])
                                ->delete('modulo_actividades_cuestionario_respuestas');
                        }
                    }
                }

                if (isset($data['eliminar-respuesta'])) {
                    for ($i = 0; $i < count($data['eliminar-respuesta']); $i++) {
                        $pregunta = $db
                            ->where('Id_macr', $data['eliminar-respuesta'][$i])
                            ->delete('modulo_actividades_cuestionario_respuestas');
                    }
                }

                 $db
                    ->where('Id_mad', $data['idtcuestionario'])
                    ->update('modulos_actividades_detalle', ['nombre_mad' => $data['nombre']]);

                $informacion['status'] = true;
                $informacion['msg'] = 'Actividad guardada.';
            } else {
                $informacion['status'] = false;
                $informacion['msg'] = 'La actividad no se pudo guardar';
            }
        }

        echo json_encode($informacion);
        break;
    case 'Calificar':
        $respuestas = $db
            ->where('Id_mad', $data['idcuestionario'])
            ->objectBuilder()->get('modulo_actividades_cuestionario_calificaciones');

        if ($db->count > 0) {
            $i = 0;
            foreach ($respuestas as $respuesta) {
                $calificar = $db
                    ->where('Id_macc', $respuesta->Id_macc)
                    ->update('modulo_actividades_cuestionario_calificaciones', ['calificacion_macc' => $data['nota'][$i]]);
                $i++;
            }

            if ($calificar) {
                $informacion['status'] = true;
                $informacion['msg'] = 'Calificación guardada.';
            } else {
                $informacion['status'] = false;
                $informacion['msg'] = 'La calificación no se pudo guardar';
            }
        } else {
            $informacion['status'] = false;
            $informacion['msg'] = 'La calificación no se pudo guardar';
        }

         echo json_encode($informacion);
        break;
}
