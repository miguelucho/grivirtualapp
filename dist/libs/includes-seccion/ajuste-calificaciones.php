 <div class="Contenedor-principal-titulo">
   <div class="Contenedor-principal-titulo-sec">
     <h2>Calificaciones</h2>
   </div>
   <div class="Contenedor-principal-titulo-sec">
     <div class="Btn-flotante-crear">
       <a href="#tipo" data-position="left" data-tooltip="Crear Fuente" class="modal-trigger tooltipped btn-floating btn-large waves-effect waves-light blue-grey darken-4"><i class="material-icons">add</i></a>
     </div>
   </div>
 </div>
 <div class="Contenedor-desc">
   <div class="Contenedor-desc-int">
     <div class="Content-table Table-tr-gris">
       <table>
         <thead>
           <tr>
             <th>Nombre</th>
             <th>Estado</th>
             <th colspan="2">Acciones</th>
           </tr>
         </thead>
         <tbody id="Listado-calificaciones">
         </tbody>
       </table>
       <div class="listado-loader" style="text-align: center; margin-top: 50px;">
         <div class="preloader-wrapper big active">
           <div class="spinner-layer">
             <div class="circle-clipper left">
               <div class="circle"></div>
             </div>
             <div class="gap-patch">
               <div class="circle"></div>
             </div>
             <div class="circle-clipper right">
               <div class="circle"></div>
             </div>
           </div>
         </div>
       </div>
       <div class="listado-paginacion">
         <ul class="pagination">
         </ul>
       </div>
     </div>
   </div>
 </div>

 <div id="tipo" class="modal">
   <div class="modal-header Modal-padding-top">
     <div class="row">
       <div class="col s12">
         <button type="button" class="btn-floating modal-close waves-effect waves-light btn right blue-grey darken-4">
           &times;
         </button>
         <h5 class="center-align Titulo-grande"><span id="Modal-titulo">Crear</span> Calificaciones</h5>
       </div>
     </div>
   </div>
   <div class="modal-content">
     <div class="Contenedor-formularios">
       <div class="Contenedor-formularios-int">
         <form id="Tipo-nuevo">
           <div class="Contenedor-formularios-bloque">
             <div class="Colum-dos">
               <div class="input-field">
                 <input id="Nombre" type="text" name="ajuste[Nombre]" class="validate" required="">
                 <label for="Nombre">Calificación</label>
               </div>
             </div>
             <div class="Colum-dos">
               <div class="input-field ">
                 <select name="ajuste[Estado]" id="Estado">
                   <option value="" disabled selected>Seleccionar</option>
                   <option value="1">ACTIVO</option>
                   <option value="0">INACTIVO</option>
                 </select>
                 <label>Estado</label>
               </div>
             </div>
           </div>
           <div class="Colum-uno">
             <input type="submit" class="Btn Btn-azul Bold-ro Btn-expand" value="Guardar">
           </div>
         </form>
       </div>
     </div>
   </div>
 </div>
