<?php
	$imagen = 'demo.jpg';

	$usuarios = $db
    	->where('Id_us', $_SESSION['griapp_user'])
    	->objectBuilder()->get('usuarios_app');

	if($db->count > 0 && $usuarios[0]->imagen_us != ''){
		$imagen = $usuarios[0]->imagen_us;
	}

	$ls_notificaciones = '';

	if($_SESSION['griapp_tipo'] == 2){
		$matricula = $db
			->where('Id_ma', $_SESSION['griapp_IDtipo'])
			->objectBuilder()->get('matriculas');

		if ($db->count > 0) {
			$nombre_programa = '';

			$programas = $db
				->where('Id_pr', $matricula[0]->programa_ma)
				->objectBuilder()->get('programas');

			$nombre_programa = $programas[0]->nombre_pr;

			$detalles_matricula = $db
				->where('Id_ma', $matricula[0]->Id_ma)
				->objectBuilder()->get('matriculas_detalles');

			if ($db->count > 0) {
				$actividades = $db
					->where('Id_gr', $detalles_matricula[0]->grupo_md)
					->objectBuilder()->get('modulos_actividades');

				if ($db->count > 0) {
					foreach ($actividades as $actividad) {
						$detalles = $db
							->where('Id_ma', $actividad->Id_ma)
							->objectBuilder()->get('modulos_actividades_detalle');

						if ($db->count > 0) {
							foreach ($detalles as $detalle) {
								$notificaciones = $db
									->where('Id_us', $_SESSION['griapp_user'])
									->where('tipoid_ea', $detalle->Id_mad)
									->objectBuilder()->get('usuarios_notificaciones');

								if($db->count == 0){
									$link = '';
									$tipo_actividad = '';
									switch ($detalle->tipo_mad) {
										case 1:
											$link = 'estudiante-tarea?tr=' . $detalle->Id_mad;
											$tipo_actividad = 'Nueva Tarea';
											break;
										case 3:
											$link = 'estudiante-foro?fr=' . $detalle->Id_mad;
											$tipo_actividad = 'Nuevo Foro';
											break;
										case 5:
											$link = 'estudiante-cuestionario?cu=' . $detalle->Id_mad;
											$tipo_actividad = 'Nuevo Cuestionario';
											break;
									}
									$ls_notificaciones .= '<li><a href="' . $link . '">'. $tipo_actividad . ' : ' . $detalle->nombre_mad . ' '. $nombre_programa.'</a></li>';
								}
							}
						}
					}
				}
			}
		}


	}
?>

<div class="Admin-top-int">
	<div class="Admin-top-int-izq">
		<a href="javascript:void(0)" id="Drop" class="Adminm-min-menu">
			<img src="dist/assets/images/menu_hamburgesa.png" class="Admin-menu-burger">
		</a>
		<a href="#adminhome" class="Admin-top-title">Gricompany</a>
	</div>
	<div class="Admin-top-int-der">
		<div class="Header-user" >
			<?php if($_SESSION['griapp_tipo'] == 2 && $ls_notificaciones != ''){?>
			<div style="position: absolute;right: 60px;top: 10px;">
				<a class="btn-flat dropdown-trigger" style="color: red;" href="#!" data-target="dropdown2"><i class="material-icons">add_alert</i></a>
				<ul id="dropdown2" class="dropdown-content" >
					<?php echo $ls_notificaciones ?>
				</ul>
			</div>
			<?php }?>
			<div class="Header-user-int" >
				<a href="javascript:void(0)" id="Menu-user">
				<div class="Header-user-img">
					<img src="dist/assets/images/usuarios/<?php echo $imagen ?>" class="Menu-usuario" >
				</div>
				</a>
				<div class="Header-menu-user Header-menu-user-oculto">
					<ul class="Header-menu-user-int">
						<li><a href="perfil-usuario"><i class="icon-user"></i> Perfil</a></li>
						<li><a href="empresas"><i class="icon-bubble2"></i> Ayuda</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<style>
	#dropdown2 li a{
		width: 400px !important;
		overflow: hidden;
    	text-overflow: ellipsis;
	}
</style>
