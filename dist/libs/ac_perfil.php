<?php

require_once "conexion.php";
$data = $_REQUEST['usuario'];
$informacion = array();
$folder = '../assets/images/usuarios/';

switch ($data['opc']) {
    case 'Editar':
        session_start();

        $datos = array(
            'nombre_us' => $data['Nombreusu'],
            'login_us' => $data['Correo'],
        );

        if (trim($data['Contrasena']) != '') {
            require_once "Password.php";

            $newpass = password_hash($data['Contrasena'], PASSWORD_BCRYPT);
            $datos['password_us'] = $newpass;
        }

        $editar = $db
            ->where('Id_us', $_SESSION['griapp_user'])
            ->update('usuarios_app', $datos);

        if ($editar) {
            $informacion['status'] = true;
            $informacion['nombre'] = $data['Nombreusu'];
            $informacion['msg'] = 'Perfil editado.';

            if (isset($data['imagen_perfil']) && trim($data['imagen_perfil']) != '') {
                $img        = explode(',', $data['imagen_perfil']);
                $img        = base64_decode($img[1]);
                $nombre     = $_SESSION['griapp_user'] . '_' . date('YmdHis') . Limpiar($data['imagen_perfil_nombre']) . '.jpg';
                $archivador = $folder . $nombre;
                if (file_put_contents($archivador, $img)) {
                    $usuarios = $db
                        ->where('Id_us', $_SESSION['griapp_user'])
                        ->objectBuilder()->get('usuarios_app');

                    $img_actual = $usuarios[0]->imagen_us;

                    require 'imagine/vendor/autoload.php';
                    $imagine = new Imagine\Gd\Imagine();

                    $img = $imagine->open($archivador);
                    $img = $img->save($archivador);

                    $addimg = $db
                        ->where('Id_us', $_SESSION['griapp_user'])
                        ->update('usuarios_app', ['imagen_us' => $nombre]);

                    if ($addimg) {
                        if (file_exists($folder . $img_actual)) {
                            unlink($folder . $img_actual);
                        }
                    }

                    $informacion['img'] = 'dist/assets/images/usuarios/' . $nombre;
                } else {
                    $informacion['img'] = '';
                    $informacion['msg'] .= ', No se pudo subir la imagen.';
                }
            }
        } else {
            $informacion['status'] = false;
            $informacion['msg'] = 'No se pudo editar el perfil.';
        }

        echo json_encode($informacion);
        break;
}

function limpiar($String)
{
    $String = str_replace(array('á', 'à', 'â', 'ã', 'ª', 'ä'), "a", $String);
    $String = str_replace(array('Á', 'À', 'Â', 'Ã', 'Ä'), "A", $String);
    $String = str_replace(array('Í', 'Ì', 'Î', 'Ï'), "I", $String);
    $String = str_replace(array('í', 'ì', 'î', 'ï'), "i", $String);
    $String = str_replace(array('é', 'è', 'ê', 'ë'), "e", $String);
    $String = str_replace(array('É', 'È', 'Ê', 'Ë'), "E", $String);
    $String = str_replace(array('ó', 'ò', 'ô', 'õ', 'ö', 'º'), "o", $String);
    $String = str_replace(array('Ó', 'Ò', 'Ô', 'Õ', 'Ö'), "O", $String);
    $String = str_replace(array('ú', 'ù', 'û', 'ü'), "u", $String);
    $String = str_replace(array('Ú', 'Ù', 'Û', 'Ü'), "U", $String);
    $String = str_replace(array('[', '^', '´', '`', '¨', '~', ']'), "", $String);
    $String = str_replace("ç", "c", $String);
    $String = str_replace("Ç", "C", $String);
    $String = str_replace("ñ", "n", $String);
    $String = str_replace("Ñ", "N", $String);
    $String = str_replace("Ý", "Y", $String);
    $String = str_replace("ý", "y", $String);
    $String = preg_replace('/\s+/', '_', $String);
    $String = str_replace(array('(', ')'), '', $String);
    $String = str_replace("-", "_", $String);
    return $String;
}
