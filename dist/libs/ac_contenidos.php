<?php

require_once "conexion.php";
$data = $_REQUEST['contenido'];
$informacion = array();

switch ($data['opc']) {
    case 'Nuevo':
        $datos = array(
            'Id_gr' => $data['grupo'],
            'descripcion_con' => $data['descripcion'],
            'fecha_con' => $db->now()
        );

        $nuevo = $db
            ->insert('grupos_contenidos', $datos);

        if ($nuevo) {
            $informacion['status'] = true;
            $informacion['msg'] = 'Contenido creado.';
        } else {
            $informacion['status'] = false;
            $informacion['msg'] = 'El contenido no se pudo crear.';
        }

        echo json_encode($informacion);
        break;
    case 'Contenido-listado':
        session_start();

        require_once 'Paginacion.php';
        $page = $data['pagina'];
        $results_pg = 50;
        $adjacent = 2;

        $grupos = $db
            ->where('Id_gr', $data['grupo'], 'LIKE')
            ->objectBuilder()->get('grupos_contenidos');

        // print_r($db->getLastQuery());

        $numpgs = ceil($db->count / $results_pg);

        if ($numpgs >= 1) {
            $content = '';
            $db->pageLimit = $results_pg;

            $grupos = $db
                ->where('Id_gr', $data['grupo'], 'LIKE')
                ->orderBy('Id_con', 'DESC')
                ->objectBuilder()->paginate('grupos_contenidos', $page);


            foreach ($grupos as $grupo) {
                $content .= '<tr id="Con-' . $grupo->Id_con . '">
                                <td>' . firstXChars($grupo->descripcion_con) . '</td>
                                <td nowrap>' . date('d-m-Y - h:i A', strtotime($grupo->fecha_con)) . '</td>
                                <td><a href="javascript:void(0)" class="Btn-ver Btn-table-verde Editar-contenido"><i class="icon-pencil"></i>Editar</a></td>
                            </tr>';
            }

            $informacion['list'] = $content;
            $pagconfig = array(
                'pagina' => $page,
                'totalrows' => $db->totalPages,
                'ultima_pag' => $numpgs,
                'resultados_pag' => $results_pg,
                'adyacentes' => $adjacent
            );
            $paginate = new Paginacion($pagconfig);
            $informacion['pagination'] = $paginate->crearlinks();
        } else {
            $informacion['list'] = '<tr>
                                <td colspan="2">No hay registros</td>
                            </tr>';
            $informacion['pagination'] = '';
        }

        echo json_encode($informacion);
        break;
    case 'Contenido-info':
        $idcon = explode('-', $data['idcon']);

        $contenidos = $db
            ->where('Id_con', $idcon[1])
            ->objectBuilder()->get('grupos_contenidos', null, 'descripcion_con');

        if ($db->count > 0) {
            $informacion['info'] = $contenidos[0];
            $informacion['status'] = true;
        } else {
            $informacion['status'] = false;
            $informacion['msg'] = 'El contenido no existe.';
        }

        echo json_encode($informacion);
        break;
    case 'Editar':
        $idcon = explode('-', $data['idcon']);

        $datos = array(
            'descripcion_con' => $data['descripcion'],
            'fecha_con' => $db->now()
        );

        $nuevo = $db
            ->where('Id_con', $idcon[1])
            ->update('grupos_contenidos', $datos);

        if ($nuevo) {
            $informacion['status'] = true;
            $informacion['msg'] = 'Contenido editado.';
        } else {
            $informacion['status'] = false;
            $informacion['msg'] = 'El contenido no se pudo editar.';
        }

        echo json_encode($informacion);
        break;
}


function firstXChars($string, $chars = 500)
{
    preg_match('/^.{0,' . $chars . '}(?:.*?)\b/iu', $string, $matches);
    return $matches[0] . '...';
}
