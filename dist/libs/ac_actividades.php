<?php

require_once "conexion.php";
$data = $_REQUEST['actividad'];
$informacion = array();

switch ($data['opc']) {
    case 'Nueva':
        if ($data['idactividad'] == 0) {
            $datos = array(
                'Id_gr' => $data['grupo'],
                'Id_mo' => $data['modulo'],
                'descripcion_ma' => $data['modulodescripcion'],
                'periodo_ma' => $data['periodo'],
            );

            $nuevo = $db
                ->insert('modulos_actividades', $datos);

            if ($nuevo) {
                for ($i = 0; $i < count($data['nombre']); $i++) {

                    $datos = array(
                        'Id_ma' => $nuevo,
                        'tipo_mad' => $data['tipo'][$i],
                        'valor_mad' => $data['valor'][$i],
                        'nombre_mad' => $data['nombre'][$i],
                        'Id_co' => $data['corte'][$i],
                    );

                    $detalle = $db
                        ->insert('modulos_actividades_detalle', $datos);
                }


                $informacion['status'] = true;
                $informacion['msg'] = 'Actividades asignadas.';
            } else {
                $informacion['status'] = false;
                $informacion['msg'] = 'Las actividades no pudieron ser asignadas.';
            }
        } else {
            $datos = array(
                'Id_gr' => $data['grupo'],
                'Id_mo' => $data['modulo'],
                'descripcion_ma' => $data['modulodescripcion'],
            );

            $editar = $db
                ->where('Id_ma', $data['idactividad'])
                ->update('modulos_actividades', $datos);


            if ($editar) {
                for ($i = 0; $i < count($data['nombre']); $i++) {

                    if (isset($data['actividad'][$i])) {
                        $datos = array(
                            'tipo_mad' => $data['tipo'][$i],
                            'valor_mad' => $data['valor'][$i],
                            'nombre_mad' => $data['nombre'][$i],
                            'Id_co' => $data['corte'][$i],
                        );

                        $db
                            ->where('Id_mad', $data['actividad'][$i])
                            ->update('modulos_actividades_detalle', $datos);
                    } else {
                        $datos = array(
                            'Id_ma' => $data['idactividad'],
                            'tipo_mad' => $data['tipo'][$i],
                            'valor_mad' => $data['valor'][$i],
                            'nombre_mad' => $data['nombre'][$i],
                            'Id_co' => $data['corte'][$i],
                        );

                        $detalle = $db
                            ->insert('modulos_actividades_detalle', $datos);
                    }

                    if (isset($data['idexistente'])) {
                        for ($j = 0; $j < count($data['idexistente']); $j++) {
                            $detalles = $db
                                ->where('Id_mad', $data['idexistente'][$j])
                                ->objectBuilder()->get('modulos_actividades_detalle');

                            if ($db->count > 0) {
                                $datos = array(
                                    'Id_ma' => $data['idactividad'],
                                    'tipo_mad' => $data['tipo'][$i],
                                    'valor_mad' => $data['valor'][$i],
                                    'nombre_mad' => $data['nombre'][$i],
                                    'Id_co' => $data['corte'][$i],
                                );

                                $copia_nuevo = $db
                                    ->insert('modulos_actividades_detalle', $datos);

                                if ($copia_nuevo) {
                                    if ($detalles[0]->tipo_mad == 1) {
                                        $tareas = $db
                                            ->where('Id_mad', $data['idexistente'][$j])
                                            ->objectBuilder()->get('modulos_actividades_tareas');

                                        if ($db->count > 0) {
                                            foreach ($tareas as $tarea) {
                                                $datos = [
                                                    'Id_mad' => $copia_nuevo,
                                                    'descripcion_mat' => $tarea->descripcion_mat,
                                                    'fecha_inicio_mat' => $tarea->fecha_inicio_mat,
                                                    'hora_inicio_mat' => $tarea->hora_inicio_mat,
                                                    'fecha_fin_mat' => $tarea->fecha_fin_mat,
                                                    'hora_fin_mat' => $tarea->hora_fin_mat,
                                                ];

                                                $tarea_detalles = $db
                                                    ->insert('modulos_actividades_tareas', $datos);

                                                if ($tarea_detalles) {
                                                    $archivos = $db
                                                        ->where('Id_mat', $tarea->Id_mat)
                                                        ->objectBuilder()->get('modulos_actividades_tareas_archivos');

                                                    if ($db->count > 0) {
                                                        foreach ($archivos as $archivo) {
                                                            $datos = [
                                                                'Id_mat' => $tarea_detalles,
                                                                'nombre_mata' => $archivo->nombre_mata,
                                                                'archivo_mata' => $archivo->archivo_mata,
                                                            ];

                                                            $db
                                                                ->insert('modulos_actividades_tareas_archivos', $datos);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    if ($detalles[0]->tipo_mad == 3) {
                                        $foros = $db
                                            ->where('Id_mad', $data['idexistente'][$j])
                                            ->objectBuilder()->get('modulos_actividades_foros');

                                        if ($db->count > 0) {
                                            foreach ($foros as $foro) {
                                                $datos = [
                                                    'Id_mad' => $copia_nuevo,
                                                    'descripcion_maf' => $foro->descripcion_maf,
                                                    'fecha_inicio_maf' => $foro->fecha_inicio_maf,
                                                    'hora_inicio_maf' => $foro->hora_inicio_maf,
                                                    'fecha_fin_maf' => $foro->fecha_fin_maf,
                                                    'hora_fin_maf' => $foro->hora_fin_maf,
                                                ];

                                                $foro_detalles = $db
                                                    ->insert('modulos_actividades_foros', $datos);

                                                if ($foro_detalles) {
                                                    $archivos = $db
                                                        ->where('Id_maf', $foro->Id_maf)
                                                        ->objectBuilder()->get('modulos_actividades_foros_archivos');

                                                    if ($db->count > 0) {
                                                        foreach ($archivos as $archivo) {
                                                            $datos = [
                                                                'Id_maf' => $archivo->Id_maf,
                                                                'nombre_mafa' => $archivo->nombre_mafa,
                                                                'archivo_mafa' => $archivo->archivo_mafa,
                                                            ];

                                                            $db
                                                                ->insert('modulos_actividades_foros_archivos', $datos);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    if ($detalles[0]->tipo_mad == 5) {
                                        $cuestionarios = $db
                                            ->where('Id_mad', $data['idexistente'][$j])
                                            ->objectBuilder()->get('modulos_actividades_cuestionario');

                                        if ($db->count > 0) {
                                            foreach ($cuestionarios as $cuestionario) {
                                                $datos = [
                                                    'Id_mad' => $copia_nuevo,
                                                    'fecha_inicio_mac' => $cuestionario->fecha_inicio_mac,
                                                    'hora_inicio_mac' => $cuestionario->hora_inicio_mac,
                                                    'fecha_fin_mac' => $cuestionario->fecha_fin_mac,
                                                    'hora_fin_mac' => $cuestionario->hora_fin_mac,
                                                ];

                                                $cuestionario_detalles = $db
                                                    ->insert('modulos_actividades_cuestionario', $datos);

                                                if ($cuestionario_detalles) {
                                                    $preguntas = $db
                                                        ->where('Id_mac', $cuestionario->Id_mac)
                                                        ->objectBuilder()->get('modulos_actividades_cuestionario_preguntas');

                                                    if ($db->count > 0) {
                                                        foreach ($preguntas as $pregunta) {
                                                            $datos = [
                                                                'Id_mac' => $cuestionario_detalles,
                                                                'pregunta_macp' => $pregunta->pregunta_macp,
                                                                'tipo_macp' => $pregunta->tipo_macp,
                                                                'valor_macp' => $pregunta->valor_macp,
                                                            ];

                                                            $cuestionario_preguntas = $db
                                                                ->insert('modulos_actividades_cuestionario_preguntas', $datos);

                                                            if ($cuestionario_preguntas) {
                                                                $respuestas = $db
                                                                    ->where('Id_macp', $pregunta->Id_macp)
                                                                    ->objectBuilder()->get('modulos_actividades_cuestionario_respuestas');

                                                                if ($db->count > 0) {
                                                                    foreach ($respuestas as $respuesta) {
                                                                        $datos = [
                                                                            'Id_macp' => $cuestionario_preguntas,
                                                                            'respuesta_macr' => $respuesta->respuesta_macr,
                                                                            'tipo_macr' => $respuesta->tipo_macr,
                                                                            'palabra_macr' => $respuesta->palabra_macr,
                                                                        ];

                                                                        $respuestas = $db
                                                                            ->insert('modulos_actividades_cuestionario_respuestas', $datos);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }


                $informacion['status'] = true;
                $informacion['msg'] = 'Actividades asignadas.';
            } else {
                $informacion['status'] = false;
                $informacion['msg'] = 'Las actividades no pudieron ser asignadas.';
            }
        }

        echo json_encode($informacion);
        break;
    case 'Asignados-listado':
        require_once 'Paginacion.php';
        $page = $data['pagina'];
        $results_pg = 50;
        $adjacent = 2;

        ($data['programa'] == '' ? $data['programa'] = '%' : '');
        ($data['curso'] == '' ? $data['curso'] = '%' : '');
        ($data['codigo'] == '' ? $data['codigo'] = '%' : '');


        $grupos = $db
            ->where('Id_pr', $data['programa'], 'LIKE')
            ->where('Id_cu', $data['curso'], 'LIKE')
            ->where('codigo_gr', $data['codigo'], 'LIKE')
            ->where('estado_gr', 1)
            ->objectBuilder()->get('grupos');

        $numpgs = ceil($db->count / $results_pg);

        if ($numpgs >= 1) {
            $content = '';
            $db->pageLimit = $results_pg;

            $grupos = $db
                ->where('Id_pr', $data['programa'], 'LIKE')
                ->where('Id_cu', $data['curso'], 'LIKE')
                ->where('codigo_gr', $data['codigo'], 'LIKE')
                ->where('estado_gr', 1)
                ->orderBy('Id_gr', 'DESC')
                ->objectBuilder()->paginate('grupos', $page);


            foreach ($grupos as $grupo) {
                $nm_programa = '';
                $nm_curso = '';
                $ls_modulos = '';

                $programas = $db
                    ->where('Id_pr', $grupo->Id_pr)
                    ->objectBuilder()->get('programas');

                if ($db->count > 0) {
                    $nm_programa = $programas[0]->nombre_pr;
                }

                $cursos = $db
                    ->where('Id_cu', $grupo->Id_cu)
                    ->objectBuilder()->get('cursos');

                if ($db->count > 0) {
                    $nm_curso = $cursos[0]->nombre_cu;
                }

                $modulos = $db
                    ->where('Id_pr', $grupo->Id_pr)
                    ->where('Id_cu', $grupo->Id_cu)
                    ->objectBuilder()->get('modulos');

                if ($db->count > 0) {
                    foreach ($modulos as $modulo) {
                        $ls_modulos .= '<span>' . $modulo->nombre_mo . '</span><br>';
                    }
                }

                $inicio = date_format(date_create($grupo->inicio_gr), 'd-m-Y');
                $fin = date_format(date_create($grupo->fin_gr), 'd-m-Y');

                $content .= '<tr>
                                <td>
                                    <span>' . $nm_programa . '</span><br>
                                    <span>' . $nm_curso . '</span>
                                </td>
                                <td>' . $ls_modulos . '</td>
                                <td style="white-space: nowrap;">' . $grupo->codigo_gr . '</td>
                                <td class="center">' . $grupo->estudiantes_gr . '</td>
                                <td class="center">0</td>
                                <td>' . $inicio . '</td>
                                <td>' . $fin . '</td>
                                <td><a href="administrar-modulos-interno?gr=' . $grupo->Id_gr . '" class="Btn-ver Btn-table-verde"><i class="icon-pencil"></i>Administrar</a></td>
                            </tr>';
            }

            $informacion['list'] = $content;
            $pagconfig = array(
                'pagina' => $page,
                'totalrows' => $db->totalPages,
                'ultima_pag' => $numpgs,
                'resultados_pag' => $results_pg,
                'adyacentes' => $adjacent
            );
            $paginate = new Paginacion($pagconfig);
            $informacion['pagination'] = $paginate->crearlinks();
        } else {
            $informacion['list'] = '<tr>
                                <td colspan="8">No hay registros</td>
                            </tr>';
            $informacion['pagination'] = '';
        }

        echo json_encode($informacion);
        break;
    case 'Grupo-info':
        $idgrupo = explode('-', $data['idgrupo']);

        $grupos = $db
            ->where('Id_gr', $idgrupo[1])
            ->objectBuilder()->get('grupos');

        if ($db->count > 0) {
            $informacion['info'] = $grupos[0];
            $informacion['status'] = true;
        } else {
            $informacion['status'] = false;
            $informacion['msg'] = 'El grupo no existe.';
        }

        echo json_encode($informacion);
        break;
    case 'Grupo-editar':
        $idgrupo = explode('-', $data['idgrupo']);
        $codigo = substr(date('Y'), -2) . '-' . $data['programa'] . '-' . $data['curso'] . '-' . $idgrupo[1];

        $datos = array(
            'Id_pr' => $data['programa'],
            'Id_cu' => $data['curso'],
            'Id_do' => $data['docente'],
            'codigo_gr' => $codigo,
            'estudiantes_gr' => $data['estudiantes'],
            'inicio_gr' => $data['inicio'],
            'fin_gr' => $data['fin'],
            'estado_gr' => $data['estado'],
        );

        $nuevo = $db
            ->where('Id_gr', $idgrupo[1])
            ->update('grupos', $datos);

        if ($nuevo) {
            $informacion['status'] = true;
            $informacion['msg'] = 'Grupo editado.';
        } else {
            $informacion['status'] = false;
            $informacion['msg'] = 'El grupo no se pudo editar.';
        }

        echo json_encode($informacion);
        break;
    case 'Cortes':
        $total = 0;

        $actividades = $db
            ->where('Id_gr', $data['grupo'])
            ->objectBuilder()->get('modulos_actividades');

        if ($db->count > 0) {
            foreach ($actividades as $actividad) {
                $detalles = $db
                    ->where('Id_ma', $actividad->Id_ma)
                    ->where('Id_co', $data['corte'])
                    ->objectBuilder()->get('modulos_actividades_detalle');

                if ($db->count > 0) {
                    foreach ($detalles as $detalle) {
                        $total += $detalle->valor_mad;
                    }

                    $informacion['status'] = true;
                    $informacion['total'] = $total;
                } else {
                    $informacion['status'] = false;
                }
            }
        } else {
            $informacion['status'] = false;
        }

        echo json_encode($informacion);
        break;
    case 'Nuevo-contenido-curso':
        if ($data['idcontenido'] == 0) {
            $datos = array(
                'Id_gr' => $data['grupo'],
                'descripcion_con' => $data['descripcion'],
            );

            $nuevo = $db
                ->insert('modulos_contenidos', $datos);

            if ($nuevo) {
                $informacion['status'] = true;
                $informacion['msg'] = 'Actividades asignadas.';
            } else {
                $informacion['status'] = false;
                $informacion['msg'] = 'Las actividades no pudieron ser asignadas.';
            }
        } else {
            $datos = array(
                'Id_gr' => $data['grupo'],
                'descripcion_ma' => $data['descripcion'],
            );

            $editar = $db
                ->where('Id_con', $data['idcontenido'])
                ->update('modulos_contenidos', $datos);


            if ($editar) {
                $informacion['status'] = true;
                $informacion['msg'] = 'Actividades asignadas.';
            } else {
                $informacion['status'] = false;
                $informacion['msg'] = 'Las actividades no pudieron ser asignadas.';
            }
        }

        echo json_encode($informacion);
        break;
    case 'Existentes':
        $existentes = array();

        switch ($data['tipo']) {
            case '2':
                $grupo = $db
                    ->where('Id_gr', $data['grupo'])
                    ->objectBuilder()->get('grupos');

                if ($db->count > 0) {
                    $curso = $grupo[0]->Id_cu;
                    $docente = $grupo[0]->Id_do;

                    $grupos = $db
                        ->where('Id_cu', $curso)
                        ->where('Id_do', $docente)
                        ->where('Id_gr', $data['grupo'], '!=')
                        ->objectBuilder()->get('grupos');

                    if ($db->count > 0) {
                        foreach ($grupos as $grupo) {
                            $actividades = $db
                                ->where('Id_gr', $grupo->Id_gr)
                                ->objectBuilder()->get('modulos_actividades');

                            if ($db->count > 0) {
                                foreach ($actividades as $actividad) {
                                    $detalles = $db
                                        ->where('Id_ma', $actividad->Id_ma)
                                        ->where('tipo_mad', 1)
                                        ->objectBuilder()->get('modulos_actividades_detalle');

                                    if ($db->count > 0) {
                                        $existentes[] = array('Id' => $detalles[0]->Id_mad, 'nombre' => $detalles[0]->nombre_mad, 'tipo' => $detalles[0]->tipo_mad);
                                    }
                                }
                            }
                        }
                    }
                }

                break;
            case '4':
                $grupo = $db
                    ->where('Id_gr', $data['grupo'])
                    ->objectBuilder()->get('grupos');

                if ($db->count > 0) {
                    $curso = $grupo[0]->Id_cu;
                    $docente = $grupo[0]->Id_do;

                    $grupos = $db
                        ->where('Id_cu', $curso)
                        ->where('Id_do', $docente)
                        ->where('Id_gr', $data['grupo'], '!=')
                        ->objectBuilder()->get('grupos');

                    if ($db->count > 0) {
                        foreach ($grupos as $grupo) {
                            $actividades = $db
                                ->where('Id_gr', $grupo->Id_gr)
                                ->objectBuilder()->get('modulos_actividades');

                            if ($db->count > 0) {
                                foreach ($actividades as $actividad) {
                                    $detalles = $db
                                        ->where('Id_ma', $actividad->Id_ma)
                                        ->where('tipo_mad', 3)
                                        ->objectBuilder()->get('modulos_actividades_detalle');

                                    if ($db->count > 0) {
                                        $existentes[] = array('Id' => $detalles[0]->Id_mad, 'nombre' => $detalles[0]->nombre_mad, 'tipo' => $detalles[0]->tipo_mad);
                                    }
                                }
                            }
                        }
                    }
                }

                break;
            case '6':
                $grupo = $db
                    ->where('Id_gr', $data['grupo'])
                    ->objectBuilder()->get('grupos');

                if ($db->count > 0) {
                    $curso = $grupo[0]->Id_cu;
                    $docente = $grupo[0]->Id_do;

                    $grupos = $db
                        ->where('Id_cu', $curso)
                        ->where('Id_do', $docente)
                        ->where('Id_gr', $data['grupo'], '!=')
                        ->objectBuilder()->get('grupos');

                    if ($db->count > 0) {
                        foreach ($grupos as $grupo) {
                            $actividades = $db
                                ->where('Id_gr', $grupo->Id_gr)
                                ->objectBuilder()->get('modulos_actividades');

                            if ($db->count > 0) {
                                foreach ($actividades as $actividad) {
                                    $detalles = $db
                                        ->where('Id_ma', $actividad->Id_ma)
                                        ->where('tipo_mad', 5)
                                        ->objectBuilder()->get('modulos_actividades_detalle');

                                    if ($db->count > 0) {
                                        $existentes[] = array('Id' => $detalles[0]->Id_mad, 'nombre' => $detalles[0]->nombre_mad, 'tipo' => $detalles[0]->tipo_mad);
                                    }
                                }
                            }
                        }
                    }
                }

                break;
        }

        $informacion['existentes'] = $existentes;

        echo json_encode($informacion);
        break;
}
