<?php

require_once "conexion.php";
$data = $_REQUEST['tarea'];
$informacion = array();

$folder = '../assets/documentos_tareas';

switch ($data['opc']) {
    case 'Administrar':
        $detalles = $db
            ->where('Id_mad', $data['idtarea'])
            ->objectBuilder()->get('modulo_actividades_tareas');

        if ($db->count == 0) {
            $datos = array(
                'Id_mad' => $data['idtarea'],
                'descripcion_mat' => $data['descripcion'],
                'fecha_inicio_mat' => $data['apertura_fecha'],
                'hora_inicio_mat' => date('H:i:s', strtotime($data['apertura_hora'])),
                'fecha_fin_mat' => $data['cierre_fecha'],
                'hora_fin_mat' => date('H:i:s', strtotime($data['cierre_hora']))
            );

            $nuevo = $db
                ->insert('modulo_actividades_tareas', $datos);

            if ($nuevo) {
                if (isset($_FILES['archivo'])) {
                    for ($i = 0; $i < count($_FILES['archivo']); $i++) {
                        $mk         = date('mdYhis');
                        $nombre_archivo = $_FILES['archivo']['name'][$i];
                        $tmp_archivo    = $_FILES['archivo']['tmp_name'][$i];
                        $ext = pathinfo($nombre_archivo, PATHINFO_EXTENSION);

                        $documento  = limpiar(basename($nombre_archivo, "." . $ext)) . '.' . $ext;
                        $archivador = $folder . '/' . $mk . '_' . $documento;
                        if (move_uploaded_file($tmp_archivo, $archivador)) {
                            $datos = array(
                                'Id_mat' => $nuevo,
                                'nombre_mata' => $data['archivo_nombre'][$i],
                                'archivo_mata' => substr($archivador, 3),
                            );

                            $nuevo = $db
                                ->insert('modulo_actividades_tareas_archivos', $datos);
                        }
                    }
                }

                $informacion['status'] = true;
                $informacion['msg'] = 'Actividad guardada.';
            } else {
                $informacion['status'] = false;
                $informacion['msg'] = 'La actividad no se pudo guardar';
            }
        } else {
            $datos = array(
                'descripcion_mat' => $data['descripcion'],
                'fecha_inicio_mat' => $data['apertura_fecha'],
                'hora_inicio_mat' => date('H:i:s', strtotime($data['apertura_hora'])),
                'fecha_fin_mat' => $data['cierre_fecha'],
                'hora_fin_mat' => date('H:i:s', strtotime($data['cierre_hora']))
            );

            $actualiza = $db
                ->where('Id_mad', $data['idtarea'])
                ->update('modulo_actividades_tareas', $datos);

            if ($actualiza) {
                if (isset($data['eliminar'])) {
                    for ($i = 0; $i < count($data['eliminar']); $i++) {
                        $documentos = $db
                            ->where('Id_mata', $data['eliminar'][$i])
                            ->objectBuilder()->get('modulo_actividades_tareas_archivos');

                        if ($db->count > 0) {
                            if (file_exists('../' . $documentos[0]->archivo_mata)) {
                                unlink('../' . $documentos[0]->archivo_mata);
                            }

                            $db
                                ->where('Id_mata', $data['eliminar'][$i])
                                ->delete('modulo_actividades_tareas_archivos');
                        }
                    }
                }

                if (isset($_FILES['archivo'])) {
                    for ($i = 0; $i < count($_FILES['archivo']); $i++) {
                        $mk         = date('mdYhis');
                        $nombre_archivo = $_FILES['archivo']['name'][$i];
                        $tmp_archivo    = $_FILES['archivo']['tmp_name'][$i];
                        $ext = pathinfo($nombre_archivo, PATHINFO_EXTENSION);

                        $documento  = limpiar(basename($nombre_archivo, "." . $ext)) . '.' . $ext;
                        $archivador = $folder . '/' . $mk . '_' . $documento;
                        if (move_uploaded_file($tmp_archivo, $archivador)) {
                            $datos = array(
                                'Id_mat' => $detalles[0]->Id_mat,
                                'nombre_mata' => $data['archivo_nombre'][$i],
                                'archivo_mata' => substr($archivador, 3),
                            );

                            $nuevo = $db
                                ->insert('modulo_actividades_tareas_archivos', $datos);
                        }
                    }
                }

                $db
                    ->where('Id_mad', $data['idtarea'])
                    ->update('modulos_actividades_detalle', ['nombre_mad' => $data['nombre']]);

                $informacion['status'] = true;
                $informacion['msg'] = 'Actividad guardada.';
            } else {
                $informacion['status'] = false;
                $informacion['msg'] = 'La actividad no se pudo guardar';

                // print_r($db->getLastQuery());
            }
        }

        echo json_encode($informacion);
        break;
    case 'Calificar':
        $respuestas = $db
            ->where('Id_matc', $data['idtarea'])
            ->objectBuilder()->get('modulo_actividades_tareas_calificaciones');

        if ($db->count > 0) {
            $calificar = $db
                ->where('Id_matc', $data['idtarea'])
                ->update('modulo_actividades_tareas_calificaciones', ['calificacion_matc' => $data['nota'], 'observaciones_matc' => $data['observacion']]);

            if ($calificar) {
                $informacion['status'] = true;
                $informacion['msg'] = 'Calificación guardada.';
            } else {
                $informacion['status'] = false;
                $informacion['msg'] = 'La calificación no se pudo guardar';
            }
        }

        echo json_encode($informacion);
        break;
}


function limpiar($String)
{
    $String = preg_replace("/[^A-Za-z0-9\_\-]/", '', $String);
    return $String;
}
