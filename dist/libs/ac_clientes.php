<?php

require_once "conexion.php";
$data = $_REQUEST['empresa'];
$informacion = array();

switch ($data['opc']) {
    case 'Nuevo':
        session_start();

        $datos = array(
            'usuario_cl' => $data['Usuario'],
            'tipoidentificacion_cl' => $data['Tipoidenficacion'],
            'identificacion_cl' => $data['Identificacion'],
            'nombre_cl' => $data['Nombre'],
            'tipo_cl' => $data['Tipocliente'],
            'sector_cl' => $data['Sector'],
            'fuentecontacto_cl' => $data['Fuentecontacto'],
            'direccion_cl' => $data['Direccion'],
            'telefono_cl' => $data['Telefono'],
            'correo_cl' => $data['Email'],
            'sitio_cl' => $data['Sitioweb'],
            'contactobservacion_cl' => $data['Observaciones'],
            'potencial_cl' => 0,
            'estado_cl' => 1,
            'crea_cl' => $_SESSION['aud_user'],
        );

        $nuevo = $db
            ->insert('clientes', $datos);

        if ($nuevo) {
            $informacion['status'] = true;
            $informacion['msg'] = 'Cliente creado.';
        } else {
            $informacion['status'] = false;
            $informacion['msg'] = 'No se pudo crear el registro.';
        }

        echo json_encode($informacion);
        break;
    case 'Listado':
        require_once 'Paginacion.php';
        $page = $data['pagina'];
        $results_pg = 50;
        $adjacent = 2;

        ($data['identificacion'] == '' ? $data['identificacion'] = '%' : '');
        ($data['nombre'] == '' ? $data['nombre'] = '%' : '');


        $clientes = $db
            ->where('identificacion_cl', $data['identificacion'], 'LIKE')
            ->where('nombre_cl', '%' . $data['nombre'] . '%', 'LIKE')
            ->where('potencial_cl', 0)
            ->objectBuilder()->get('clientes');

        $numpgs = ceil($db->count / $results_pg);

        if ($numpgs >= 1) {
            $content = '';
            $db->pageLimit = $results_pg;

            $clientes = $db
                ->where('identificacion_cl', $data['identificacion'], 'LIKE')
                ->where('nombre_cl', '%' . $data['nombre'] . '%', 'LIKE')
                ->where('potencial_cl', 0)
                ->orderBy('Id_cl', 'DESC')
                ->objectBuilder()->paginate('clientes', $page);


            foreach ($clientes as $cliente) {
                $tipocliente = '';

                $tipos = $db
                    ->where('Id_ct', $cliente->tipo_cl)
                    ->objectBuilder()->get('cliente_tipo');

                if ($db->count > 0) {
                    $tipocliente = $tipos[0]->nombre_ct;
                }

                $btn_convertir = 'Si';

                if ($cliente->convertido_cl == 0) {
                    $btn_convertir = '<a href="javascript:void(0)" class="Btn-table-verde Btn-convertir" ><span><i class="icon-checkmark"></i>Convertir</span></a>';
                }

                $btn_activo = '';

                if ($cliente->estado_cl == 0) {
                    $btn_activo = '<a href="javascript:void(0)" class="Btn-table-verde Btn-bloquear Activar"><span><i class="icon-checkmark"></i>Desbloquear</span></a>';
                } else {
                    $btn_activo = '<a href="javascript:void(0)" class="Btn-table-rojo Btn-bloquear"><span><i class="icon-blocked"></i>Bloquear</span></a>';
                }

                $content .= ' <tr id="Cl-' . $cliente->Id_cl . '">
                                <td>' . $cliente->identificacion_cl . '</td>
                                <td>' . $cliente->nombre_cl . '</td>
                                <td>' . $tipocliente . '</td>
                                <td>' . $cliente->telefono_cl . '</td>
                                <td>' . $cliente->correo_cl . '</td>
                                <td>' . $cliente->direccion_cl . '</td>
                                <td><a href="javascript:void(0)" class="Btn-ver Btn-table-verde" ><span><i class="icon-eye"></i>Ver</span></a></td>
								<td><a href="empresas-editar?id=' . $cliente->Id_cl . '" class="Btn-table-gris"><span><i class="icon-pencil"></i>Editar</span></a></td>
								<td>' . $btn_activo . '</td>
                            </tr>';
            }

            $informacion['list'] = $content;
            $pagconfig = array(
                'pagina' => $page,
                'totalrows' => $db->totalPages,
                'ultima_pag' => $numpgs,
                'resultados_pag' => $results_pg,
                'adyacentes' => $adjacent
            );
            $paginate = new Paginacion($pagconfig);
            $informacion['pagination'] = $paginate->crearlinks();
        } else {
            $informacion['list'] = '<tr>
                                <td colspan="8">No hay registros</td>
                            </tr>';
            $informacion['pagination'] = '';
        }

        echo json_encode($informacion);
        break;
    case 'Editar':
        $datos = array(
            'usuario_cl' => $data['Usuario'],
            'tipoidentificacion_cl' => $data['Tipoidenficacion'],
            'identificacion_cl' => $data['Identificacion'],
            'nombre_cl' => $data['Nombre'],
            'tipo_cl' => $data['Tipocliente'],
            'sector_cl' => $data['Sector'],
            'fuentecontacto_cl' => $data['Fuentecontacto'],
            'direccion_cl' => $data['Direccion'],
            'telefono_cl' => $data['Telefono'],
            'correo_cl' => $data['Email'],
            'sitio_cl' => $data['Sitioweb'],
            'contactobservacion_cl' => $data['Observaciones'],
        );

        $nuevo = $db
            ->where('Id_cl', $data['idcliente'])
            ->update('clientes', $datos);

        if ($nuevo) {
            $informacion['status'] = true;
            $informacion['msg'] = 'Cliente editado.';
        } else {
            $informacion['status'] = false;
            $informacion['msg'] = 'No se pudo editar el registro.';
        }

        echo json_encode($informacion);
        break;
    case 'Bloqueo':
        $id = explode('-', $data['id']);

        $clientes = $db
            ->where('Id_cl', $id[1])
            ->objectBuilder()->get('clientes');

        if ($db->count > 0) {
            $activar = $db
                ->where('Id_cl', $id[1])
                ->update('clientes', ['estado_cl' => $db->not()]);

            $informacion['status'] = true;
        } else {
            $informacion['status'] = false;
            $informacion['msg'] = 'El cliente no existe.';
        }

        echo json_encode($informacion);
        break;
    case 'Info':
        $id = explode('-', $data['idcliente']);

        $clientes = $db
            ->where('Id_cl', $id[1])
            ->objectBuilder()->get('clientes');

        if ($db->count > 0) {
            $informacion['status'] = true;

            $usuarios = $db
                ->where('Id_us', $clientes[0]->usuario_cl)
                ->objectBuilder()->get('usuarios');

            $tipoidentifica = $db
                ->where('Id_ti', $clientes[0]->tipoidentificacion_cl)
                ->objectBuilder()->get('tipo_identificacion');

            $tipos = $db
                ->where('Id_ct', $clientes[0]->tipo_cl)
                ->objectBuilder()->get('cliente_tipo');

            $sectores = $db
                ->where('Id_se', $clientes[0]->sector_cl)
                ->objectBuilder()->get('sectores');

            $fuentes = $db
                ->where('Id_fc', $clientes[0]->fuentecontacto_cl)
                ->objectBuilder()->get('fuente_contacto');

            $info = [
                'usuario' => $usuarios[0]->nombre_us,
                'identificacion' => $tipoidentifica[0]->nombre_ti . ' ' . $clientes[0]->identificacion_cl,
                'direccion' => $clientes[0]->direccion_cl,
                'sector' => $sectores[0]->nombre_se,
                'correo' => $clientes[0]->correo_cl,
                'nombre' => $clientes[0]->nombre_cl,
                'tipo' => $tipos[0]->nombre_ct,
                'telefono' => $clientes[0]->telefono_cl,
                'sitio' => $clientes[0]->sitio_cl,
                'fuente' => $fuentes[0]->nombre_fc,
                'observacion' => $clientes[0]->contactobservacion_cl,
            ];

            $informacion['info'] = $info;
        } else {
            $informacion['status'] = false;
            $informacion['msg'] = 'El cliente no existe.';
        }

        echo json_encode($informacion);
        break;
}
