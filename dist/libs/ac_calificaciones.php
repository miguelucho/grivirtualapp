<?php

require_once "conexion.php";
$data = $_REQUEST['califica'];
$informacion = array();

switch ($data['opc']) {
    case 'Listado-estudiantes':
        session_start();

        require_once 'Paginacion.php';
        $page = $data['pagina'];
        $results_pg = 50;
        $adjacent = 2;

        $matriculas = $db
            ->where('grupo_md', $data['grupo'])
            ->objectBuilder()->get('matriculas_detalles');

        $numpgs = ceil($db->count / $results_pg);

        if ($numpgs >= 1) {
            $content = '';
            $db->pageLimit = $results_pg;

            $matriculas = $db
                ->where('grupo_md', $data['grupo'])
                ->orderBy('Id_md', 'DESC')
                ->objectBuilder()->paginate('matriculas_detalles', $page);


            foreach ($matriculas as $matricula) {
                $detalle = $db
                    ->where('Id_ma', $matricula->Id_ma)
                    ->objectBuilder()->paginate('matriculas', $page);

                $actividades = $db
                    ->where('Id_gr', $data['grupo'])
                    ->objectBuilder()->get('modulos_actividades');

                $modulo = $actividades[0]->Id_mo;

                $content .= '<tr>
                                <td>
                                    <span>' . $detalle[0]->nombre_ma . ' ' . $detalle[0]->apellido_ma . '</span>
                                </td>
                                <td>' . $detalle[0]->identificacion_ma . '</td>
                                <td style="white-space: nowrap;">' . $detalle[0]->correo_ma . '</td>
                                <td><a href="administrar-calificar-actividades?gr=' . $data['grupo'] . '&mo=' . $modulo . '&es=' . $matricula->Id_ma . '" data-target="editar" class="Btn-ver Btn-table-verde"><i class="icon-list"></i>Calificar</a></td>
                            </tr>';
            }

            $informacion['list'] = $content;
            $pagconfig = array(
                'pagina' => $page,
                'totalrows' => $db->totalPages,
                'ultima_pag' => $numpgs,
                'resultados_pag' => $results_pg,
                'adyacentes' => $adjacent
            );
            $paginate = new Paginacion($pagconfig);
            $informacion['pagination'] = $paginate->crearlinks();
        } else {
            $informacion['list'] = '<tr>
                                <td colspan="5">No hay registros</td>
                            </tr>';
            $informacion['pagination'] = '';
        }

        echo json_encode($informacion);
        break;
}
