<?php

require_once "conexion.php";
$data = $_REQUEST['estudiante'];
$informacion = array();

switch ($data['opc']) {
    case 'Curso-contenido':
        session_start();

        require_once 'Paginacion.php';
        $page = $data['pagina'];
        $results_pg = 10;
        $adjacent = 2;

        $detalles_matricula = $db
            ->where('Id_md', $data['curso'])
            ->objectBuilder()->get('matriculas_detalles');

        if ($db->count > 0) {
            $nombre_docente = '';

            $grupos = $db
                ->where('Id_gr', $detalles_matricula[0]->grupo_md)
                ->objectBuilder()->get('grupos');

            if ($db->count > 0) {
                $docentes = $db
                    ->where('Id_do', $grupos[0]->Id_do)
                    ->objectBuilder()->get('docentes');

                if ($db->count > 0) {
                    $nombre_docente = $docentes[0]->nombre_do . ' ' . $docentes[0]->apellido_do;
                }
            }


            $contenidos = $db
                ->where('Id_gr', $detalles_matricula[0]->grupo_md)
                ->objectBuilder()->get('grupos_contenidos');

            $numpgs = ceil($db->count / $results_pg);

            if ($numpgs >= 1) {
                $content = '';
                $db->pageLimit = $results_pg;

                $contenidos = $db
                    ->where('Id_gr', $detalles_matricula[0]->grupo_md)
                    ->orderBy('Id_con', 'DESC')
                    ->objectBuilder()->paginate('grupos_contenidos', $page);


                foreach ($contenidos as $contenido) {
                    $content .= '<div class="Contenedor-inf-docente">
                                    <div class="Contenedor-inf-docente-int">
                                        <div class="Contenedor-bloque-docente">
                                            <div class="Contenedor-bloque-imagen">
                                                <img src="dist/assets/images/usuarios/demo.jpg" alt="">
                                            </div>
                                            <figcaption>' . $nombre_docente . '</figcaption>
                                            <div class="Contenedor-bloque-texto">
                                                ' . $contenido->descripcion_con  . '
                                            </div>
                                            <label>Fecha de ultima actualización: <strong>' . date('d-m-Y - h:i A', strtotime($contenido->fecha_con))  . '</strong></label>
                                        </div>
                                    </div>
                                </div>';
                }

                $informacion['list'] = $content;
                $pagconfig = array(
                    'pagina' => $page,
                    'totalrows' => $db->totalPages,
                    'ultima_pag' => $numpgs,
                    'resultados_pag' => $results_pg,
                    'adyacentes' => $adjacent
                );
                $paginate = new Paginacion($pagconfig);
                $informacion['pagination'] = $paginate->crearlinks();
            } else {
                $informacion['list'] = '';
                $informacion['pagination'] = '';
            }
        } else {
            $informacion['list'] = '';
            $informacion['pagination'] = '';
        }

        echo json_encode($informacion);
        break;
    case 'Contenido-info':
        $idcon = explode('-', $data['idcon']);

        $contenidos = $db
            ->where('Id_con', $idcon[1])
            ->objectBuilder()->get('grupos_contenidos', null, 'descripcion_con');

        if ($db->count > 0) {
            $informacion['info'] = $contenidos[0];
            $informacion['status'] = true;
        } else {
            $informacion['status'] = false;
            $informacion['msg'] = 'El contenido no existe.';
        }

        echo json_encode($informacion);
        break;
    case 'Foro-respuesta':
        session_start();

        $usuarios = $db
            ->where('Id_us', $_SESSION['griapp_user'])
            ->objectBuilder()->get('usuarios_app');

        if ($db->count > 0) {
            $matriculas = $db
                ->where('Id_ma', $usuarios[0]->Id_tipo)
                ->objectBuilder()->get('matriculas');

            if ($db->count > 0) {
                $datos  = [
                    'Id_mad' => $data['foro'],
                    'Id_ma' =>  $_SESSION['griapp_IDtipo'],
                    'descripcion_mafc' =>  $data['descripcion'],
                    'fecha_mafc' =>  $db->now(),
                ];

                $nuevo = $db
                    ->insert('modulo_actividades_foros_calificaciones', $datos);

                if ($nuevo) {
                    $informacion['status'] = true;
                    $informacion['msg'] = 'Comentario registrado';
                } else {
                    $informacion['status'] = false;
                    $informacion['msg'] = 'El comentario no se pudo registrar.';
                }
            }
        }

        echo json_encode($informacion);
        break;
    case 'Foro-respuestas':
        require_once 'Paginacion.php';
        $page = $data['pagina'];
        $results_pg = 50;
        $adjacent = 2;

        $respuestas = $db
            ->objectBuilder()->get('modulo_actividades_foros_calificaciones');

        $numpgs = ceil($db->count / $results_pg);

        if ($numpgs >= 1) {
            $content = '';
            $db->pageLimit = $results_pg;

            $respuestas = $db
                ->orderBy('Id_mafc', 'DESC')
                ->objectBuilder()->paginate('modulo_actividades_foros_calificaciones', $page);


            foreach ($respuestas as $respuesta) {
                $estudiante = '';

                $usuarios = $db
                    ->where('tipo_us', 2)
                    ->where('Id_tipo', $respuesta->Id_ma)
                    ->objectBuilder()->get('usuarios_app');

                if ($db->count > 0) {
                    $estudiante = $usuarios[0]->nombre_us;
                }

                $content .= '<div class="Contenedor-repuestaforo-comentario">
                                <div class="Contenedor-repuestaforo-comentario-sup">
                                    <div class="Contenedor-repuestaforo-comentario-sup-izq">
                                    <span>' . $estudiante . '</span>
                                    </div>
                                    <div class="Contenedor-repuestaforo-comentario-sup-der">
                                    <span><i class="icon-clock"></i>  ' . date('d-m-Y - h:i A', strtotime($respuesta->fecha_mafc)) . '</span>
                                    </div>
                                </div>
                                <div class="Contenedor-repuestaforo-comentario-inf">
                                    ' . $respuesta->descripcion_mafc . '
                                </div>
                                </div>';
            }

            $informacion['list'] = $content;
            $pagconfig = array(
                'pagina' => $page,
                'totalrows' => $db->totalPages,
                'ultima_pag' => $numpgs,
                'resultados_pag' => $results_pg,
                'adyacentes' => $adjacent
            );
            $paginate = new Paginacion($pagconfig);
            $informacion['pagination'] = $paginate->crearlinks();
        } else {
            $informacion['list'] = '<tr>
                                <td colspan="2">No hay registros</td>
                            </tr>';
            $informacion['pagination'] = '';
        }

        echo json_encode($informacion);
        break;
    case 'Tarea-respuesta':
        $folder = '../assets/documentos_tareas/respuestas';
        session_start();

        $usuarios = $db
            ->where('Id_us', $_SESSION['griapp_user'])
            ->objectBuilder()->get('usuarios_app');

        if ($db->count > 0) {
            $matriculas = $db
                ->where('Id_ma', $usuarios[0]->Id_tipo)
                ->objectBuilder()->get('matriculas');

            if ($db->count > 0) {
                $datos  = [
                    'Id_mad' => $data['tarea'],
                    'Id_ma' =>  $_SESSION['griapp_IDtipo'],
                    'descripcion_matc' =>  $data['descripcion'],
                    'fecha_matc' =>  $db->now(),
                ];

                $nuevo = $db
                    ->insert('modulo_actividades_tareas_calificaciones', $datos);

                if ($nuevo) {
                    if (isset($_FILES['archivo'])) {
                        for ($i = 0; $i < count($_FILES['archivo']); $i++) {
                            $mk         = date('mdYhis');
                            $nombre_archivo = $_FILES['archivo']['name'][$i];
                            $tmp_archivo    = $_FILES['archivo']['tmp_name'][$i];
                            $ext = pathinfo($nombre_archivo, PATHINFO_EXTENSION);

                            $documento  = limpiar(basename($nombre_archivo, "." . $ext)) . '.' . $ext;
                            $archivador = $folder . '/' . $mk . '_' . $documento;
                            if (move_uploaded_file($tmp_archivo, $archivador)) {
                                $datos = array(
                                    'archivo_matc' => substr($archivador, 3),
                                );

                                $nuevo = $db
                                    ->where('Id_matc', $nuevo)
                                    ->update('modulo_actividades_tareas_calificaciones', $datos);
                            }
                        }
                    }

                    $informacion['status'] = true;
                    $informacion['msg'] = 'Tarea registrada';
                } else {
                    $informacion['status'] = false;
                    $informacion['msg'] = 'La tarea no se pudo registrar.';

                    print_r($db->getLastQuery());
                }
            }
        }

        echo json_encode($informacion);
        break;
    case 'Cuestionario-respuesta':
        session_start();

        $usuarios = $db
            ->where('Id_us', $_SESSION['griapp_user'])
            ->objectBuilder()->get('usuarios_app');

        if ($db->count > 0) {
            $matriculas = $db
                ->where('Id_ma', $usuarios[0]->Id_tipo)
                ->objectBuilder()->get('matriculas');

            if ($db->count > 0) {
                foreach ($data['pregunta'] as $id => $preguntas) {
                    foreach ($preguntas as $pregunta => $respuesta) {
                        $nota = '';

                        /* $cuestionario_preguntas = $db
                            ->where('Id_macp', $pregunta)
                            ->objectBuilder()->get('modulo_actividades_cuestionario_preguntas');


                        if ($cuestionario_preguntas[0]->tipo_macp == 1 || $cuestionario_preguntas[0]->tipo_macp == 2) {
                            $valor_respuesta = $cuestionario_preguntas[0]->valor_macp / $db->count;

                            if ($respuesta == 'V') {
                                $nota += $valor_respuesta;
                            }
                        } */

                        $datos  = [
                            'Id_mad' => $data['cuestionario'],
                            'Id_ma' =>  $_SESSION['griapp_IDtipo'],
                            'Id_macp' => $pregunta,
                            'respuesta_macc' => $respuesta,
                            'fecha_macc' =>  $db->now(),
                            'calificacion_macc' => $nota
                        ];

                        $nuevo = $db
                            ->insert('modulo_actividades_cuestionario_calificaciones', $datos);

                        if ($nuevo) {
                            $informacion['status'] = true;
                            $informacion['msg'] = 'Cuestionario registrado';
                        } else {
                            $informacion['status'] = false;
                            $informacion['msg'] = 'El cuestionario no se pudo registrar.';
                        }
                    }
                }
            }
        }

        echo json_encode($informacion);
        break;
}


function firstXChars($string, $chars = 500)
{
    preg_match('/^.{0,' . $chars . '}(?:.*?)\b/iu', $string, $matches);
    return $matches[0] . '...';
}


function limpiar($String)
{
    $String = preg_replace("/[^A-Za-z0-9\_\-]/", '', $String);
    return $String;
}
