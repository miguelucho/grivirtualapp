<?php

require_once "conexion.php";
$data   = $_REQUEST['login'];
$informacion = array();

switch ($data['opc']) {
    case 'User-login':
        $login = $data['usuario'];

        $check = $db
            ->where('login_us', $login)
            ->objectBuilder()->get('usuarios_app');
        if ($db->count > 0) {
            if (password_verify($data['contrasena'], $check[0]->password_us)) {
                session_start();

                $_SESSION['griapp_user'] = $check[0]->Id_us;
                $_SESSION['griapp_tipo'] = $check[0]->tipo_us;
                $_SESSION['griapp_IDtipo'] = $check[0]->Id_tipo;

                $db
                    ->where('Id_us', $check[0]->Id_us)
                    ->update('usuarios_app', ['ultimo_ingreso_us' => $db->now()]);

                $msg['status']   = true;

                if($check[0]->tipo_us == 1){
                    $msg['redirect'] = 'administrar-modulos';
                }

                if($check[0]->tipo_us == 2){
                    $msg['redirect'] = 'estudiante-home';
                }
            } else {
                $msg['status'] = false;
                $msg['msg']    = 'Usuario o contraseña incorrecto.';
            }
        } else {
            $msg['status'] = false;
            $msg['msg']    = 'Error, el usuario no existe';
        }

        echo json_encode($msg);
        break;
    case 'Reset-pass':
        $login = $data['usuario'];

        $check = $db
            ->where('login_us', $login)
            ->objectBuilder()->get('usuarios_app');

        if ($db->count > 0) {
            include "Password.php";

            $npass     = RandomPassword();
            $tmppass   = password_hash($npass, PASSWORD_BCRYPT);
            $actualiza = $db
                ->where('Id_us', $check[0]->Id_us)
                ->update('usuarios_app', ['password_us' => $tmppass]);



            include_once 'phpmailer/class.phpmailer.php';

            $message = file_get_contents('templates/template-restablecer-contrasena.php');
            $message = str_replace('$usuario$', $check[0]->nombre_us, $message);
            $message = str_replace('$email$', $check[0]->login_us, $message);
            $message = str_replace('$password$', $npass, $message);
            $mail = new PHPMailer();

            $mail->Host = "localhost";
            $mail->SetFrom('noreply@gricompany.com');
            $mail->AddAddress($check[0]->login_us);
            $mail->Subject = 'Restablecer Contraseña Gricompany Virtual';
            $mail->MsgHTML($message);
            $mail->IsHTML(true);
            $mail->CharSet = "utf-8";

            if ($mail->Send()) {
                $msg['status']   = true;
                $msg['msg'] = 'Se te ha enviado un email con tu nueva contraseña.';
            } else {
                 $msg['status'] = false;
                $msg['msg'] = 'Error';
            }
        } else {
            $msg['status'] = false;
            $msg['msg']    = 'Error, el usuario no existe';
        }
        echo json_encode($msg);
        break;
}

function RandomPassword($length = 6)
{
    $chars    = "abcdefghijkmnpqrstuvwxyz23456789";
    $charsLen = strlen($chars);
    $pass     = '';
    for ($i = 0; $i < $length; $i++) {
        $pass .= substr($chars, mt_rand(0, $charsLen - 1), 1);
    }
    return $pass;
}
