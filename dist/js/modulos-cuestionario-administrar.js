$(document).ready(function () {
    preguntas = $('.Contenedor-pregunta').length;
    eliminar_preguntas = [];
    eliminar_respuestas = [];
    guardar_correcto = 1;


    $('body').on('click', '.select-wrapper', function () {
        pregunta_seleccionada = $('.Select-tipo-pregunta', this).val();
    });

    $('body').on('change', '.Select-tipo-pregunta', function () {
        contenedor_pregunta = $(this).closest('.Contenedor-pregunta');

        if ($(this).val() == 3) {
            contenedor_pregunta.find('.Select-tipo-respuesta').attr('disabled', true);
            $('.Bloque-respuesta', contenedor_pregunta).hide();
            $('.Bloque-respuesta textarea', contenedor_pregunta).attr('required', false)
            $('.Input-clave', contenedor_pregunta).remove();
        } else if ($(this).val() == 4) {
            indx = contenedor_pregunta.data('pregunta');
            contenedor_pregunta.find('.Select-tipo-respuesta').attr('disabled', true);
            $('.Sel-respuesta', contenedor_pregunta).hide();
            $('.Bloque-respuesta textarea', contenedor_pregunta).attr('required', false)
            $('.Sel-respuesta', contenedor_pregunta).after('<div class="input-field Input-clave">' +
                '<input type="text" name="cuestionario[palabra][' + indx + '][]">' +
                '<label>Palabra</label>' +
                '</div>');
        } else {
            contenedor_pregunta.find('.Select-tipo-respuesta').attr('disabled', false);
            $('.Bloque-respuesta', contenedor_pregunta).show();
            $('.Sel-respuesta', contenedor_pregunta).show();
            $('.Bloque-respuesta textarea', contenedor_pregunta).attr('required', true);
            $('.Input-clave', contenedor_pregunta).remove();
        }

        if ($(this).val() == 3 && $('.Bloque-respuesta', contenedor_pregunta).length > 0) {

            contenedor_respuesta = contenedor_pregunta.find('.Temp-respuesta');
            pregunta = $(this);

            var n = new Noty({
                text: 'Este tipo de pregunta no admite respuestas, se eliminarán todas las respuestas vinculadas, desea continuar?',
                buttons: [
                    Noty.button('Si', 'btn btn-success', function () {
                        contenedor_respuesta.empty();
                        $('.Bloque-respuesta textarea', contenedor_pregunta).val('');
                        n.close();
                    }, { id: 'button1', 'data-status': 'ok' }),

                    Noty.button('No', 'btn btn-error', function () {
                        pregunta.val(pregunta_seleccionada).change();
                        n.close();
                    })
                ],
                layout: 'center',
                theme: 'relax',
                modal: true,
            });
            n.show();
        }

        $("select").formSelect();
    });

    $('body').on('click', '.Eliminar-pregunta', function () {
        pregunta_eliminar = $(this).closest('.Contenedor-pregunta');
        fila = $(this).prop('id').split('-');
        var n = new Noty({
            text: 'Desea eliminar la pregunta?',
            buttons: [
                Noty.button('Si', 'btn btn-success', function () {
                    pregunta_eliminar.remove();
                    eliminar_preguntas.push(fila[1]);
                    Valorar();
                    n.close();
                }, { id: 'button1', 'data-status': 'ok' }),

                Noty.button('No', 'btn btn-error', function () {
                    n.close();
                })
            ],
            layout: 'center',
            theme: 'relax',
            modal: true,
        });
        n.show();
    });

    $('body').on('click', '.Eliminar-respuesta', function () {
        respuesta_eliminar = $(this).closest('.Contenedor-formularios-bloque');
        fila = $(this).prop('id').split('-');
        var n = new Noty({
            text: 'Desea eliminar la respuesta?',
            buttons: [
                Noty.button('Si', 'btn btn-success', function () {
                    respuesta_eliminar.remove();
                    eliminar_respuestas.push(fila[1]);
                    n.close();
                }, { id: 'button1', 'data-status': 'ok' }),

                Noty.button('No', 'btn btn-error', function () {
                    n.close();
                })
            ],
            layout: 'center',
            theme: 'relax',
            modal: true,
        });
        n.show();
    });

    n = 3;

    $('body').on('click', '.Mas-respuesta', function () {
        contenedor_pregunta = $(this).closest('.Contenedor-pregunta');
        contenedor_respuesta = contenedor_pregunta.find('.Temp-respuesta');

        if (contenedor_pregunta.find('.Select-tipo-pregunta').val() != '3') {
            indx = contenedor_pregunta.data('pregunta');
            contenedor_respuesta.append('<div class="Contenedor-formularios-bloque Bloque-respuesta">' +
                '<div class="Colum-uno">' +
                '<div class="input-field">' +
                '<textarea id="textarea' + (n + 1) + '" class="materialize-textarea" name="cuestionario[respuesta][' + indx + '][]" required></textarea>' +
                '<label for="textarea' + (n + 1) + '">Descripción de la respuesta</label>' +
                '</div>' +
                '</div>' +
                '<div class="Colum-cuatro">' +
                '<div class="input-field">' +
                '<select name="cuestionario[respuesta_tipo][' + indx + '][]" class="Select-tipo-respuesta Temp-select-respuesta">' +
                '<option value="" disabled selected>Selecciona</option>' +
                '</select>' +
                '<label>Tipo de respuesta</label>' +
                '</div>' +
                '</div>' +
                '<div class="Colum-cuatro">' +
                '<a href="javascript:void(0)" data-position="right" data-tooltip="Eliminar respuesta" class="tooltipped btn-floating btn-large waves-effect waves-light  red darken-4 Eliminar-respuesta"><i class="material-icons">delete</i></a>' +
                '</div>' +
                '</div>');


            $('.Oculto-respuesta option').clone().appendTo('.Temp-select-respuesta');
            $('.Temp-select-respuesta').removeClass('Temp-select-respuesta');
            $("select").formSelect();
            n++;
        } else if (contenedor_pregunta.find('.Select-tipo-pregunta').val() == null) {
            notification('warning', 'Debe seleccionar un tipo de pregunta');
        } else {
            notification('warning', 'No se puede añadir una respuesta para el tipo de pregunta seleccionado');
        }

    });


    $('body').on('click', '.Mas-pregunta', function () {
        $('.Temp-pregunta').append('<div class="Contenedor-pregunta" data-pregunta="' + preguntas + '">' +
            '<div class="Contenedor-pregunta-top">' +
            '<a href="javascript:void(0)" data-position="left" data-tooltip="Eliminar pregunta" class="tooltipped Eliminar-pregunta"><i class="icon-bin"></i></a>' +
            '</div>' +
            '<div class="Contenedor-formularios-bloque">' +
            '<div class="Colum-uno">' +
            '<div class="input-field">' +
            '<textarea id="textarea' + n + '" name="cuestionario[pregunta][' + preguntas + ']" class="materialize-textarea" required></textarea>' +
            '<label for="textarea' + n + '">Descripción de la pregunta</label>' +
            '</div>' +
            '</div>' +
            '<div class="Colum-cuatro">' +
            '<div class="input-field">' +
            '<select name="cuestionario[pregunta_tipo][' + preguntas + ']" class="Select-tipo-pregunta Temp-select-pregunta">' +
            '<option value="" disabled selected>Selecciona</option>' +
            '</select>' +
            '<label>Tipo de pregunta</label>' +
            '</div>' +
            '</div>' +
            '<div class="Colum-cuatro">' +
            '<div class="input-field">' +
            '<input id="Valor_' + (n + 1) + '" type="number" name="cuestionario[pregunta_valor][' + preguntas + ']" class="validate Valor-pregunta" required>' +
            '<label for="Valor_' + (n + 1) + '">Valor máximo</label>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="Contenedor-formularios-bloque Bloque-respuesta">' +
            '<div class="Colum-uno">' +
            '<div class="input-field">' +
            '<textarea id="textarea' + (n + 1) + '" class="materialize-textarea" name="cuestionario[respuesta][' + preguntas + '][]" required></textarea>' +
            '<label for="textarea' + (n + 1) + '">Descripción de la respuesta</label>' +
            '</div>' +
            '</div>' +
            '<div class="Colum-cuatro Sel-respuesta">' +
            '<div class="input-field">' +
            '<select name="cuestionario[respuesta_tipo][' + preguntas + '][]" class="Select-tipo-respuesta Temp-select-respuesta">' +
            '<option value="" disabled selected>Selecciona</option>' +
            '</select>' +
            '<label>Tipo de respuesta</label>' +
            '</div>' +
            '</div>' +
            '<div class="Colum-cuatro">' +
            '</div>' +
            '</div>' +
            '<div class="Temp-respuesta"></div>' +
            '<div class="Contenedor-formularios-bloque">' +
            '<div class="Colum-cuatro">' +
            '<a href="javascript:void(0)" data-position="right" data-tooltip="Nueva respuesta" class="Agregar-archivo tooltipped btn-floating btn-large waves-effect waves-light blue-grey darken-4 Mas-respuesta"><i class="material-icons">add</i></a>' +
            '</div>' +
            '</div>' +
            '</div>');

        $('.Oculto-pregunta option').clone().appendTo('.Temp-select-pregunta');
        $('.Temp-select-pregunta').removeClass('Temp-select-pregunta');

        $('.Oculto-respuesta option').clone().appendTo('.Temp-select-respuesta');
        $('.Temp-select-respuesta').removeClass('Temp-select-respuesta');
        $("select").formSelect();
        preguntas++;
        n++;
    });

    $('body').on('keyup', '.Valor-pregunta', function () {
        Valorar();
    });

    $('body').on('change', '.Select-tipo-respuesta', function () {
        contenedor_pregunta = $(this).closest('.Contenedor-pregunta');

        if (contenedor_pregunta.find('.Select-tipo-pregunta').val() == 1) {
            unica = 0;
            $.each($('.Select-tipo-respuesta', contenedor_pregunta), function () {
                if ($(this).val() == 1) {
                    unica++;
                }
            });

            if (unica > 1) {
                notification('warning', 'Este tipo de pregunta solo admite una respuesta verdadera');
                $(this).val('');
                $("select").formSelect();
            }
        }

    });

    $('#Administra-cuestionario').on('submit', function (e) {
        e.preventDefault();
        Valorar();
        if (guardar_correcto == 1) {
            loader();
            data = $(this).serializeArray();

            data.push({
                name: 'cuestionario[opc]',
                value: 'Administrar'
            });

            $.each(eliminar_preguntas, function (i, dat) {
                data.push({
                    name: 'cuestionario[eliminar-pregunta][]',
                    value: dat
                });
            });

            $.each(eliminar_respuestas, function (i, dat) {
                data.push({
                    name: 'cuestionario[eliminar-respuesta][]',
                    value: dat
                });
            });

            $.post('dist/libs/ac_cuestionario', data, function (data) {
                $('#fondo').remove();
                if (data.status == true) {
                    notification('success', data.msg);
                    setTimeout(function () {
                        location.reload();
                    }, 2000);
                } else {
                    notification('error', data.msg);
                }
            }, 'json');
        } else {
            notification('warning', 'La sumatoria del valor de las preguntas no debe ser superior a 5');
        }
    });

    function Valorar() {
        total_valor = 0;
        $.each($('.Valor-pregunta'), function () {
            total_valor += Number($(this).val());
        });

        $('.Valor-actual').html(total_valor);

        if (total_valor > 5) {
            notification('warning', 'La sumatoria del valor de las preguntas no debe ser superior a 5');
            guardar_correcto = 0;
        } else {
            guardar_correcto = 1;
        }
    }

    function loader() {
        $('#fondo').remove();
        $('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
        $('#fondo').append('<div class="loader preloader-wrapper big active">' +
            '<div class="spinner-layer">' +
            '<div class="circle-clipper left">' +
            '<div class="circle"></div>' +
            '</div><div class="gap-patch">' +
            '<div class="circle"></div>' +
            '</div><div class="circle-clipper right">' +
            '<div class="circle"></div>' +
            '</div>' +
            '</div>' +
            '</div>');
        setTimeout(function () {
            $('#fondo').fadeIn('fast');
        }, 100);
    }

    function notification(t, m) {
        var n = new Noty({
            type: t,
            text: m,
            layout: 'center',
            theme: 'relax',
            modal: true,
            closeWith: ['click'],
            progressBar: false,
            timeout: 1500,
        });
        n.show();
    }

});
