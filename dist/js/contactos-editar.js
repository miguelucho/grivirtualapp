$(document).ready(function () {

    setTimeout(function () {
        if ($('.select-usuarios').val() != '') {
            $('.select-usuarios').change();
        }
    }, 400);

    $('.select-usuarios').on('change', function () {
        usuario_selected = $(this).val();
        select_clientes = $('.select-clientes');
        $('option:not(:first)', select_clientes).remove();
        select_clientes.formSelect('destroy');
        $.each($('option', '#clientes-oculto'), function () {
            if ($(this).hasClass('Us-' + usuario_selected)) {
                $(this).clone().appendTo(select_clientes);
            }
        });

        select_clientes.formSelect();
    });

    $('#Contacto-editar').on('submit', function (e) {
        e.preventDefault();
        select_clientes = $('.select-clientes');
        data = $(this).serializeArray();
        data.push({
            name: 'contacto[opc]',
            value: 'Editar'
        });
        $.post('dist/libs/ac_contactos', data, function (data) {
            if (data.status == true) {
                notification('success', data.msg);
            } else {
                notification('error', data.msg);
            }
        }, 'json');
    });

    function notification(t, m) {
        var n = new Noty({
            type: t,
            text: m,
            layout: 'center',
            theme: 'relax',
            modal: true,
            closeWith: ['click'],
            progressBar: false,
            timeout: 1500,
        });
        n.show();
    }
});
