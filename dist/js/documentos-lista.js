$(document).ready(function () {
    var bsq_cliente= '';

    listado(1);

    $('body').on('click', '.mpag', function () {
        listado($(this).prop('id'));
    });

    $('#busqueda').on('submit', function (e) {
        e.preventDefault();
        bsq_cliente = $('#Empresa-bus').val();
        listado(1);
    });

    $('body').on('click', '.Btn-eliminar', function () {
        documento = $(this).closest('tr').prop('id');
        var n = new Noty({
            text: 'Desea eliminar el documento?',
            buttons: [
                Noty.button('Si', 'btn btn-success', function () {
                    $.post('dist/libs/ac_documentos', {
                        'documento[opc]': 'Eliminar',
                        'documento[id]': documento,
                    }, function (data) {
                        n.close();
                        if (data.status == true) {
                            listado(1);
                        } else {
                            notification('error', data.msg);
                        }
                    }, 'json');
                }, {id: 'button1', 'data-status': 'ok'}),

                Noty.button('No', 'btn btn-error', function () {
                    n.close();
                })
            ],
            layout: 'center',
            theme: 'relax',
            modal: true,
        });
        n.show();
    });

    function listado(pg) {
        $('.listado-loader').fadeIn(0);
        $('#Listado-documentos').empty().fadeOut(0);
        $.post('dist/libs/ac_documentos', {
            'documento[opc]': 'Listado',
            'documento[cliente]': bsq_cliente,
            'documento[pagina]': pg
        }, function (data) {
            $('#Listado-documentos').html(data.list);
            $('.listado-paginacion ul').html(data.pagination);
            $('.listado-loader').fadeOut(0);
            $('#Listado-documentos').fadeIn();
        }, 'json');
    }

    function notification(t, m) {
        var n = new Noty({
            type: t,
            text: m,
            layout: 'center',
            theme: 'relax',
            modal: true,
            closeWith: ['click'],
            progressBar: false,
            timeout: 1500,
        });
        n.show();
    }
});
