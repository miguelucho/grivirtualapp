$(document).ready(function () {
    Valorar();

    $('.Nota-respuesta').on('keyup', function () {
        Valorar();
    });

    function Valorar() {
        final = 0;

        $.each($('.Nota-respuesta'), function () {
            final += Number($(this).val());
        });

        $('.Nota-final').html(final);

        if (final > 5) {
            notification('warning', 'La sumatoria del valor de las notas no debe ser superior a 5');
            guardar_correcto = 0;
        } else {
            guardar_correcto = 1;
        }
    }

    $('#Calificar-cuestionario').on('submit', function (e) {
        e.preventDefault();
        loader();

        data = $(this).serializeArray();

        data.push({
            name: 'cuestionario[opc]',
            value: 'Calificar'
        });

        $.post('dist/libs/ac_cuestionario', data, function (data) {
            $('#fondo').remove();
            if (data.status == true) {
                notification('success', data.msg);
            } else {
                notification('error', data.msg);
            }
        }, 'json');
    });

    function loader() {
        $('#fondo').remove();
        $('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
        $('#fondo').append('<div class="loader preloader-wrapper big active">' +
            '<div class="spinner-layer">' +
            '<div class="circle-clipper left">' +
            '<div class="circle"></div>' +
            '</div><div class="gap-patch">' +
            '<div class="circle"></div>' +
            '</div><div class="circle-clipper right">' +
            '<div class="circle"></div>' +
            '</div>' +
            '</div>' +
            '</div>');
        setTimeout(function () {
            $('#fondo').fadeIn('fast');
        }, 100);
    }

    function notification(t, m) {
        var n = new Noty({
            type: t,
            text: m,
            layout: 'center',
            theme: 'relax',
            modal: true,
            closeWith: ['click'],
            progressBar: false,
            timeout: 1500,
        });
        n.show();
    }

});
