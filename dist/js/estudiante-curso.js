$(document).ready(function () {

    listado(1);

    function listado(pg) {
        $('.listado-loader').fadeIn(0);
        $('#Listado-contenidos').empty().fadeOut(0);
        $.post('dist/libs/ac_estudiantes', {
            'estudiante[opc]': 'Curso-contenido',
            'estudiante[curso]': $('#idcurso').val(),
            'estudiante[pagina]': pg
        }, function (data) {
            $('#Listado-contenidos').html(data.list);
            $('.listado-paginacion ul').html(data.pagination);
            $('.listado-loader').fadeOut(0);
            $('#Listado-contenidos').fadeIn();
        }, 'json');
    }

    $('body').on('click', '.mpag', function () {
        listado($(this).prop('id'));
    });

});
