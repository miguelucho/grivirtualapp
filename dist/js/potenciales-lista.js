$(document).ready(function () {
    var bsq_nombre = '',
        bsq_identifica = '';

    listado(1);

    $('body').on('click', '.mpag', function () {
        listado($(this).prop('id'));
    });

    $('#busqueda').on('submit', function (e) {
        e.preventDefault();
        bsq_nombre = $('#Nombre-bus').val();
        bsq_identifica = $('#Identificacion-bus').val();
        listado(1);
    });

    $('body').on('click', '.Btn-convertir', function () {
        potencial = $(this).closest('tr').prop('id');
        var n = new Noty({
            text: 'Desea convertir el cliente potencial?',
            buttons: [
                Noty.button('Si', 'btn btn-success', function () {
                    $.post('dist/libs/ac_potenciales', {
                        'clientepot[opc]': 'Convertir',
                        'clientepot[id]': potencial,
                    }, function (data) {
                        n.close();
                        if (data.status == true) {
                            listado(1);
                        } else {
                            notification('error', data.msg);
                        }
                    }, 'json');
                }, { id: 'button1', 'data-status': 'ok' }),

                Noty.button('No', 'btn btn-error', function () {
                    n.close();
                })
            ],
            layout: 'center',
            theme: 'relax',
            modal: true,
        });
        n.show();
    });

    $('body').on('click', '.Btn-ver', function () {
        cliente = $(this).closest('tr').prop('id');
        $('.Info-usuario').html('');
        $('.Info-identificacion').html('');
        $('.Info-direccion').html('');
        $('.Info-sector').html('');
        $('.Info-correo').html('');
        $('.Info-nombre').html('');
        $('.Info-tipo').html('');
        $('.Info-telefono').html('');
        $('.Info-sitio').html('');
        $('.Info-fuente').html('');
        $('.Info-observacion').html('');

        data = [];
        data.push({
            name: 'clientepot[opc]',
            value: 'Info'
        }, {
            name: 'clientepot[idcliente]',
            value: cliente
        });
        $.post('dist/libs/ac_potenciales', data, function (data) {
            if (data.status == true) {
                $('.Info-usuario').html(data.info.usuario);
                $('.Info-identificacion').html(data.info.identificacion);
                $('.Info-direccion').html(data.info.direccion);
                $('.Info-sector').html(data.info.sector);
                $('.Info-correo').html(data.info.correo);
                $('.Info-nombre').html(data.info.nombre);
                $('.Info-tipo').html(data.info.tipo);
                $('.Info-telefono').html(data.info.telefono);
                $('.Info-sitio').html(data.info.sitio);
                $('.Info-fuente').html(data.info.fuente);
                $('.Info-observacion').html(data.info.observacion);
                $('#Ver-info').modal('open');
            } else if (data.status == false) {
                notification('error', data.msg);
            }
        }, 'json');
    });

    $('body').on('click', '.Btn-bloquear', function () {
        potencial = $(this).closest('tr').prop('id');
        if ($(this).hasClass('Activar')) {
            tipo_activacion = 'desbloquear';
        } else {
            tipo_activacion = 'bloquear';
        }
        var n = new Noty({
            text: 'Desea ' + tipo_activacion + ' el cliente potencial?',
            buttons: [
                Noty.button('Si', 'btn btn-success', function () {
                    $.post('dist/libs/ac_potenciales', {
                        'clientepot[opc]': 'Bloqueo',
                        'clientepot[id]': potencial,
                    }, function (data) {
                        n.close();
                        if (data.status == true) {
                            listado(1);
                        } else {
                            notification('error', data.msg);
                        }
                    }, 'json');
                }, { id: 'button1', 'data-status': 'ok' }),

                Noty.button('No', 'btn btn-error', function () {
                    n.close();
                })
            ],
            layout: 'center',
            theme: 'relax',
            modal: true,
        });
        n.show();
    });

    function listado(pg) {
        $('.listado-loader').fadeIn(0);
        $('#Listado-potenciales').empty().fadeOut(0);
        $.post('dist/libs/ac_potenciales', {
            'clientepot[opc]': 'Listado',
            'clientepot[nombre]': bsq_nombre,
            'clientepot[identificacion]': bsq_identifica,
            'clientepot[pagina]': pg
        }, function (data) {
            $('#Listado-potenciales').html(data.list);
            $('.listado-paginacion ul').html(data.pagination);
            $('.listado-loader').fadeOut(0);
            $('#Listado-potenciales').fadeIn();
        }, 'json');
    }

    function notification(t, m) {
        var n = new Noty({
            type: t,
            text: m,
            layout: 'center',
            theme: 'relax',
            modal: true,
            closeWith: ['click'],
            progressBar: false,
            timeout: 1500,
        });
        n.show();
    }
});
