$(document).ready(function () {
    $('#Cliente-editar').on('submit', function (e) {
        e.preventDefault();
        data = $(this).serializeArray();
        data.push({
            name: 'empresa[opc]',
            value: 'Editar'
        });
        $.post('dist/libs/ac_clientes', data, function (data) {
            if (data.status == true) {
                notification('success', data.msg);
            } else {
                notification('error', data.msg);
            }
        }, 'json');
    });

    function notification(t, m) {
        var n = new Noty({
            type: t,
            text: m,
            layout: 'center',
            theme: 'relax',
            modal: true,
            closeWith: ['click'],
            progressBar: false,
            timeout: 1500,
        });
        n.show();
    }
});
