$(document).ready(function () {
    var bsq_programa = '',
        bsq_curso = '';
    bsq_codigo = '';

    listado(1);

    $('.select-programas').on('change', function () {
        programa_selected = $(this).val();
        select_cursos = $(this).closest('form').find('.select-cursos');
        $('option:not(:first)', select_cursos).remove();
        select_cursos.formSelect('destroy');
        $.each($('option', '#cursos-oculto'), function () {
            if ($(this).hasClass('Pr-' + programa_selected)) {
                $(this).clone().appendTo(select_cursos);
            }
        });

        select_cursos.formSelect();
    });

    $('#busqueda').on('submit', function (e) {
        e.preventDefault();
        bsq_programa = $('#Programa-bus').val();
        bsq_curso = $('#Curso-bus').val();
        bsq_codigo = $('#Codigo-bus').val();
        listado(1);
    });

    function listado(pg) {
        $('.listado-loader').fadeIn(0);
        $('#Listado-asignados').empty().fadeOut(0);
        $.post('dist/libs/ac_asignados', {
            'grupo[opc]': 'Asignados-calificaciones',
            'grupo[programa]': bsq_programa,
            'grupo[curso]': bsq_curso,
            'grupo[codigo]': bsq_codigo,
            'grupo[pagina]': pg
        }, function (data) {
            $('#Listado-asignados').html(data.list);
            $('.listado-paginacion ul').html(data.pagination);
            $('.listado-loader').fadeOut(0);
            $('#Listado-asignados').fadeIn();
        }, 'json');
    }

    $('body').on('click', '.mpag', function () {
        listado($(this).prop('id'));
    });

});
