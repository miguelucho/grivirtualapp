$(document).ready(function () {
    tipo_editar = '';

    $('#tipo').modal({
        onCloseEnd() {
            tipo_editar = '';
            $('#Modal-titulo').html('Crear');
            $('input').not('[type=submit]').val('');
            $("select").val('').attr('selected', true);
            window.M.updateTextFields();
            $('select').formSelect();
        }
    });

    listado(1);

    $('body').on('click', '.mpag', function () {
        listado($(this).prop('id'));
    });

    $('#Tipo-nuevo').on('submit', function (e) {
        e.preventDefault();
        loader();
        data = $(this).serializeArray();
        if (tipo_editar == '') {
            data.push({
                name: 'ajuste[opc]',
                value: 'Nuevo'
            });
        } else {
            data.push({
                name: 'ajuste[opc]',
                value: 'Editar'
            }, {
                name: 'ajuste[idfuente]',
                value: tipo_editar
            });
        }
        $.post('dist/libs/ac_tiposervicio', data, function (data) {
            $('#fondo').remove();
            if (data.status == true) {
                if (tipo_editar == '') {
                    $('input').not('[type=submit]').val('');
                    $("select").val('').attr('selected', true);
                    window.M.updateTextFields();
                    $('select').formSelect();
                    tipo_editar = '';
                }
                listado(1);
                notification('success', data.msg);
            } else {
                notification('error', data.msg);
            }
        }, 'json');
    });

    $('body').on('click', '.Btn-editar', function () {
        loader();
        $('#Modal-titulo').html('Editar');
        tipo_editar = $(this).closest('tr').prop('id');
        data = $(this).serializeArray();
        data.push({
            name: 'ajuste[opc]',
            value: 'Info'
        }, {
            name: 'ajuste[idfuente]',
            value: tipo_editar
        });
        $.post('dist/libs/ac_tiposervicio', data, function (data) {
            $('#fondo').remove();
            if (data.status == true) {
                $.each(data.info, function (i, dat) {
                    $('#Nombre').val(dat.nombre_ts);
                    $('#Estado').val(dat.estado_ts);
                    window.M.updateTextFields();
                    M.FormSelect.init(document.querySelectorAll("select"));
                });
                $('#tipo').modal('open');
            } else if (data.status == false) {
                notification('error', data.msg);
            }
        }, 'json');
    });

    $('body').on('click', '.Btn-bloquear', function () {
        tipo = $(this).closest('tr').prop('id');
        if ($(this).hasClass('Activar')) {
            tipo_activacion = 'desbloquear';
        } else {
            tipo_activacion = 'bloquear';
        }
        var n = new Noty({
            text: 'Desea ' + tipo_activacion + ' origen del negocio?',
            buttons: [
                Noty.button('Si', 'btn btn-success', function () {
                    $.post('dist/libs/ac_tiposervicio', {
                        'ajuste[opc]': 'Bloqueo',
                        'ajuste[id]': tipo,
                    }, function (data) {
                        n.close();
                        if (data.status == true) {
                            listado(1);
                        } else {
                            notification('error', data.msg);
                        }
                    }, 'json');
                }, { id: 'button1', 'data-status': 'ok' }),

                Noty.button('No', 'btn btn-error', function () {
                    n.close();
                })
            ],
            layout: 'center',
            theme: 'relax',
            modal: true,
        });
        n.show();
    });

    function listado(pg) {
        $('.listado-loader').fadeIn(0);
        $('#Listado-tipos').empty().fadeOut(0);
        $.post('dist/libs/ac_tiposervicio', {
            'ajuste[opc]': 'Listado',
            'ajuste[pagina]': pg
        }, function (data) {
            $('#Listado-tipos').html(data.list);
            $('.listado-paginacion ul').html(data.pagination);
            $('.listado-loader').fadeOut(0);
            $('#Listado-tipos').fadeIn();
        }, 'json');
    }

    function loader() {
        $('#fondo').remove();
        $('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
        $('#fondo').append('<div class="loader preloader-wrapper big active">' +
            '<div class="spinner-layer">' +
            '<div class="circle-clipper left">' +
            '<div class="circle"></div>' +
            '</div><div class="gap-patch">' +
            '<div class="circle"></div>' +
            '</div><div class="circle-clipper right">' +
            '<div class="circle"></div>' +
            '</div>' +
            '</div>' +
            '</div>');
        setTimeout(function () {
            $('#fondo').fadeIn('fast');
        }, 100);
    }

    function notification(t, m) {
        var n = new Noty({
            type: t,
            text: m,
            layout: 'center',
            theme: 'relax',
            modal: true,
            closeWith: ['click'],
            progressBar: false,
            timeout: 1500,
        });
        n.show();
    }
});
