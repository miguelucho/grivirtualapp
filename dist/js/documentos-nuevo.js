$(document).ready(function () {

    $('.select-usuarios').on('change', function () {
        usuario_selected = $(this).val();
        select_clientes = $('.select-clientes');
        $('option:not(:first)', select_clientes).remove();
        select_clientes.formSelect('destroy');
        $.each($('option', '#clientes-oculto'), function () {
            if ($(this).hasClass('Us-' + usuario_selected)) {
                $(this).clone().appendTo(select_clientes);
            }
        });

        select_clientes.formSelect();
    });

    $('#Documento-nuevo').on('submit', function (e) {
        e.preventDefault();
        loader();
        select_clientes = $('.select-clientes');
        var data = new FormData();
        var inputFileImage = document.getElementById('documento_adjunto');
        var file = inputFileImage.files[0];
        data.append('archivo', file);

        data.append('documento[opc]', 'Nuevo');
        otradata = $(this).serializeArray();
        $.each(otradata, function (key, input) {
            data.append(input.name, input.value);
        });

        $.ajax({
            url: 'dist/libs/ac_documentos',
            type: 'POST',
            contentType: false,
            data: data,
            processData: false,
            cache: false,
            dataType: 'json',
            success: function (data) {
                $('#fondo').remove();
                if (data.status == true) {
                    $('input').not('[type=submit]').val('');
                    $("select").val('').attr('selected', true);
                    $('.select-usuarios').prop('selectedIndex', 0);
                    $('option:not(:first)', select_clientes).remove();
                    select_clientes.formSelect('destroy');
                    window.M.updateTextFields();
                    $('select').formSelect();
                    notification('success', data.msg);
                } else {
                    notification('error', data.msg);
                }
            }
        });
    });

    function loader() {
        $('#fondo').remove();
        $('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
        $('#fondo').append('<div class="loader preloader-wrapper big active">' +
            '<div class="spinner-layer">' +
            '<div class="circle-clipper left">' +
            '<div class="circle"></div>' +
            '</div><div class="gap-patch">' +
            '<div class="circle"></div>' +
            '</div><div class="circle-clipper right">' +
            '<div class="circle"></div>' +
            '</div>' +
            '</div>' +
            '</div>');
        setTimeout(function () {
            $('#fondo').fadeIn('fast');
        }, 100);
    }

    function notification(t, m) {
        var n = new Noty({
            type: t,
            text: m,
            layout: 'center',
            theme: 'relax',
            modal: true,
            closeWith: ['click'],
            progressBar: false,
            timeout: 1500,
        });
        n.show();
    }
});
