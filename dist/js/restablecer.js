$(document).ready(function() {

	$('#login').on('submit', function(e) {
		e.preventDefault();
		loader();
		$('#Usuario-error span').html('');
		data = $(this).serializeArray();
		data.push({
			name: 'login[opc]',
			value: 'Reset-pass'
		});
		$.post('dist/libs/ac_login', data, function(data) {
			$('#fondo').remove();
			if (data.status == true) {
				notification('success', data.msg);
			} else if (data.status == false) {
				$('#Usuario-error span').html(data.msg);
			}
		}, 'json');
	});

	function loader() {
		$('#fondo').remove();
		$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
		$('#fondo').append('<div class="loader preloader-wrapper big active">' +
			'<div class="spinner-layer">' +
			'<div class="circle-clipper left">' +
			'<div class="circle"></div>' +
			'</div><div class="gap-patch">' +
			'<div class="circle"></div>' +
			'</div><div class="circle-clipper right">' +
			'<div class="circle"></div>' +
			'</div>' +
			'</div>' +
			'</div>');
		setTimeout(function() {
			$('#fondo').fadeIn('fast');
		}, 100);
	}

	function notification(t, m) {
        var n = new Noty({
            type: t,
            text: m,
            layout: 'center',
            theme: 'relax',
            modal: true,
            closeWith: ['click'],
            progressBar: false,
            timeout: 1500,
        });
        n.show();
    }
});
