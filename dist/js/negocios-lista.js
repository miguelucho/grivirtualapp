$(document).ready(function () {
    var bsq_cliente = '',
        bsq_inicio = '',
        bsq_fin = '';

    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
        i18n: {
            cancel: 'Cancelar',
            clear: 'limpiar',
            done: 'aplicar',
            months: [
                'Enero',
                'Febrero',
                'Marzo',
                'Abril',
                'Mayo',
                'Junio',
                'Julio',
                'Agosto',
                'Septiembre',
                'Octubre',
                'Noviembre',
                'Diciembre'
            ],
            monthsShort: [
                'Ene',
                'Feb',
                'Mar',
                'Abr',
                'May',
                'Jun',
                'Jul',
                'Ago',
                'Sep',
                'Oct',
                'Nov',
                'Dic'
            ],
            weekdays: [
                'Domingo',
                'Lunes',
                'Martes',
                'Miercoles',
                'Jueves',
                'Viernes',
                'Sabado'
            ],
            weekdaysShort: [
                'Dom',
                'Lun',
                'Mar',
                'Mir',
                'Jue',
                'Vie',
                'Sab'
            ],
            weekdaysAbbrev: ['D', 'L', 'M', 'M', 'J', 'V', 'S']
        }
    });

    listado(1);

    $('body').on('click', '.mpag', function () {
        listado($(this).prop('id'));
    });

    var options_de = {
        url: function (tipo) {
            return "dist/libs/ac_contactos";
        },
        getValue: function (element) {
            return element.nombre_cl;
        },
        list: {
            maxNumberOfElements: 10,
            onChooseEvent: function () {
                bsq_cliente = $('#Empresa-bus').getSelectedItemData().Id_cl;
            },
            sort: {
                enabled: true
            }
        },
        ajaxSettings: {
            dataType: "json",
            method: "POST",
            data: {
                dataType: "json"
            }
        },
        preparePostData: function (data) {
            data['contacto[opc]'] = 'Clientes';
            data['contacto[bus]'] = $('#Empresa-bus').val();
            return data;
        },
        requestDelay: 200
    };

    $('#Empresa-bus').easyAutocomplete(options_de);

    $('body').on('click', '#Empresa-bus', function () {
        $('.Empresa-bus-label').addClass('active');
    });

    $('body').on('blur', '#Empresa-bus', function () {
        setTimeout(function () {
            if (bsq_cliente == '') {
                $('#Empresa-bus').val('');
                $('.Empresa-bus-label').removeClass('active');
            }
        }, 400);

    });

    $('body').on('keyup', '#Empresa-bus', function () {
        if ($(this).val().trim() == '') {
            $('.Empresa-bus-label').removeClass('active');
            bsq_cliente = '';
        }
    });

    $('#busqueda').on('submit', function (e) {
        e.preventDefault();
        bsq_inicio = $('#Fecha-busi').val();
        bsq_fin = $('#Fecha-busf').val();
        listado(1);
    });

    $('body').on('click', '.Btn-ver', function () {
        idnegocio = $(this).prop('id');
        $('.Info-estado').html('');
        $('.Info-nombre').html('');
        $('.Info-cliente').html('');
        $('.Info-contacto').html('');
        $('.Info-origen').html('');
        $('.Info-cierre').html('');
        $('.Info-tipo').html('');
        $('.Info-servicio').html('');
        $('.Info-valor').html('');
        $('.Info-observacion').html('');

        data = [];
        data.push({
            name: 'negocio[opc]',
            value: 'Info'
        }, {
            name: 'negocio[idnegocio]',
            value: idnegocio
        });

        $.post('dist/libs/ac_negocios', data, function (data) {
            if (data.status == true) {
                $('.Info-estado').html(data.info.estado);
                $('.Info-nombre').html(data.info.nombre);
                $('.Info-cliente').html(data.info.cliente);
                $('.Info-contacto').html(data.info.contacto);
                $('.Info-origen').html(data.info.origen);
                $('.Info-cierre').html(data.info.cierre);
                $('.Info-tipo').html(data.info.tipo);
                $('.Info-servicio').html(data.info.servicio);
                $('.Info-valor').html(data.info.valor);
                $('.Info-observacion').html(data.info.observacion);
                $('#Ver-info').modal('open');
            } else if (data.status == false) {
                notification('error', data.msg);
            }
        }, 'json');
    });

    $('body').on('click', '.Btn-eliminar', function () {
        idnegocio = $(this).prop('id');
        var n = new Noty({
            text: 'Desea eliminar el negocio?',
            buttons: [
                Noty.button('Si', 'btn btn-success', function () {
                    $.post('dist/libs/ac_negocios', {
                        'negocio[opc]': 'Eliminar',
                        'negocio[id]': idnegocio,
                    }, function (data) {
                        n.close();
                        if (data.status == true) {
                            listado(1);
                        } else {
                            notification('error', data.msg);
                        }
                    }, 'json');
                }, { id: 'button1', 'data-status': 'ok' }),

                Noty.button('No', 'btn btn-error', function () {
                    n.close();
                })
            ],
            layout: 'center',
            theme: 'relax',
            modal: true,
        });
        n.show();
    });

    $('.Ver-cuadricula').on('click', function () {
        $('.Lista-tabla').hide();
        $('.Lista-cuadricula').show();
    });

    $('.Ver-tabla').on('click', function () {
        $('.Lista-cuadricula').hide();
        $('.Lista-tabla').show();
    });

    function listado(pg) {
        $('.listado-loader').fadeIn(0);
        $('#Listado-negocios').empty().fadeOut(0);
        $('.Tablero-info').empty().fadeOut(0);
        $.post('dist/libs/ac_negocios', {
            'negocio[opc]': 'Listado',
            'negocio[cliente]': bsq_cliente,
            'negocio[inicio]': bsq_inicio,
            'negocio[fin]': bsq_fin,
            'negocio[pagina]': pg
        }, function (data) {
            $('#Listado-negocios').html(data.list);
            $('.listado-paginacion ul').html(data.pagination);
            $('.Tablero-info').html(data.grid).fadeIn(0);
            $('.listado-loader').fadeOut(0);
            $('#Listado-negocios').fadeIn();
        }, 'json');
    }

    function notification(t, m) {
        var n = new Noty({
            type: t,
            text: m,
            layout: 'center',
            theme: 'relax',
            modal: true,
            closeWith: ['click'],
            progressBar: false,
            timeout: 1500,
        });
        n.show();
    }
});
