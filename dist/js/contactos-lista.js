$(document).ready(function () {
    var bsq_nombre = '',
        bsq_cliente = '';

    listado(1);

    $('body').on('click', '.mpag', function () {
        listado($(this).prop('id'));
    });

     var options_de = {
        url: function (tipo) {
            return "dist/libs/ac_contactos";
        },
        getValue: function (element) {
            return element.nombre_cl;
        },
        list: {
            maxNumberOfElements: 10,
            onChooseEvent: function () {
                bsq_cliente = $('#Empresa-bus').getSelectedItemData().Id_cl;
            },
            sort: {
                enabled: true
            }
        },
        ajaxSettings: {
            dataType: "json",
            method: "POST",
            data: {
                dataType: "json"
            }
        },
        preparePostData: function (data) {
            data['contacto[opc]'] = 'Clientes';
            data['contacto[bus]'] = $('#Empresa-bus').val();
            return data;
        },
        requestDelay: 200
    };

    $('#Empresa-bus').easyAutocomplete(options_de);

    $('body').on('click', '#Empresa-bus', function () {
        $('.Empresa-bus-label').addClass('active');
    });

    $('body').on('blur', '#Empresa-bus', function () {
        setTimeout(function () {
            if (bsq_cliente == '') {
                $('#Empresa-bus').val('');
                $('.Empresa-bus-label').removeClass('active');
            }
        }, 400);

    });

    $('body').on('keyup', '#Empresa-bus', function () {
        if ($(this).val().trim() == '') {
            $('.Empresa-bus-label').removeClass('active');
            bsq_cliente = '';
        }
    });

    $('#busqueda').on('submit', function (e) {
        e.preventDefault();
        bsq_nombre = $('#Nombre-bus').val();
        listado(1);
    });

    $('body').on('click', '.Btn-ver', function () {
        contacto = $(this).closest('tr').prop('id');
        $('.Info-usuario').html('');
        $('.Info-identificacion').html('');
        $('.Info-direccion').html('');
        $('.Info-sector').html('');
        $('.Info-correo').html('');
        $('.Info-nombre').html('');
        $('.Info-tipo').html('');
        $('.Info-telefono').html('');
        $('.Info-sitio').html('');
        $('.Info-fuente').html('');
        $('.Info-observacion').html('');

        data = [];
        data.push({
            name: 'contacto[opc]',
            value: 'Info'
        }, {
            name: 'contacto[idcontacto]',
            value: contacto
        });
        $.post('dist/libs/ac_contactos', data, function (data) {
            if (data.status == true) {
                $('.Info-usuario').html(data.info.usuario);
                $('.Info-nombre').html(data.info.nombre);
                $('.Info-cargo').html(data.info.cargo);
                $('.Info-celular').html(data.info.celular);
                $('.Info-correo').html(data.info.correo);
                $('.Info-skype').html(data.info.skype);
                $('.Info-cliente').html(data.info.cliente);
                $('.Info-departamento').html(data.info.departamento);
                $('.Info-telefono').html(data.info.telefono);
                $('.Info-correo2').html(data.info.correo2);
                $('.Info-direccion').html(data.info.direccion);
                $('.Info-observacion').html(data.info.observacion);
                $('#Ver-info').modal('open');
            } else if (data.status == false) {
                notification('error', data.msg);
            }
        }, 'json');
    });

    $('body').on('click', '.Btn-eliminar', function () {
        contacto = $(this).closest('tr').prop('id');
        var n = new Noty({
            text: 'Desea eliminar el contacto?',
            buttons: [
                Noty.button('Si', 'btn btn-success', function () {
                    $.post('dist/libs/ac_contactos', {
                        'contacto[opc]': 'Eliminar',
                        'contacto[id]': contacto,
                    }, function (data) {
                        n.close();
                        if (data.status == true) {
                            listado(1);
                        } else {
                            notification('error', data.msg);
                        }
                    }, 'json');
                }, { id: 'button1', 'data-status': 'ok' }),

                Noty.button('No', 'btn btn-error', function () {
                    n.close();
                })
            ],
            layout: 'center',
            theme: 'relax',
            modal: true,
        });
        n.show();
    });

    function listado(pg) {
        $('.listado-loader').fadeIn(0);
        $('#Listado-contactos').empty().fadeOut(0);
        $.post('dist/libs/ac_contactos', {
            'contacto[opc]': 'Listado',
            'contacto[nombre]': bsq_nombre,
            'contacto[cliente]': bsq_cliente,
            'contacto[pagina]': pg
        }, function (data) {
            $('#Listado-contactos').html(data.list);
            $('.listado-paginacion ul').html(data.pagination);
            $('.listado-loader').fadeOut(0);
            $('#Listado-contactos').fadeIn();
        }, 'json');
    }

    function notification(t, m) {
        var n = new Noty({
            type: t,
            text: m,
            layout: 'center',
            theme: 'relax',
            modal: true,
            closeWith: ['click'],
            progressBar: false,
            timeout: 1500,
        });
        n.show();
    }
});
