$(document).ready(function () {

    $('.modal').modal();

    editor = CKEDITOR.replace('descripcion-modulo', {
        toolbarGroups: [{ name: 'document', groups: ['mode', 'document', 'doctools'] },
        { name: 'clipboard', groups: ['clipboard', 'undo'] },
        { name: 'editing', groups: ['find', 'selection', 'spellchecker', 'editing'] },
        { name: 'forms', groups: ['forms'] },
        { name: 'basicstyles', groups: ['basicstyles', 'cleanup'] },
        { name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi', 'paragraph'] },
        { name: 'links', groups: ['links'] },
        { name: 'styles', groups: ['styles'] },
        { name: 'colors', groups: ['colors'] },
            '/',
        { name: 'insert', groups: ['insert'] },
            '/',
        { name: 'tools', groups: ['tools'] },
        { name: 'others', groups: ['others'] },
        { name: 'about', groups: ['about'] }
        ],
        removeButtons: 'Save,NewPage,Print,Preview,Templates,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Language,Anchor,Image,Flash,Table,HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe,Maximize,ShowBlocks,About,Source',
        resize_dir: 'both',
    });

    editor.on('change', function (evt) {
        editor.updateElement();
    });

    n = 0;

    $('.Mas-actividad').on('click', function () {
        $('.Cant-actividades').val(Number($('.Cant-actividades').val()) + 1);
        $('.Lista-actividades').append('<tr class="Temp-actividad">' +
            '<td>' +
            '<div class="input-field">' +
            '<select name="actividad[tipo][]" class="Select-tipo" required>' +
            '<option value="" disabled selected>Seleccionar</option>' +
            '<option value="1">Tarea</option>' +
            '<option value="2">Tarea Existente</option>' +
            '<option value="3">Nuevo foro</option>' +
            '<option value="4">Foro existente</option>' +
            '<option value="5">Cuestionario</option>' +
            '<option value="6">Cuestionario Existente</option>' +
            '</select>' +
            '<label>Tipo</label>' +
            '</div>' +
            '</td>' +
            '<td>' +
            '<div class="input-field Ig-abajo">' +
            '<input placeholder="%" id="valor' + n + '" name="actividad[valor][]" type="number" class="Actividad-porcentaje validate">' +
            '<label for="valor' + n + '">Valor</label>' +
            '</div>' +
            '</td>' +
            '<td>' +
            '<div class="input-field Ig-abajo">' +
            '<input placeholder="Nombre de la actividad" id="nombre' + n + '" name="actividad[nombre][]" type="text" class="validate">' +
            '<label for="nombre' + n + '">Nombre</label>' +
            '</div>' +
            '</td>' +
            '<td>' +
            '<div class="input-field">' +
            '<select name="actividad[corte][]" class="Select-corte Temp-corte">' +
            '<option value="" disabled selected>Seleccionar</option>' +
            '</select>' +
            '<label>Corte</label>' +
            '</div>' +
            '</td>' +
            '<td>' +
            '<a href="javascript:void(0)" class="Btn-table-rojo Btn-bloquear Eliminar"><span><i class="icon-blocked"></i>Eliminar</span></a>' +
            '</td>' +
            '<td>' +
            '</td>' +
            '</tr>');

        $('.Hide-cortes option').clone().appendTo('.Temp-corte');
        $('.Temp-corte').removeClass('Temp-corte');

        $('select').formSelect();
        window.M.updateTextFields();
        n++;
    });

    $('.Menos-actividad').on('click', function () {
        if ($('.Cant-actividades').data('existente') != $('.Cant-actividades').val()) {
            if ($('.Cant-actividades').val() > 1) {
                $('.Cant-actividades').val(Number($('.Cant-actividades').val()) - 1);
                $('.Temp-actividad:last').remove();
            }
        }
    });

    $('body').on('change', '.Select-corte', function () {
        corte = $('option:selected', this).val();
        select = $(this);

        if (corte != '' && $(this).closest('tr').find('.Actividad-porcentaje').val() != '') {
            total_porcentaje = 0;

            $.each($('.Select-corte'), function () {
                if ($('option:selected', this).val() == corte) {
                    total_porcentaje += Number($(this).closest('tr').find('.Actividad-porcentaje').val());
                }
            });

            loader();
            Totalcorte();
        }
    });

    $('body').on('keyup', '.Actividad-porcentaje', function () {
        corte = Number($(this).closest('tr').find('.Select-corte').val());
        select = $(this);

        if (corte != '') {
            total_porcentaje = 0;

            $.each($('.Select-corte'), function () {
                if ($('option:selected', this).val() == corte) {
                    total_porcentaje += Number($(this).closest('tr').find('.Actividad-porcentaje').val());
                }
            });

            loader();
            Totalcorte();
        }
    });

    function Totalcorte() {
        $.post('dist/libs/ac_actividades', {
            'actividad[opc]': 'Cortes',
            'actividad[grupo]': $('.Grupo-actividad').val(),
            'actividad[corte]': corte
        }, function (data) {
            $('#fondo').remove();
            if (data.status == true) {
                if (total_porcentaje + data.total > 100) {
                    select.val('');
                    $('select').formSelect();
                    notification('error', 'El valor del porcentaje supera el 100% para el corte seleccionado');
                }
            } else {
                if (total_porcentaje > 100) {
                    select.val('');
                    $('select').formSelect();
                    notification('error', 'El valor del porcentaje supera el 100% para el corte seleccionado');
                }
            }
        }, 'json');
    }

    $('body').on('click', '.Eliminar', function () {
        fila = $(this).closest('tr');
        var n = new Noty({
            text: 'Desea eliminar la actividad?',
            buttons: [
                Noty.button('Si', 'btn btn-success', function () {
                    fila.remove();
                    n.close();
                }, { id: 'button1', 'data-status': 'ok' }),

                Noty.button('No', 'btn btn-error', function () {
                    n.close();
                })
            ],
            layout: 'center',
            theme: 'relax',
            modal: true,
        });
        n.show();
    });

    $('body').on('change', '.Select-tipo', function () {
        if ($(this).val() == 2 || $(this).val() == 4 || $(this).val() == 6) {
            $('.Sel-existente option').not(':first').remove();
            $('#nombre-existente').val('');
            $.post('dist/libs/ac_actividades', {
                'actividad[opc]': 'Existentes',
                'actividad[grupo]': $('.Grupo-actividad').val(),
                'actividad[tipo]': $(this).val(),
            }, function (data) {
                $('#fondo').remove();
                if (data.existentes.length > 0) {
                    $.each(data.existentes, function (i, dat) {
                        $('.Sel-existente').append('<option value="' + dat.tipo + '" data-id="' + dat.Id + '" >' + dat.nombre + '</option>')
                    });
                    $('select').formSelect();
                    $('#copiar').modal('open');
                } else {

                }
            }, 'json');
        }
    });

    $('#Nueva-copia').on('submit', function (e) {
        e.preventDefault();

        $('.Cant-actividades').val(Number($('.Cant-actividades').val()) + 1);
        $('.Lista-actividades').append('<tr class="Temp-actividad">' +
            '<td>' +
            '<div class="input-field">' +
            '<select name="actividad[tipo][]" class="Select-tipo Copia-tipo" required>' +
            '<option value="" disabled selected>Seleccionar</option>' +
            '<option value="1">Tarea</option>' +
            '<option value="2">Tarea Existente</option>' +
            '<option value="3">Nuevo foro</option>' +
            '<option value="4">Foro existente</option>' +
            '<option value="5">Cuestionario</option>' +
            '<option value="6">Cuestionario Existente</option>' +
            '</select>' +
            '<label>Tipo</label>' +
            '<input type="hidden" name="actividad[idexistente][]" class="Copia-Id">' +
            '</div>' +
            '</td>' +
            '<td>' +
            '<div class="input-field Ig-abajo">' +
            '<input placeholder="%" id="valor' + n + '" name="actividad[valor][]" type="number" class="Actividad-porcentaje validate">' +
            '<label for="valor' + n + '">Valor</label>' +
            '</div>' +
            '</td>' +
            '<td>' +
            '<div class="input-field Ig-abajo">' +
            '<input placeholder="Nombre de la actividad" id="nombre' + n + '" name="actividad[nombre][]" type="text" class="validate Copia-nombre">' +
            '<label for="nombre' + n + '">Nombre</label>' +
            '</div>' +
            '</td>' +
            '<td>' +
            '<div class="input-field">' +
            '<select name="actividad[corte][]" class="Select-corte Temp-corte">' +
            '<option value="" disabled selected>Seleccionar</option>' +
            '</select>' +
            '<label>Corte</label>' +
            '</div>' +
            '</td>' +
            '<td>' +
            '<a href="javascript:void(0)" class="Btn-table-rojo Btn-bloquear Eliminar"><span><i class="icon-blocked"></i>Eliminar</span></a>' +
            '</td>' +
            '<td>' +
            '</td>' +
            '</tr>');

        $('.Hide-cortes option').clone().appendTo('.Temp-corte');
        $('.Temp-corte').removeClass('Temp-corte');

        $('.Copia-tipo').val($('.Sel-existente').val()).removeClass('Copia-tipo');
        $('.Copia-Id').val($('.Sel-existente option:selected').data('id')).removeClass('Copia-Id');
        $('.Copia-nombre').val($('#nombre-existente').val()).removeClass('Copia-nombre');

        $('select').formSelect();
        window.M.updateTextFields();
        n++;

        $('#copiar').modal('close');
    });

    $('#Nueva-actividad').on('submit', function (e) {
        e.preventDefault();
        loader();
        data = $(this).serializeArray();

        data.push({
            name: 'actividad[opc]',
            value: 'Nueva'
        });

        $.post('dist/libs/ac_actividades', data, function (data) {
            $('#fondo').remove();
            if (data.status == true) {
                notification('success', data.msg);
                setTimeout(function () {
                    location.reload();
                }, 2000);
            } else {
                notification('error', data.msg);
            }
        }, 'json');
    });

    function loader() {
        $('#fondo').remove();
        $('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
        $('#fondo').append('<div class="loader preloader-wrapper big active">' +
            '<div class="spinner-layer">' +
            '<div class="circle-clipper left">' +
            '<div class="circle"></div>' +
            '</div><div class="gap-patch">' +
            '<div class="circle"></div>' +
            '</div><div class="circle-clipper right">' +
            '<div class="circle"></div>' +
            '</div>' +
            '</div>' +
            '</div>');
        setTimeout(function () {
            $('#fondo').fadeIn('fast');
        }, 100);
    }

    function notification(t, m) {
        var n = new Noty({
            type: t,
            text: m,
            layout: 'center',
            theme: 'relax',
            modal: true,
            closeWith: ['click'],
            progressBar: false,
            timeout: 1500,
        });
        n.show();
    }

});
