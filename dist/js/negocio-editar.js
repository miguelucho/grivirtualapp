$(document).ready(function () {

    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
        i18n: {
            cancel: 'Cancelar',
            clear: 'limpiar',
            done: 'aplicar',
            months: [
                'Enero',
                'Febrero',
                'Marzo',
                'Abril',
                'Mayo',
                'Junio',
                'Julio',
                'Agosto',
                'Septiembre',
                'Octubre',
                'Noviembre',
                'Diciembre'
            ],
            monthsShort: [
                'Ene',
                'Feb',
                'Mar',
                'Abr',
                'May',
                'Jun',
                'Jul',
                'Ago',
                'Sep',
                'Oct',
                'Nov',
                'Dic'
            ],
            weekdays: [
                'Domingo',
                'Lunes',
                'Martes',
                'Miercoles',
                'Jueves',
                'Viernes',
                'Sabado'
            ],
            weekdaysShort: [
                'Dom',
                'Lun',
                'Mar',
                'Mir',
                'Jue',
                'Vie',
                'Sab'
            ],
            weekdaysAbbrev: ['D', 'L', 'M', 'M', 'J', 'V', 'S']
        }
    });

    setTimeout(function () {
        if ($('.select-usuarios').val() != '') {
            $('.select-usuarios').change();
        }
    }, 400);

    $('.select-usuarios').on('change', function () {
        usuario_selected = $(this).val();
        select_clientes = $('.select-clientes');
        $('option:not(:first)', select_clientes).remove();
        select_clientes.formSelect('destroy');
        $.each($('option', '#clientes-oculto'), function () {
            if ($(this).hasClass('Us-' + usuario_selected)) {
                $(this).clone().appendTo(select_clientes);
            }
        });

        $('.select-clientes').change();
        select_clientes.formSelect();
    });

    $('.select-clientes').on('change', function () {
        cliente_selected = $(this).val();
        select_contactos = $('.select-contactos');
        $('option:not(:first)', select_contactos).remove();
        select_contactos.formSelect('destroy');
        $.each($('option', '#contactos-oculto'), function () {
            if ($(this).hasClass('Cl-' + cliente_selected)) {
                $(this).clone().appendTo(select_contactos);
            }
        });

        select_contactos.formSelect();
    });

    $('#Negocio-editar').on('submit', function (e) {
        e.preventDefault();
        loader();
        select_clientes = $('.select-clientes');
        select_contactos = $('.select-contactos');
        data = $(this).serializeArray();
        data.push({
            name: 'negocio[opc]',
            value: 'Editar'
        });
        $.post('dist/libs/ac_negocios', data, function (data) {
            $('#fondo').remove();
            if (data.status == true) {
                notification('success', data.msg);
            } else {
                notification('error', data.msg);
            }
        }, 'json');
    });

    function loader() {
        $('#fondo').remove();
        $('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
        $('#fondo').append('<div class="loader preloader-wrapper big active">' +
            '<div class="spinner-layer">' +
            '<div class="circle-clipper left">' +
            '<div class="circle"></div>' +
            '</div><div class="gap-patch">' +
            '<div class="circle"></div>' +
            '</div><div class="circle-clipper right">' +
            '<div class="circle"></div>' +
            '</div>' +
            '</div>' +
            '</div>');
        setTimeout(function () {
            $('#fondo').fadeIn('fast');
        }, 100);
    }

    function notification(t, m) {
        var n = new Noty({
            type: t,
            text: m,
            layout: 'center',
            theme: 'relax',
            modal: true,
            closeWith: ['click'],
            progressBar: false,
            timeout: 1500,
        });
        n.show();
    }
});
