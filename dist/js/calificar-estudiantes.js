$(document).ready(function () {

    listado(1);

    function listado(pg) {
        $('.listado-loader').fadeIn(0);
        $('#Listado-estudiantes').empty().fadeOut(0);
        $.post('dist/libs/ac_calificaciones', {
            'califica[opc]': 'Listado-estudiantes',
            'califica[grupo]': $('.Calificar-grupo').val(),
            'califica[modulo]': $('.Calificar-modulo').val(),
            'califica[pagina]': pg,
        }, function (data) {
            $('#Listado-estudiantes').html(data.list);
            $('.listado-paginacion ul').html(data.pagination);
            $('.listado-loader').fadeOut(0);
            $('#Listado-estudiantes').fadeIn();
        }, 'json');
    }

    $('body').on('click', '.mpag', function () {
        listado($(this).prop('id'));
    });

});
