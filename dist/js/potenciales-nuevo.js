$(document).ready(function () {
    departamento = '';
    ciudad = '';

    var options_de = {
        url: function (tipo) {
            return "dist/libs/ac_potenciales";
        },
        getValue: function (element) {
            return element.nombre_dp;
        },
        list: {
            maxNumberOfElements: 10,
            onChooseEvent: function () {
                departamento = $('#Departamento').getSelectedItemData().Id_dp;
                $('#Ciudad').attr('disabled', false).val('');
                ciudad = '';
            },
            sort: {
                enabled: true
            }
        },
        ajaxSettings: {
            dataType: "json",
            method: "POST",
            data: {
                dataType: "json"
            }
        },
        preparePostData: function (data) {
            data['clientepot[opc]'] = 'Departamentos';
            data['clientepot[bus]'] = $('#Departamento').val();
            return data;
        },
        requestDelay: 200
    };

    $('#Departamento').easyAutocomplete(options_de);

    $('body').on('click', '#Departamento', function () {
        $('.Departamento-label').addClass('active');
    });

    $('body').on('blur', '#Departamento', function () {
        setTimeout(function () {
            if (departamento == '') {
                $('#Departamento').val('');
                $('.Departamento-label').removeClass('active');
                $('#Ciudad').attr('disabled', true).val('');
                ciudad = '';
            }
        }, 400);

    });

    $('body').on('keyup', '#Departamento', function () {
        if ($(this).val().trim() == '') {
            $('.Departamento-label').removeClass('active');
            departamento = '';
            $('#Ciudad').attr('disabled', true).val('');
            $('.Ciudad-label').removeClass('active');
            ciudad = '';
        }
    });

    var options_ci = {
        url: function (tipo) {
            return "dist/libs/ac_potenciales";
        },
        getValue: function (element) {
            return element.nombre_mn;
        },
        list: {
            maxNumberOfElements: 10,
            onChooseEvent: function () {
                ciudad = $('#Ciudad').getSelectedItemData().Id_mn;
            },
            sort: {
                enabled: true
            }
        },
        ajaxSettings: {
            dataType: "json",
            method: "POST",
            data: {
                dataType: "json"
            }
        },
        preparePostData: function (data) {
            data['clientepot[opc]'] = 'Ciudades';
            data['clientepot[bus]'] = $('#Ciudad').val();
            data['clientepot[departamento]'] = departamento;
            return data;
        },
        requestDelay: 200
    };

    $('#Ciudad').easyAutocomplete(options_ci);

    $('body').on('click', '#Ciudad', function () {
        $('.Ciudad-label').addClass('active');
    });

    $('body').on('blur', '#Ciudad', function () {
        setTimeout(function () {
            if (ciudad == '') {
                $('#Ciudad').val('');
                $('.Ciudad-label').removeClass('active');
            }
        }, 400);

    });

    $('body').on('keyup', '#Ciudad', function () {
        if ($(this).val().trim() == '') {
            $('.Ciudad-label').removeClass('active');
            ciudad = '';
        }
    });

    $('#Cliente-nuevo').on('submit', function (e) {
        e.preventDefault();
        loader();
        data = $(this).serializeArray();
        data.push({
            name: 'clientepot[opc]',
            value: 'Nuevo'
        }, {
            name: 'clientepot[Departamento]',
            value: departamento
        }, {
            name: 'clientepot[Ciudad]',
            value: ciudad
        });
        $.post('dist/libs/ac_potenciales', data, function (data) {
            $('#fondo').remove();
            if (data.status == true) {
                $('input').not('[type=submit]').val('');
                $("select").val('').attr('selected', true);
                $('textarea').val('');
                window.M.updateTextFields();
                $('select').formSelect();
                departamento = '';
                ciudad = '';
                notification('success', data.msg);
            } else {
                notification('error', data.msg);
            }
        }, 'json');
    });

    function loader() {
        $('#fondo').remove();
        $('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
        $('#fondo').append('<div class="loader preloader-wrapper big active">' +
            '<div class="spinner-layer">' +
            '<div class="circle-clipper left">' +
            '<div class="circle"></div>' +
            '</div><div class="gap-patch">' +
            '<div class="circle"></div>' +
            '</div><div class="circle-clipper right">' +
            '<div class="circle"></div>' +
            '</div>' +
            '</div>' +
            '</div>');
        setTimeout(function () {
            $('#fondo').fadeIn('fast');
        }, 100);
    }

    function notification(t, m) {
        var n = new Noty({
            type: t,
            text: m,
            layout: 'center',
            theme: 'relax',
            modal: true,
            closeWith: ['click'],
            progressBar: false,
            timeout: 1500,
        });
        n.show();
    }
});
