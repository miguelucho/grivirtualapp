$(document).ready(function () {
    data = new FormData();
    var $uploadCrop,
        tempFilename,
        rawImg;

    function readFile(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                rawImg = e.target.result;
            };
            reader.readAsDataURL(input.files[0]);
        } else {
            nmensaje('error', 'Error', 'Su navegador no soporta FileReader API');
        }
    }

    $('#imagen_perfil').on('change', function () {
        if ($(this).val() !== '') {
            if (/^image\/\w+/.test(this.files[0].type)) {
                tempFilename = $(this).val().replace(/C:\\fakepath\\/i, '');
                tempFilename = tempFilename.substr(0, tempFilename.lastIndexOf('.')) || tempFilename;
                readFile(this);
                modal({
                    type: 'info',
                    title: 'Recortar Imagen',
                    text: '<div class="modal-body">' +
                        '<div id="upload-demo" class="center-block"></div>' +
                        '</div>',
                    size: 'large',
                    callback: function (result) {
                        if (result === true) {
                            $uploadCrop.croppie('result', {
                                type: 'base64',
                                format: 'jpeg',
                                size: 'original',
                                size: {
                                    width: 167,
                                    height: 167
                                },
                            }).then(function (resp) {
                                data.append('usuario[imagen_perfil]', resp);
                                data.append('usuario[imagen_perfil_nombre]', tempFilename);
                            });
                        }
                    },
                    onShow: function (result) {
                        $uploadCrop = $('#upload-demo').croppie({
                            viewport: {
                                width: 167,
                                height: 167,
                            },
                            enforceBoundary: false,
                            enableExif: true,
                            enableResize: false
                        });
                        $uploadCrop.croppie('bind', {
                            url: rawImg
                        }).then(function () {
                            console.log('jQuery bind complete');
                        });
                    },
                    closeClick: false,
                    animate: true,
                    buttonText: {
                        yes: 'Confirmar',
                        cancel: 'Cancelar'
                    }
                });
            } else {
                nmensaje('error', 'Error', 'No es un formato válido');
                $(this).val('');
            }
        }
    });


    $('#Perfil').on('submit', function (e) {
        e.preventDefault();
        loader();

        otradata = $(this).serializeArray();

        $.each(otradata, function (key, input) {
            data.append(input.name, input.value);
        });

        data.append('usuario[opc]', 'Editar');

        $.ajax({
            url: 'dist/libs/ac_perfil',
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            dataType: 'json',
            success: function (data) {
                $('#fondo').remove();
                if (data.status == true) {
                    $('input[type=file],input[type=password]').val('');
                    notification('success', data.msg);
                    if (data.img != '') {
                        $('.Header-user-img img').attr('src', data.img);
                        $('.Perfil-imagen img').attr('src', data.img);
                    }
                    $('.Perfil-nombre').html('Hola '+ data.nombre);
                } else {
                    notification('error', data.msg);
                }
            }
        });
    });

    function loader() {
        $('#fondo').remove();
        $('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
        $('#fondo').append('<div class="loader preloader-wrapper big active">' +
            '<div class="spinner-layer">' +
            '<div class="circle-clipper left">' +
            '<div class="circle"></div>' +
            '</div><div class="gap-patch">' +
            '<div class="circle"></div>' +
            '</div><div class="circle-clipper right">' +
            '<div class="circle"></div>' +
            '</div>' +
            '</div>' +
            '</div>');
        setTimeout(function () {
            $('#fondo').fadeIn('fast');
        }, 100);
    }

    function notification(t, m) {
        var n = new Noty({
            type: t,
            text: m,
            layout: 'center',
            theme: 'relax',
            modal: true,
            closeWith: ['click'],
            progressBar: false,
            timeout: 1500,
        });
        n.show();
    }
});
