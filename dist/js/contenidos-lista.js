$(document).ready(function () {

    contenido_editado = 0;

    editor = CKEDITOR.replace('descripcion', {
        toolbarGroups: [{ name: 'document', groups: ['mode', 'document', 'doctools'] },
        { name: 'clipboard', groups: ['clipboard', 'undo'] },
        { name: 'editing', groups: ['find', 'selection', 'spellchecker', 'editing'] },
        { name: 'forms', groups: ['forms'] },
        { name: 'basicstyles', groups: ['basicstyles', 'cleanup'] },
        { name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi', 'paragraph'] },
        { name: 'links', groups: ['links'] },
        { name: 'styles', groups: ['styles'] },
        { name: 'colors', groups: ['colors'] },
            '/',
        { name: 'insert', groups: ['insert'] },
            '/',
        { name: 'tools', groups: ['tools'] },
        { name: 'others', groups: ['others'] },
        { name: 'about', groups: ['about'] }
        ],
        removeButtons: 'Save,NewPage,Print,Preview,Templates,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Language,Anchor,Image,Flash,Table,HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe,Maximize,ShowBlocks,About,Source',
        resize_dir: 'both',
    });

    editor.on('change', function (evt) {
        editor.updateElement();
    });

    listado(1);

    $('#Nuevo-contenido').on('submit', function (e) {
        e.preventDefault();
        loader();
        data = $(this).serializeArray();

        if (contenido_editado == 0) {
            data.push({
                name: 'contenido[opc]',
                value: 'Nuevo'
            });
        }else{
            data.push({
                name: 'contenido[opc]',
                value: 'Editar'
            },{
                name: 'contenido[idcon]',
                value: contenido_editado
            });
        }

        $.post('dist/libs/ac_contenidos', data, function (data) {
            $('#fondo').remove();
            if (data.status == true) {
                notification('success', data.msg);
                editor.setData('');
                contenido_editado = 0;
                listado(1);
            } else {
                notification('error', data.msg);
            }
        }, 'json');
    });

    $('body').on('click', '.Editar-contenido', function () {
        contenido_editado = $(this).closest('tr').prop('id');
        editor.setData('');
        $.post('dist/libs/ac_contenidos', {
            'contenido[opc]': 'Contenido-info',
            'contenido[idcon]': contenido_editado,
        }, function (data) {
            editor.setData(data.info.descripcion_con);
        }, 'json');
    });

    function listado(pg) {
        $('.listado-loader').fadeIn(0);
        $('#Listado-contenidos').empty().fadeOut(0);
        $.post('dist/libs/ac_contenidos', {
            'contenido[opc]': 'Contenido-listado',
            'contenido[grupo]': $('#contenido-grupo').val(),
            'contenido[pagina]': pg
        }, function (data) {
            $('#Listado-contenidos').html(data.list);
            $('.listado-paginacion ul').html(data.pagination);
            $('.listado-loader').fadeOut(0);
            $('#Listado-contenidos').fadeIn();
        }, 'json');
    }

    $('body').on('click', '.mpag', function () {
        listado($(this).prop('id'));
    });


    function loader() {
        $('#fondo').remove();
        $('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
        $('#fondo').append('<div class="loader preloader-wrapper big active">' +
            '<div class="spinner-layer">' +
            '<div class="circle-clipper left">' +
            '<div class="circle"></div>' +
            '</div><div class="gap-patch">' +
            '<div class="circle"></div>' +
            '</div><div class="circle-clipper right">' +
            '<div class="circle"></div>' +
            '</div>' +
            '</div>' +
            '</div>');
        setTimeout(function () {
            $('#fondo').fadeIn('fast');
        }, 100);
    }


    function notification(t, m) {
        var n = new Noty({
            type: t,
            text: m,
            layout: 'center',
            theme: 'relax',
            modal: true,
            closeWith: ['click'],
            progressBar: false,
            timeout: 1500,
        });
        n.show();
    }

});
