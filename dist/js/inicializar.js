$(document).ready(function () {
  $(".tooltipped").tooltip();
  $("select").formSelect();
  $(".modal").modal();
  $(".datepicker").datepicker();
  $('.timepicker').timepicker();

  $('.datepicker').datepicker({
    format: 'yyyy-mm-dd',
    i18n: {
      cancel: 'Cancelar',
      clear: 'limpiar',
      done: 'aplicar',
      months: [
        'Enero',
        'Febrero',
        'Marzo',
        'Abril',
        'Mayo',
        'Junio',
        'Julio',
        'Agosto',
        'Septiembre',
        'Octubre',
        'Noviembre',
        'Diciembre'
      ],
      monthsShort: [
        'Ene',
        'Feb',
        'Mar',
        'Abr',
        'May',
        'Jun',
        'Jul',
        'Ago',
        'Sep',
        'Oct',
        'Nov',
        'Dic'
      ],
      weekdays: [
        'Domingo',
        'Lunes',
        'Martes',
        'Miercoles',
        'Jueves',
        'Viernes',
        'Sabado'
      ],
      weekdaysShort: [
        'Dom',
        'Lun',
        'Mar',
        'Mir',
        'Jue',
        'Vie',
        'Sab'
      ],
      weekdaysAbbrev: ['D', 'L', 'M', 'M', 'J', 'V', 'S']
    }
  });
});
