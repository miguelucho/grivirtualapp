(function ($) {
  var $lastOpened = false;

  $("#Drop").on("click", function () {
    $(".Contenedor-principal-izq").toggleClass("Contenedor-principal-izq-min");
  });

  $("#Menu-user").on("click", function () {
    $(".Header-menu-user").toggleClass("Header-menu-user-oculto");
  });

  $(document).click(function (event) {
    if (!$(event.target).closest("img").hasClass("Menu-usuario")) {
      if (!$(".Header-menu-user").hasClass("Header-menu-user-oculto")) {
        $(".Header-menu-user").addClass("Header-menu-user-oculto");
      }
    }
  });

  $('.dropdown-trigger').dropdown({
    constrainWidth:false
  });
})(jQuery);


