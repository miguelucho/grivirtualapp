$(document).ready(function () {
  var bsq_cliente = "",
    bsq_inicio = "",
    bsq_fin = "";

  $(".datepicker").datepicker({
    format: "yyyy-mm-dd",
    i18n: {
      cancel: "Cancelar",
      clear: "limpiar",
      done: "aplicar",
      months: [
        "Enero",
        "Febrero",
        "Marzo",
        "Abril",
        "Mayo",
        "Junio",
        "Julio",
        "Agosto",
        "Septiembre",
        "Octubre",
        "Noviembre",
        "Diciembre",
      ],
      monthsShort: [
        "Ene",
        "Feb",
        "Mar",
        "Abr",
        "May",
        "Jun",
        "Jul",
        "Ago",
        "Sep",
        "Oct",
        "Nov",
        "Dic",
      ],
      weekdays: [
        "Domingo",
        "Lunes",
        "Martes",
        "Miercoles",
        "Jueves",
        "Viernes",
        "Sabado",
      ],
      weekdaysShort: ["Dom", "Lun", "Mar", "Mir", "Jue", "Vie", "Sab"],
      weekdaysAbbrev: ["D", "L", "M", "M", "J", "V", "S"],
    },
  });

  listado(1);

  $("body").on("click", ".mpag", function () {
    listado($(this).prop("id"));
  });

  var options_de = {
    url: function (tipo) {
      return "dist/libs/ac_contactos";
    },
    getValue: function (element) {
      return element.nombre_cl;
    },
    list: {
      maxNumberOfElements: 10,
      onChooseEvent: function () {
        bsq_cliente = $("#Empresa-bus").getSelectedItemData().Id_cl;
      },
      sort: {
        enabled: true,
      },
    },
    ajaxSettings: {
      dataType: "json",
      method: "POST",
      data: {
        dataType: "json",
      },
    },
    preparePostData: function (data) {
      data["contacto[opc]"] = "Clientes";
      data["contacto[bus]"] = $("#Empresa-bus").val();
      return data;
    },
    requestDelay: 200,
  };

  $("#Empresa-bus").easyAutocomplete(options_de);

  $("body").on("click", "#Empresa-bus", function () {
    $(".Empresa-bus-label").addClass("active");
  });

  $("body").on("blur", "#Empresa-bus", function () {
    setTimeout(function () {
      if (bsq_cliente == "") {
        $("#Empresa-bus").val("");
        $(".Empresa-bus-label").removeClass("active");
      }
    }, 400);
  });

  $("body").on("keyup", "#Empresa-bus", function () {
    if ($(this).val().trim() == "") {
      $(".Empresa-bus-label").removeClass("active");
      bsq_cliente = "";
    }
  });

  $("#busqueda").on("submit", function (e) {
    e.preventDefault();
    bsq_inicio = $("#Fecha-busi").val();
    bsq_fin = $("#Fecha-busf").val();
    listado(1);
  });

  function listado(pg) {
    $(".listado-loader").fadeIn(0);
    $("#Listado-negocios").empty().fadeOut(0);
    $(".Tablero-info").empty().fadeOut(0);
    $.post(
      "dist/libs/ac_reportes",
      {
        "reporte[opc]": "Listado",
        "reporte[cliente]": bsq_cliente,
        "reporte[inicio]": bsq_inicio,
        "reporte[fin]": bsq_fin,
        "reporte[pagina]": pg,
      },
      function (data) {
        $(".Potencial-creado").html(data.potenciales.creados);
        $(".Potencial-convertido").html(data.potenciales.convertidos + " %");
        $(".Negocio-creado").html(data.negocios.creados);

        // Potenciales por fuente

        if (data.potenciales.fuentes != "") {
          valor_fuentes = [];
          label_fuentes = [];

          $.each(data.potenciales.fuentes, function (i, dat) {
            valor_fuentes.push(dat.total);
            label_fuentes.push(dat.fuente);
          });

          var chart = new Chart($(".Grafica-potencial-fuente"), {
            type: "pie",
            data: {
              datasets: [
                {
                  data: valor_fuentes,
                  backgroundColor: [
                    "rgb(99, 147, 255)",
                    "rgb(255, 121, 99)",
                    "rgb(255, 205, 85)",
                    "rgb(99, 147, 255)",
                    "rgb(166, 85, 255)",
                  ],
                },
              ],
              labels: label_fuentes,
            },
            options: {
              responsive: true,
            },
          });
        }

        // Negocios por origen

        if (data.negocios.origenes != "") {
          valor_origenes = [];
          label_origenes = [];

          $.each(data.negocios.origenes, function (i, dat) {
            valor_origenes.push(dat.total);
            label_origenes.push(dat.origen);
          });

          var chart = new Chart($(".Grafica-negocio-origen"), {
            type: "pie",
            data: {
              datasets: [
                {
                  data: valor_origenes,
                  backgroundColor: [
                    "rgb(99, 147, 255)",
                    "rgb(255, 121, 99)",
                    "rgb(255, 205, 85)",
                    "rgb(99, 147, 255)",
                    "rgb(166, 85, 255)",
                  ],
                },
              ],
              labels: label_origenes,
            },
            options: {
              responsive: true,
            },
          });
        }

        // Potenciales por usuario

        if (data.potenciales.usuarios != "") {
          valor_usuarios = [];
          label_usuarios = [];

          $.each(data.potenciales.usuarios, function (i, dat) {
            valor_usuarios.push(dat.total);
            label_usuarios.push(dat.usuario);
          });

          var chart = new Chart($(".Grafica-potencial-usuario"), {
            type: "pie",
            data: {
              datasets: [
                {
                  data: valor_usuarios,
                  backgroundColor: [
                    "rgb(99, 147, 255)",
                    "rgb(255, 121, 99)",
                    "rgb(255, 205, 85)",
                    "rgb(99, 147, 255)",
                    "rgb(166, 85, 255)",
                  ],
                },
              ],
              labels: label_usuarios,
            },
            options: {
              responsive: true,
            },
          });
        }

        // Negocios por usuario

        if (data.negocios.usuarios != "") {
          valor_usuarios = [];
          label_usuarios = [];

          $.each(data.negocios.usuarios, function (i, dat) {
            valor_usuarios.push(dat.total);
            label_usuarios.push(dat.usuario);
          });

          var chart = new Chart($(".Grafica-negocio-usuario"), {
            type: "pie",
            data: {
              datasets: [
                {
                  data: valor_usuarios,
                  backgroundColor: [
                    "rgb(99, 147, 255)",
                    "rgb(255, 121, 99)",
                    "rgb(255, 205, 85)",
                    "rgb(99, 147, 255)",
                    "rgb(166, 85, 255)",
                  ],
                },
              ],

              labels: label_usuarios,
            },

            // borderColor: "rgb(255, 99, 132)",

            options: {
              responsive: true,
            },
          });
        }
      },
      "json"
    );
  }
});
