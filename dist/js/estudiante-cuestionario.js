$(document).ready(function () {

    /*  editor = CKEDITOR.replace('descripcion', {
         toolbarGroups: [{ name: 'document', groups: ['mode', 'document', 'doctools'] },
         { name: 'clipboard', groups: ['clipboard', 'undo'] },
         { name: 'editing', groups: ['find', 'selection', 'spellchecker', 'editing'] },
         { name: 'forms', groups: ['forms'] },
         { name: 'basicstyles', groups: ['basicstyles', 'cleanup'] },
         { name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi', 'paragraph'] },
         { name: 'links', groups: ['links'] },
         { name: 'styles', groups: ['styles'] },
         { name: 'colors', groups: ['colors'] },
             '/',
         { name: 'insert', groups: ['insert'] },
             '/',
         { name: 'tools', groups: ['tools'] },
         { name: 'others', groups: ['others'] },
         { name: 'about', groups: ['about'] }
         ],
         removeButtons: 'Save,NewPage,Print,Preview,Templates,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Language,Anchor,Image,Flash,Table,HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe,Maximize,ShowBlocks,About,Source',
         resize_dir: 'both',
     });

     editor.on('change', function (evt) {
         editor.updateElement();
     }); */

    $('#Cuestionario-responder').on('submit', function (e) {
        e.preventDefault();
        loader();

        data = $(this).serializeArray();

        data.push({
            name: 'estudiante[opc]',
            value: 'Cuestionario-respuesta'
        });

        $.each($('.Respuesta-input'), function () {
            if ($(this).is(':radio')) {
                if ($(this).is(':checked')) {
                    data.push({
                        name: 'estudiante[pregunta][][' + $(this).data('respuesta') + ']',
                        value: $(this).val()
                    });
                }
            } else {
                data.push({
                    name: 'estudiante[pregunta][][' + $(this).data('respuesta') + ']',
                    value: $(this).val()
                });
            }

        });

        $.post('dist/libs/ac_estudiantes', data, function (data) {
            $('#fondo').remove();
            if (data.status == true) {
                notification('success', data.msg);
                setTimeout(function(){
                    location.reload();
                },800);
            } else {
                notification('error', data.msg);
            }
        }, 'json');
    });

    function loader() {
        $('#fondo').remove();
        $('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
        $('#fondo').append('<div class="loader preloader-wrapper big active">' +
            '<div class="spinner-layer">' +
            '<div class="circle-clipper left">' +
            '<div class="circle"></div>' +
            '</div><div class="gap-patch">' +
            '<div class="circle"></div>' +
            '</div><div class="circle-clipper right">' +
            '<div class="circle"></div>' +
            '</div>' +
            '</div>' +
            '</div>');
        setTimeout(function () {
            $('#fondo').fadeIn('fast');
        }, 100);
    }


    function notification(t, m) {
        var n = new Noty({
            type: t,
            text: m,
            layout: 'center',
            theme: 'relax',
            modal: true,
            closeWith: ['click'],
            progressBar: false,
            timeout: 1500,
        });
        n.show();
    }

});
