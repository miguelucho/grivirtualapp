$(document).ready(function () {

    editor = CKEDITOR.replace('descripcion', {
        toolbarGroups: [{ name: 'document', groups: ['mode', 'document', 'doctools'] },
        { name: 'clipboard', groups: ['clipboard', 'undo'] },
        { name: 'editing', groups: ['find', 'selection', 'spellchecker', 'editing'] },
        { name: 'forms', groups: ['forms'] },
        { name: 'basicstyles', groups: ['basicstyles', 'cleanup'] },
        { name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi', 'paragraph'] },
        { name: 'links', groups: ['links'] },
        { name: 'styles', groups: ['styles'] },
        { name: 'colors', groups: ['colors'] },
            '/',
        { name: 'insert', groups: ['insert'] },
            '/',
        { name: 'tools', groups: ['tools'] },
        { name: 'others', groups: ['others'] },
        { name: 'about', groups: ['about'] }
        ],
        removeButtons: 'Save,NewPage,Print,Preview,Templates,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Language,Anchor,Image,Flash,Table,HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe,Maximize,ShowBlocks,About,Source',
        resize_dir: 'both',
    });

    editor.on('change', function (evt) {
        editor.updateElement();
    });

    n = 1;

    $('.Agregar-archivo').on('click', function () {
        $('.Temp-archivos').append('<div class="Contenedor-formularios-bloque">' +
            '<div class="Colum-tres">' +
            '<div class="input-field">' +
            '<input placeholder="Escriba el nombre del archivo" name="tarea[archivo_nombre][]" id="Nombre_Archivo-' + n + '" type="text" class="validate">' +
            '<label for="Nombre_Archivo-' + n + '">Nombre del archivo</label>' +
            '</div>' +
            '</div>' +
            '<div class="Colum-cuatro">' +
            '<div class="file-field input-field">' +
            '<div class="btn blue darken-3">' +
            '<span>Archivo</span>' +
            '<input type="file">' +
            '</div>' +
            '<div class="file-path-wrapper">' +
            '<input class="file-path validate" type="text">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="Colum-cuatro">' +
            '<br>' +
            '<a href="javascript:void(0)" class="Btn-table-rojo Btn-bloquear Eliminar"><span><i class="icon-blocked"></i>Eliminar</span></a>' +
            '</div>' +
            '</div>');

        n++;
    });

    $('body').on('click', '.Eliminar', function () {
        $(this).closest('.Contenedor-formularios-bloque').remove();
    });

    eliminar_documentos = [];

    $('body').on('click', '.Eliminar-documento', function () {
        documento = $(this).prop('id').split('-');
        fila = $(this).closest('.Contenedor-formularios-bloque');
        documento = documento[1];
        var n = new Noty({
            text: 'Desea eliminar el archivo?',
            buttons: [
                Noty.button('Si', 'btn btn-success', function () {
                    eliminar_documentos.push(documento);
                    fila.remove();
                    n.close();
                }, { id: 'button1', 'data-status': 'ok' }),

                Noty.button('No', 'btn btn-error', function () {
                    n.close();
                })
            ],
            layout: 'center',
            theme: 'relax',
            modal: true,
        });
        n.show();
    });

    $('#Administra-foro').on('submit', function (e) {
        e.preventDefault();
        loader();
        select_clientes = $('.select-clientes');
        var data = new FormData();

        $.each($('[type=file]'), function (i, obj) {
            $.each(obj.files, function (j, file) {
                data.append('archivo[' + i + ']', file);
            });
        });


        data.append('foro[opc]', 'Administrar');

        otradata = $(this).serializeArray();
        $.each(otradata, function (key, input) {
            data.append(input.name, input.value);
        });

        $.each(eliminar_documentos, function (i, dat) {
            data.append('foro[eliminar][]', dat);
        });

        $.ajax({
            url: 'dist/libs/ac_foros',
            type: 'POST',
            contentType: false,
            data: data,
            processData: false,
            cache: false,
            dataType: 'json',
            success: function (data) {
                $('#fondo').remove();
                if (data.status == true) {
                    notification('success', data.msg);
                    setTimeout(function(){
                        location.reload();
                    },1500);
                } else {
                    notification('error', data.msg);
                }
            }
        });
    });

    function loader() {
        $('#fondo').remove();
        $('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
        $('#fondo').append('<div class="loader preloader-wrapper big active">' +
            '<div class="spinner-layer">' +
            '<div class="circle-clipper left">' +
            '<div class="circle"></div>' +
            '</div><div class="gap-patch">' +
            '<div class="circle"></div>' +
            '</div><div class="circle-clipper right">' +
            '<div class="circle"></div>' +
            '</div>' +
            '</div>' +
            '</div>');
        setTimeout(function () {
            $('#fondo').fadeIn('fast');
        }, 100);
    }

    function notification(t, m) {
        var n = new Noty({
            type: t,
            text: m,
            layout: 'center',
            theme: 'relax',
            modal: true,
            closeWith: ['click'],
            progressBar: false,
            timeout: 1500,
        });
        n.show();
    }

});
