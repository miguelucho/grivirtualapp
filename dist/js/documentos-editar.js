$(document).ready(function () {

    setTimeout(function () {
        if ($('.select-usuarios').val() != '') {
            $('.select-usuarios').change();
        }
    }, 400);

    $('.select-usuarios').on('change', function () {
        usuario_selected = $(this).val();
        select_clientes = $('.select-clientes');
        $('option:not(:first)', select_clientes).remove();
        select_clientes.formSelect('destroy');
        $.each($('option', '#clientes-oculto'), function () {
            if ($(this).hasClass('Us-' + usuario_selected)) {
                $(this).clone().appendTo(select_clientes);
            }
        });

        select_clientes.formSelect();
    });

    $('#Documento-editar').on('submit', function (e) {
        e.preventDefault();
        select_clientes = $('.select-clientes');
        var data = new FormData();
        if ($('#documento_adjunto').val() != '') {
            var inputFileImage = document.getElementById('documento_adjunto');
            var file = inputFileImage.files[0];
            data.append('archivo', file);
        }

        data.append('documento[opc]', 'Editar');
        otradata = $(this).serializeArray();
        $.each(otradata, function (key, input) {
            data.append(input.name, input.value);
        });

        $.ajax({
            url: 'dist/libs/ac_documentos',
            type: 'POST',
            contentType: false,
            data: data,
            processData: false,
            cache: false,
            dataType: 'json',
            success: function (data) {
                if (data.status == true) {
                    $('#documento_adjunto').val('');
                    notification('success', data.msg);
                } else {
                    notification('error', data.msg);
                }
            }
        });
    });

    function notification(t, m) {
        var n = new Noty({
            type: t,
            text: m,
            layout: 'center',
            theme: 'relax',
            modal: true,
            closeWith: ['click'],
            progressBar: false,
            timeout: 1500,
        });
        n.show();
    }
});
