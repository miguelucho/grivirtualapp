<?php
session_start();
if (!isset($_SESSION['griapp_user'])) {
  header('Location: login');
}

require_once "dist/libs/conexion.php";

$nombre = '';
$login = '';

$usuarios = $db
  ->where('Id_us', $_SESSION['griapp_user'])
  ->objectBuilder()->get('usuarios_app');

if ($db->count > 0) {
  $nombre = $usuarios[0]->nombre_us;
  $login = $usuarios[0]->login_us;
}

?>
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Administrador Modulos</title>
  <link rel="stylesheet" type="text/css" href="dist/css/fonts.css">
  <?php include("dist/libs/cssvariable/css-variables.php") ?>
  <link rel="stylesheet" type="text/css" href="dist/css/materialize.css">
  <link rel="stylesheet" type="text/css" href="dist/css/load.css">
  <link rel="stylesheet" type="text/css" href="dist/css/noty.css">
  <link rel="stylesheet" type="text/css" href="dist/css/relax.css">
  <link rel="stylesheet" type="text/css" href="dist/css/croppie.css">
  <link rel="stylesheet" type="text/css" href="dist/css/paginacion.css">
  <link rel="stylesheet" type="text/css" href="dist/css/jquery.modal.css" />
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>

<body>
  <header>
    <div class="Admin-top">
      <?php include("dist/libs/includes-seccion/top-header.php") ?>
    </div>
  </header>
  <section>
    <div class="Contenedor-principal">
      <div class="Contenedor-principal-izq Contenedor-principal-izq-min">
        <?php include("dist/libs/includes-seccion/menu-izq-docentes.php"); ?>
      </div>
      <div class="Contenedor-principal-der">
        <div class="Contenedor-principal-der-int">
          <div class="Contenedor-principal-titulo">
            <div class="Contenedor-principal-titulo-sec">
              <h2 class="Titulo-seccion">Administrar contenidos del módulo</h2>
            </div>
            <div class="Contenedor-principal-titulo-sec">
              <div class="Btn-flotante-crear">
                <a href="administrar-modulos" data-target="crear" data-position="left" data-tooltip="Volver atras" class="modal-trigger tooltipped btn-floating btn-large waves-effect waves-light blue-grey"><i class="material-icons">keyboard_backspace</i></a>
              </div>
            </div>
          </div>

          <div class="Contenedor-desc">
           <!--  <div class="Contenedor-filtros">
              <form id="busqueda">
                <div class="Contenedor-filtros-int">
                  <div class="Colum-cuatro">
                    <div class="input-field">
                      <select id="Programa-bus" class="select-programas">
                        <option value="" selected>Seleccionar</option>
                        <?php echo $ls_programas ?>
                      </select>
                      <label>Nombre del programa</label>
                    </div>
                  </div>
                  <div class="Colum-cuatro">
                    <div class="input-field">
                      <select id="Curso-bus" class="select-cursos">
                        <option value="" selected>Seleccionar</option>
                      </select>
                      <label>Nombre del curso</label>
                    </div>
                  </div>
                  <div class="Colum-cuatro">
                    <div class="input-field">
                      <input id="Codigo-bus" type="text">
                      <label for="Codigo-bus">Código del programa</label>
                    </div>
                  </div>
                  <div class="Colum-cuatro">
                    <div class="input-field">
                      <input type="submit" class="Btn Btn-azul Bold-ro Btn-expand" value="Buscar">
                    </div>
                  </div>
                </div>
              </form>
            </div> -->
            <p>Ingresa el contenido: </p><br>
            <form id="Nuevo-contenido">
              <div class="Contenedor-editor-texto">
                <textarea id="descripcion" name="contenido[descripcion]"></textarea>
              </div>
              <div class="Contenedor-editor-texto">
                <div class="Colum-uno">
                  <input type="hidden" name="contenido[grupo]" value="<?php echo $_REQUEST['gr']; ?>" id="contenido-grupo">
                  <input type="submit" class="Btn Btn-dark Bold-ro Btn-expand" value="Guardar">
                </div>
              </div>
            </form>
            <div class="Contenedor-desc-int">
              <section>
                <table class="striped Table-virtual">
                  <thead>
                    <tr>
                      <th>Descripción</th>
                      <th>F. de actualización</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody id="Listado-contenidos">
                  </tbody>
                </table>
                <div class="listado-loader" style="text-align: center; margin-top: 50px;">
                    <div class="preloader-wrapper big active">
                        <div class="spinner-layer">
                            <div class="circle-clipper left">
                                <div class="circle"></div>
                            </div>
                            <div class="gap-patch">
                                <div class="circle"></div>
                            </div>
                            <div class="circle-clipper right">
                                <div class="circle"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="listado-paginacion">
                    <ul class="pagination">
                    </ul>
                </div>
              </section>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <script src="dist/js/jquery-1.11.1.min.js"></script>
  <script src="dist/js/noty.min.js"></script>
  <script src="dist/js/inicializar.js"></script>
  <script src="dist/js/materialize.min.js"></script>
  <script src="dist/js/noty.min.js"></script>
  <script src="dist/js/croppie.min.js"></script>
  <script src="dist/js/jquery.modal.min.js"></script>
  <script src="dist/js/ckeditor/ckeditor.js"></script>
  <script src="dist/js/menu-slide.js?v<?php echo date('YmdHis') ?>"></script>
  <script src="dist/js/contenidos-lista.js?v<?php echo date('YmdHis') ?>"></script>
</body>

</html>
