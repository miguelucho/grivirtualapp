<?php
session_start();
if (!isset($_SESSION['griapp_user'])) {
  header('Location: login');
}

require_once "dist/libs/conexion.php";

$ls_preguntas = '';

$cuestionario = $db
  ->where('Id_mad', $_REQUEST['cu'])
  ->objectBuilder()->get('modulo_actividades_cuestionario');

if ($db->count > 0) {
  $notificaciones = $db
    ->where('Id_us', $_SESSION['griapp_user'])
    ->where('tipoid_ea', $_REQUEST['cu'])
    ->objectBuilder()->get('usuarios_notificaciones');

  if ($db->count == 0) {
    $dats = [
      'Id_us' => $_SESSION['griapp_user'],
      'tipo_ea' => 'cuestionario',
      'tipoid_ea' => $_REQUEST['cu'],
      'visto_ea' => 1
    ];

    $db
      ->insert('usuarios_notificaciones', $dats);
  }

  $disabled = '';

  $resuelto = $db
    ->where('Id_mad', $_REQUEST['cu'])
    ->objectBuilder()->get('modulo_actividades_cuestionario_calificaciones');

  if ($db->count > 0) {
    $disabled = 'disabled="true"';
  }

  $preguntas = $db
    ->where('Id_mac', $cuestionario[0]->Id_mac)
    ->objectBuilder()->get('modulo_actividades_cuestionario_preguntas');

  if ($db->count > 0) {
    foreach ($preguntas as $pregunta) {
      $ls_preguntas .= '<div class="Contenedor-texto-pregunta">
                          <h6>Pregunta:</h6>
                          ' . $pregunta->pregunta_macp . '
                        </div>';

      $respuestas = $db
        ->where('Id_macp', $pregunta->Id_macp)
        ->objectBuilder()->get('modulo_actividades_cuestionario_respuestas');

      if ($db->count > 0) {
        $tipo_pr = '';

        $tipos_pregunta = $db
          ->where('Id_tp', $pregunta->tipo_macp)
          ->objectBuilder()->get('cuestionario_tipo_pregunta');

        if ($db->count > 0) {
          $tipo_pr = $tipos_pregunta[0]->nombre_tp;
        }

        $ls_respuestas = '';

        foreach ($respuestas as $respuesta) {
          $tipo_rs = '';

          /* $tipos_respuesta = $db
              ->where('Id_tr', $respuesta->tipo_macr)
              ->objectBuilder()->get('cuestionario_tipo_respuesta');

            if ($db->count > 0) {
              $tipo_rs = ($tipos_respuesta[0]->nombre_tr == 'Verdadero' ? 'V' : 'F');
            } */

          $tipo_rs = ($respuesta->tipo_macr == 1 ? 'V' : 'F');

          if ($pregunta->tipo_macp == 1) {
            $ls_respuestas .= '<p><label><input name="pregunta[]" data-respuesta="' . $respuesta->Id_macp . '" class="Respuesta-input" type="radio" value="' . $tipo_rs . '" ' . $disabled . ' ><span>' . $respuesta->respuesta_macr . '</span></label></p>';
          } elseif ($pregunta->tipo_macp == 2) {
            $ls_respuestas .= '<p><label><input name="pregunta[multiple]" data-respuesta="' . $respuesta->Id_macp . '" class="Respuesta-input" type="checkbox" value="' . $tipo_rs . '" ' . $disabled . '><span>' . $respuesta->respuesta_macr . '</span></label></p>';
          } else {
            $ls_respuestas .= '<textarea name="pregunta[]" data-respuesta="' . $respuesta->Id_macp . '" class="materialize-textarea Respuesta-input" required ' . $disabled . '></textarea>';
          }
        }

        $ls_preguntas .= '<div class="Contenedor-texto-tarea">
                              <h6>Respuesta</h6>
                              <p><strong>Tipo de pregunta: </strong> ' . $tipo_pr . '</p>
                              <div class="Contenedor-respuestas-lista">
                                ' . $ls_respuestas . '
                              </div>
                            </div>';
      }
    }
  }
}

?>
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Dashboard cuestionarios</title>
  <link rel="stylesheet" type="text/css" href="dist/css/fonts.css">
  <?php include("dist/libs/cssvariable/css-variables.php") ?>
  <link rel="stylesheet" type="text/css" href="dist/css/materialize.css">
  <link rel="stylesheet" type="text/css" href="dist/css/load.css">
  <link rel="stylesheet" type="text/css" href="dist/css/noty.css">
  <link rel="stylesheet" type="text/css" href="dist/css/relax.css">
  <link rel="stylesheet" type="text/css" href="dist/css/croppie.css">
  <link rel="stylesheet" type="text/css" href="dist/css/jquery.modal.css" />
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>

<body>
  <header>
    <div class="Admin-top">
      <?php include("dist/libs/includes-seccion/top-header.php") ?>
    </div>
  </header>
  <section>
    <div class="Contenedor-principal">
      <div class="Contenedor-principal-izq Contenedor-principal-izq-min">
        <?php include("dist/libs/includes-seccion/menu-izq-estudiantes.php"); ?>
      </div>
      <div class="Contenedor-principal-der">
        <div class="Contenedor-principal-der-int">
          <div class="Contenedor-principal-titulo">
            <div class="Contenedor-principal-titulo-sec">
              <h2 class="Titulo-seccion">Cuestionario</h2><br>
            </div>
          </div>
          <div class="Contenedor-desc">
            <div class="Contenedor-desc-int">
              <form id="Cuestionario-responder">
                <section>
                  <div class="Contenedor-admin-modulo">
                    <?php echo $ls_preguntas; ?>
                    <div class="Contenedor-admin-modulo">
                      <div class="Contenedor-editor-texto">
                        <br>
                        <p>Verifica la información ingresada, luego de hacer clic en guardar.</p>
                      </div>
                      <div class="Contenedor-formularios-bloque">
                        <div class="Colum-cuatro">
                          <input type="hidden" name="estudiante[cuestionario]" value="<?php echo $_REQUEST['cu']; ?>">
                          <?php if ($disabled == '') { ?>
                            <input type="submit" class="Btn Btn-dark Bold-ro Btn-expand" value="Guardar">
                          <?php } ?>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <script src="dist/js/jquery-1.11.1.min.js"></script>
  <script src="dist/js/noty.min.js"></script>
  <script src="dist/js/inicializar.js"></script>
  <script src="dist/js/materialize.min.js"></script>
  <script src="dist/js/noty.min.js"></script>
  <script src="dist/js/croppie.min.js"></script>
  <script src="dist/js/jquery.modal.min.js"></script>
  <script src="dist/js/ckeditor/ckeditor.js"></script>
  <script src="dist/js/menu-slide.js?v<?php echo date('YmdHis') ?>"></script>
  <script src="dist/js/estudiante-cuestionario.js?v<?php echo date('YmdHis') ?>"></script>
</body>

</html>
