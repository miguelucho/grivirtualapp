<?php
session_start();
if (!isset($_SESSION['griapp_user'])) {
  header('Location: login');
}

require_once "dist/libs/conexion.php";

// $_REQUEST['cu'] = 1;

$tipo_pregunta = '';
$tipo_respuesta = '';
$nombre_actividad = '';

$tipos = $db
  ->objectBuilder()->get('cuestionario_tipo_pregunta');

foreach ($tipos as $tipo) {
  $tipo_pregunta .= '<option value="' . $tipo->Id_tp . '">' . $tipo->nombre_tp . '</option>';
}

$tipos = $db
  ->objectBuilder()->get('cuestionario_tipo_respuesta');

foreach ($tipos as $tipo) {
  $tipo_respuesta .= '<option value="' . $tipo->Id_tr . '">' . $tipo->nombre_tr . '</option>';
}

$ls_preguntas = '';
$inicio_fecha = '';
$inicio_hora = '';
$fin_fecha = '';
$fin_hora = '';
$valor_total = 0;

if (!isset($_REQUEST['cu']) || !isset($_REQUEST['cu'])) {
  header('Location: administrar-modulos');
}else{
  $detalles = $db
    ->where('Id_mad', $_REQUEST['cu'])
    ->objectBuilder()->get('modulos_actividades_detalle');

  if($db->count > 0){
    $nombre_actividad = $detalles[0]->nombre_mad;

    $cuestionario = $db
      ->where('Id_mad', $_REQUEST['cu'])
      ->objectBuilder()->get('modulo_actividades_cuestionario');

    if($db->count > 0){
      $inicio_fecha = $cuestionario[0]->fecha_inicio_mac;
      $inicio_hora = $cuestionario[0]->hora_inicio_mac;
      $fin_fecha = $cuestionario[0]->fecha_fin_mac;
      $fin_hora = $cuestionario[0]->hora_fin_mac;

      $preguntas = $db
        ->where('Id_mac', $cuestionario[0]->Id_mac)
        ->objectBuilder()->get('modulo_actividades_cuestionario_preguntas');

      if($db->count > 0){
        $n_preguntas = 0;
        $n = 0;
        $m = 0;

        foreach ($preguntas as $pregunta) {
          $tipo_p = '';
          $ls_respuestas = '';

          $tipos = $db
            ->objectBuilder()->get('cuestionario_tipo_pregunta');

          foreach ($tipos as $tipo) {
            $tipo_p .= '<option value="' . $tipo->Id_tp . '" ' . ($tipo->Id_tp == $pregunta->tipo_macp ? "selected" : "") . '>' . $tipo->nombre_tp . '</option>';
          }

          $respuestas = $db
            ->where('Id_macp', $pregunta->Id_macp)
            ->objectBuilder()->get('modulo_actividades_cuestionario_respuestas');

          if($db->count > 0){
            foreach ($respuestas as $respuesta) {
              $tipo_r= '';

              $tipos = $db
                ->objectBuilder()->get('cuestionario_tipo_respuesta');

              foreach ($tipos as $tipo) {
                $tipo_r .= '<option value="' . $tipo->Id_tr . '" ' . ($tipo->Id_tr == $respuesta->tipo_macr ? "selected" : "") . '>' . $tipo->nombre_tr . '</option>';
              }

              $ls_respuestas .= '<div class="Contenedor-formularios-bloque Bloque-respuesta">
                                  <div class="Colum-uno">
                                    <div class="input-field">
                                      <input type="hidden" name="cuestionario[respuesta-' . $n_preguntas . '][]" value="' . $respuesta->Id_macr . '">
                                      <textarea id="textarea-r' . ($m + 1) . '" class="materialize-textarea" name="cuestionario[respuesta][' . $n_preguntas . '][]">' . $respuesta->respuesta_macr . '</textarea>
                                      <label for="textarea-r' . ($m + 1) . '">Descripción de la respuesta</label>
                                    </div>
                                  </div>
                                  <div class="Colum-cuatro ">
                                    <div class="input-field Sel-respuesta">
                                      <select name="cuestionario[respuesta_tipo][' . $n_preguntas . '][]" class="Select-tipo-respuesta">
                                        <option value="" disabled selected>Selecciona</option>
                                        ' . $tipo_r . '
                                      </select>
                                      <label>Tipo de respuesta</label>
                                    </div>
                                  </div>
                                  <div class="Colum-cuatro">
                                    <a href="javascript:void(0)" data-position="right" data-tooltip="Eliminar respuesta" class="tooltipped btn-floating btn-large waves-effect waves-light  red darken-4 Eliminar-respuesta" id="Er-' . $respuesta->Id_macr . '"><i class="material-icons">delete</i></a>
                                  </div>
                                </div>';
              $m++;
            }
          }

          $valor_total += $pregunta->valor_macp;

          $ls_preguntas .= '<div class="Contenedor-pregunta" data-pregunta="' . $n_preguntas . '">
                              <div class="Contenedor-pregunta-top">
                                  <a href="javascript:void(0)" data-position="left" data-tooltip="Eliminar pregunta" class="tooltipped Eliminar-pregunta" id="Pr-' . $pregunta->Id_macp . '"><i class="icon-bin"></i></a>
                              </div>
                              <div class="Contenedor-formularios-bloque">
                                <div class="Colum-uno">
                                  <div class="input-field">
                                    <input type="hidden" name="cuestionario[pregunta-' . $n_preguntas . ']" value="' . $pregunta->Id_macp . '">
                                    <textarea id="textarea' . $n . '" name="cuestionario[pregunta][' . $n_preguntas . ']" class="materialize-textarea">' . $pregunta->pregunta_macp . '</textarea>
                                    <label for="textarea' . $n . '">Descripción de la pregunta</label>
                                  </div>
                                </div>
                                <div class="Colum-cuatro ">
                                  <div class="input-field">
                                    <select name="cuestionario[pregunta_tipo][' . $n_preguntas . ']" class="Select-tipo-pregunta">
                                      <option value="" disabled selected>Selecciona</option>
                                      ' . $tipo_p . '
                                    </select>
                                    <label>Tipo de pregunta</label>
                                  </div>
                                </div>
                                <div class="Colum-cuatro">
                                  <div class="input-field">
                                    <input id="Valor_' . ($n + 1) . '" type="number" min="0" step=".1" name="cuestionario[pregunta_valor][' . $n_preguntas . ']" value="' . $pregunta->valor_macp . '" class="validate Valor-pregunta">
                                    <label for="Valor_' . ($n + 1) . '">Valor máximo</label>
                                  </div>
                                </div>
                              </div>';

          if($pregunta->tipo_macp != 3 && $pregunta->tipo_macp != 4){
            $ls_preguntas .= '<div class="Temp-respuesta">
                                ' . $ls_respuestas . '
                              </div>';
          }

          if($pregunta->tipo_macp == 4){
            $ls_preguntas .= '<div class="Temp-respuesta">
                               <div class="Contenedor-formularios-bloque Bloque-respuesta">
                                  <div class="Colum-uno">
                                    <div class="input-field">
                                      <input type="hidden" name="cuestionario[respuesta-' . $n_preguntas . '][]" value="' . $respuesta->Id_macr . '">
                                      <textarea id="textarea-r' . ($m + 1) . '" class="materialize-textarea" name="cuestionario[respuesta][' . $n_preguntas . '][]">' . $respuesta->respuesta_macr . '</textarea>
                                      <label for="textarea-r' . ($m + 1) . '">Descripción de la respuesta</label>
                                    </div>
                                  </div>
                                  <div class="Colum-cuatro ">
                                    <div class="input-field Sel-respuesta" style="display:none">
                                      <select name="cuestionario[respuesta_tipo][' . $n_preguntas . '][]" class="Select-tipo-respuesta">
                                        <option value="" disabled selected>Selecciona</option>
                                        ' . $tipo_r . '
                                      </select>
                                      <label>Tipo de respuesta</label>
                                    </div>
                                    <div class="input-field Input-clave">
                                      <input type="text" name="cuestionario[palabra][' . $n_preguntas . '][]" value="' . $respuesta->palabra_macr . '">
                                      <label>Palabra</label>
                                    </div>
                                  </div>
                                  <div class="Colum-cuatro">
                                    <a href="javascript:void(0)" data-position="right" data-tooltip="Eliminar respuesta" class="tooltipped btn-floating btn-large waves-effect waves-light  red darken-4 Eliminar-respuesta" id="Er-' . $respuesta->Id_macr . '"><i class="material-icons">delete</i></a>
                                  </div>
                                </div>
                              </div>';
          }

          $ls_preguntas .= '<div class="Contenedor-formularios-bloque">
                                <div class="Colum-cuatro">
                                  <a href="javascript:void(0)" data-position="right" data-tooltip="Nueva respuesta" class="Agregar-archivo tooltipped btn-floating btn-large waves-effect waves-light blue-grey darken-4 Mas-respuesta"><i class="material-icons">add</i></a>
                                </div>
                              </div>
                            </div>';

          $n_preguntas++;
          $n++;
        }
      }

    }else{
      $ls_preguntas = '<div class="Contenedor-pregunta" data-pregunta="0">
                      <div class="Contenedor-formularios-bloque">
                        <div class="Colum-uno">
                          <div class="input-field">
                            <textarea id="textarea1" name="cuestionario[pregunta][0]" class="materialize-textarea" required></textarea>
                            <label for="textarea1">Descripción de la pregunta</label>
                          </div>
                        </div>
                        <div class="Colum-cuatro">
                          <div class="input-field">
                            <select name="cuestionario[pregunta_tipo][0]" class="Select-tipo-pregunta" required>
                              <option value="" disabled selected>Selecciona</option>
                              '. $tipo_pregunta . '
                            </select>
                            <label>Tipo de pregunta</label>
                          </div>
                        </div>
                        <div class="Colum-cuatro">
                          <div class="input-field">
                            <input id="Valor_1" type="number" name="cuestionario[pregunta_valor][0]" class="validate Valor-pregunta" required>
                            <label for="Valor_1">Valor máximo</label>
                          </div>
                        </div>
                      </div>
                      <div class="Contenedor-formularios-bloque Bloque-respuesta">
                        <div class="Colum-uno">
                          <div class="input-field">
                            <textarea id="textarea2" class="materialize-textarea" name="cuestionario[respuesta][0][]" required></textarea>
                            <label for="textarea2">Descripción de la respuesta</label>
                          </div>
                        </div>
                        <div class="Colum-cuatro">
                          <div class="input-field Sel-respuesta">
                            <select name="cuestionario[respuesta_tipo][0][]" class="Select-tipo-respuesta">
                              <option value="" disabled selected>Selecciona</option>
                              ' . $tipo_respuesta . '
                            </select>
                            <label>Tipo de respuesta</label>
                          </div>
                        </div>
                        <div class="Colum-cuatro">
                        </div>
                      </div>
                      <div class="Temp-respuesta"></div>
                      <div class="Contenedor-formularios-bloque">
                        <div class="Colum-cuatro">
                          <a href="javascript:void(0)" data-position="right" data-tooltip="Nueva respuesta" class="Agregar-archivo tooltipped btn-floating btn-large waves-effect waves-light blue-grey darken-4 Mas-respuesta"><i class="material-icons">add</i></a>
                        </div>
                      </div>
                    </div>';
    }
  }else{
    header('Location: administrar-modulos');
  }
}

?>
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Administrador cuestionario</title>
  <link rel="stylesheet" type="text/css" href="dist/css/fonts.css">
  <?php include("dist/libs/cssvariable/css-variables.php") ?>
  <link rel="stylesheet" type="text/css" href="dist/css/materialize.css">
  <link rel="stylesheet" type="text/css" href="dist/css/load.css">
  <link rel="stylesheet" type="text/css" href="dist/css/noty.css">
  <link rel="stylesheet" type="text/css" href="dist/css/relax.css">
  <link rel="stylesheet" type="text/css" href="dist/css/croppie.css">
  <link rel="stylesheet" type="text/css" href="dist/css/jquery.modal.css" />
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>

<body>
  <header>
    <div class="Admin-top">
      <?php include("dist/libs/includes-seccion/top-header.php") ?>
    </div>
  </header>
  <section>
    <div class="Contenedor-principal">
      <div class="Contenedor-principal-izq Contenedor-principal-izq-min">
        <?php include("dist/libs/includes-seccion/menu-izq-docentes.php"); ?>
      </div>
      <div class="Contenedor-principal-der">
        <div class="Contenedor-principal-der-int">
          <div class="Contenedor-principal-titulo">
            <div class="Contenedor-principal-titulo-sec">
              <h2 class="Titulo-seccion">Administrar cuestionario</h2>
              <p>
                <a href="administrar-calificacion-cuestionario-siguiente?cu=<?php echo $_REQUEST['cu'] ?>&gr=<?php echo $_REQUEST['gr'] ?>&mo=<?php echo $_REQUEST['mo'] ?>" target="_blank" class="Btn-verde-oscuro"><i class="icon-list"> </i> Calificar actividad</a>
              </p>
            </div>
            <div class="Contenedor-principal-titulo-sec">
              <div class="Btn-flotante-crear">
                <a href="administrar-modulos-actividades?gr=<?php echo $_REQUEST['gr'] . '&mo=' . $_REQUEST['mo'] ?>" data-target="crear" data-position="left" data-tooltip="Volver atras" class="modal-trigger tooltipped btn-floating btn-large waves-effect waves-light blue-grey"><i class="material-icons">keyboard_backspace</i></a>
              </div>
            </div>
          </div>
          <div class="Contenedor-desc">
            <p>Administra el contenido de la actividad.<br><br></p>
            <div class="Contenedor-desc-int">
              <form id="Administra-cuestionario">
                 <div class="Contenedor-formularios-bloque">
                  <div class="Colum-uno">
                    <div class="input-field">
                      <input placeholder="Escriba el nombre del archivo"  name="cuestionario[nombre]" id="Nombre_cuestionario" value="<?php echo $nombre_actividad; ?>" type="text" >
                      <label for="Nombre_cuestionario">Nombre de la actividad - cuestionario</label>
                    </div>
                  </div>
                </div>
                <div class="Temp-pregunta">
                  <?php echo $ls_preguntas; ?>
                </div>
                <div class="Contenedor-formularios-bloque">
                  <div class="Colum-cuatro">
                    <a href="javascript:void(0)" class="Btn Btn-dark Bold-ro Btn-expand Mas-pregunta"> Añadir pregunta</a>
                  </div>
                </div>
                <section>
                  <br><br>
                  <div class="Contenedor-admin-modulo">
                    <div class="Contenedor-formularios-bloque">
                      <div class="Colum-cuatro">
                        <div class="input-field">
                          <input type="text" class="datepicker" name="cuestionario[apertura_fecha]" value="<?php echo $inicio_fecha; ?>" placeholder="Haz clic para ingresar">
                          <label for="fecha_apertura">Fecha de apertura</label>
                        </div>
                      </div>
                      <div class="Colum-cuatro">
                        <div class="input-field">
                          <input type="text" class="timepicker" name="cuestionario[apertura_hora]" value="<?php echo $inicio_hora; ?>" placeholder="Haz clic para ingresar">
                          <label for="hora_apertura">Hora de apertura</label>
                        </div>
                      </div>
                      <div class="Colum-cuatro">
                        <div class="input-field">
                          <input type="text" class="datepicker" name="cuestionario[cierre_fecha]" value="<?php echo $fin_fecha; ?>" placeholder="Haz clic para ingresar">
                          <label for="fecha_cierre">Fecha de cierre</label>
                        </div>
                      </div>
                      <div class="Colum-cuatro">
                        <div class="input-field">
                          <input type="text" class="timepicker" name="cuestionario[cierre_hora]" value="<?php echo $fin_hora; ?>" placeholder="Haz clic para ingresar">
                          <label for="hora_cierre">Hora de cierre</label>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
                <section>
                  <div class="Contenedor-admin-modulo">
                    <div class="Contenedor-admin-modulo">
                      <div class="Contenedor-editor-texto">
                        <br>
                        <p><span class="Valor-numgran">5</span> es el valor máximo de este cuestionario, la suma total de las preguntas es <span class="Valor-numgran Valor-actual"><?php echo $valor_total; ?></span>  </p>
                        <p>Verifica la información ingresada, luego de hacer clic en guardar.</p>
                      </div>
                      <div class="Contenedor-editor-texto">
                        <div class="Colum-uno">
                          <input type="hidden" name="cuestionario[idtcuestionario]" value="<?php echo $_REQUEST['cu']; ?>">
                          <input type="submit" class="Btn Btn-dark Bold-ro Btn-expand" value="Guardar">
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div style="display:none">
      <select class="Oculto-pregunta">
        <?php echo $tipo_pregunta; ?>
      </select>
      <select class="Oculto-respuesta">
        <?php echo $tipo_respuesta; ?>
      </select>
    </div>
  </section>
  <script src="dist/js/jquery-1.11.1.min.js"></script>
  <script src="dist/js/noty.min.js"></script>
  <script src="dist/js/inicializar.js"></script>
  <script src="dist/js/materialize.min.js"></script>
  <script src="dist/js/noty.min.js"></script>
  <script src="dist/js/croppie.min.js"></script>
  <script src="dist/js/jquery.modal.min.js"></script>
  <script src="dist/js/ckeditor/ckeditor.js"></script>
  <script src="dist/js/menu-slide.js?v<?php echo date('YmdHis') ?>"></script>
  <script src="dist/js/modulos-cuestionario-administrar.js?v<?php echo date('YmdHis') ?>"></script>
</body>

</html>
