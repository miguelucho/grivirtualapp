<?php
session_start();
if (!isset($_SESSION['griapp_user'])) {
  header('Location: login');
}

require_once "dist/libs/conexion.php";

$nombre = '';
$login = '';
$nm_programa = '';
$nm_curso = '';
$codigo = '';
$ls_modulos = '';

$usuarios = $db
  ->where('Id_us', $_SESSION['griapp_user'])
  ->objectBuilder()->get('usuarios_app');

if ($db->count > 0) {
  $nombre = $usuarios[0]->nombre_us;
  $login = $usuarios[0]->login_us;
}

if (!isset($_REQUEST['gr'])) {
  header('Location: estudiante-home');
}else{
  $grupos = $db
    ->where('Id_gr', $_REQUEST['gr'])
    ->objectBuilder()->get('grupos');

  if($db->count == 0){
    header('Location: estudiante-home');
  }else{

    $programas = $db
        ->where('Id_pr', $grupos[0]->Id_pr)
        ->objectBuilder()->get('programas');

    if ($db->count > 0) {
        $nm_programa = $programas[0]->nombre_pr;
    }

    $cursos = $db
        ->where('Id_cu', $grupos[0]->Id_cu)
        ->objectBuilder()->get('cursos');

    if ($db->count > 0) {
        $nm_curso = $cursos[0]->nombre_cu;
    }

    $codigo = $grupos[0]->codigo_gr;

    $modulos = $db
        ->where('Id_pr', $grupos[0]->Id_pr)
        ->where('Id_cu', $grupos[0]->Id_cu)
        ->objectBuilder()->get('modulos');

    if ($db->count > 0) {
        foreach ($modulos as $modulo) {
          $total_actividades = 0;

          $actividades = $db
            ->where('Id_gr', $_REQUEST['gr'])
            ->where('Id_mo', $modulo->Id_mo)
            ->objectBuilder()->get('modulos_actividades');

          if($db->count > 0){
            $periodo = $actividades[0]->periodo_ma;
            $idma = $actividades[0]->Id_ma;
            $dsc_modulo = $actividades[0]->descripcion_ma;

            $detalles = $db
              ->where('Id_ma', $actividades[0]->Id_ma)
              ->objectBuilder()->get('modulos_actividades_detalle');

            $total_actividades = $db->count;
          }

          $ls_modulos .= '<tr>
                            <td><span>' . $modulo->nombre_mo . '</span></td>
                            <td>' . $modulo->descripcion_mo . '</td>
                            <td class="center">' . $total_actividades . '</td>
                            <td><a href="estudiante-calificacion-actividades?gr=' . $_REQUEST['gr'] . '&mo=' . $modulo->Id_mo . '" data-target="editar" class="Btn-ver Btn-table-verde"><i class="icon-list"></i>Ver Calificaciones</a></td>
                          </tr>';
        }
    }
  }
}
?>
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Administrador Modulos</title>
  <link rel="stylesheet" type="text/css" href="dist/css/fonts.css">
  <?php include("dist/libs/cssvariable/css-variables.php") ?>
  <link rel="stylesheet" type="text/css" href="dist/css/materialize.css">
  <link rel="stylesheet" type="text/css" href="dist/css/load.css">
  <link rel="stylesheet" type="text/css" href="dist/css/noty.css">
  <link rel="stylesheet" type="text/css" href="dist/css/relax.css">
  <link rel="stylesheet" type="text/css" href="dist/css/jquery.modal.css" />
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>

<body>
  <header>
    <div class="Admin-top">
      <?php include("dist/libs/includes-seccion/top-header.php") ?>
    </div>
  </header>
  <section>
    <div class="Contenedor-principal">
      <div class="Contenedor-principal-izq Contenedor-principal-izq-min">
        <?php include("dist/libs/includes-seccion/menu-izq-estudiantes.php"); ?>
      </div>
      <div class="Contenedor-principal-der">
        <div class="Contenedor-principal-der-int">
          <div class="Contenedor-principal-titulo">
            <div class="Contenedor-principal-titulo-sec">
              <h2 class="Titulo-seccion">Ver calificaciones para: <?php echo $nm_programa; ?><br><?php echo $nm_curso; ?> - <?php echo $codigo; ?></h2>
            </div>
            <div class="Contenedor-principal-titulo-sec">
              <div class="Btn-flotante-crear">
                <a href="estudiante-calificaciones" data-target="crear" data-position="left" data-tooltip="Volver atras" class="modal-trigger tooltipped btn-floating btn-large waves-effect waves-light blue-grey"><i class="material-icons">keyboard_backspace</i></a>
              </div>
            </div>
          </div>
          <div class="Contenedor-desc">
            <p></p>
            <div class="Contenedor-desc-int">
              <section>
                <table class="striped Table-virtual">
                  <thead>
                    <tr>
                      <th>Nombre del módulo</th>
                      <th>Descripción</th>
                      <th class="center">Cant de actividades</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                   <?php echo $ls_modulos; ?>
                  </tbody>
                </table>
              </section>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <script src="dist/js/jquery-1.11.1.min.js"></script>
  <script src="dist/js/noty.min.js"></script>
  <script src="dist/js/inicializar.js"></script>
  <script src="dist/js/materialize.min.js"></script>
  <script src="dist/js/noty.min.js"></script>
  <script src="dist/js/jquery.modal.min.js"></script>
  <script src="dist/js/menu-slide.js?v<?php echo date('YmdHis') ?>"></script>
  <script src="dist/js/perfil.js?v<?php echo date('YmdHis') ?>"></script>
</body>

</html>
