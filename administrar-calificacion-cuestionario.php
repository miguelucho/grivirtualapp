<?php
session_start();
if (!isset($_SESSION['griapp_user'])) {
  header('Location: login');
}

require_once "dist/libs/conexion.php";


$nombre_actividad = '';
$ls_preguntas = '';

if (!isset($_REQUEST['cu']) || !isset($_REQUEST['cu'])) {
  header('Location: administrar-modulos');
} else {
  $nombre_estudiante = '';

  $matriculas = $db
    ->where('Id_ma', $_REQUEST['es'])
    ->objectBuilder()->get('matriculas');

  if ($db->count > 0) {
    $nombre_estudiante = $matriculas[0]->nombre_ma . ' ' . $matriculas[0]->apellido_ma;
  }

  $detalles = $db
    ->where('Id_mad', $_REQUEST['cu'])
    ->objectBuilder()->get('modulos_actividades_detalle');

  if ($db->count > 0) {
    $nombre_actividad = $detalles[0]->nombre_mad;

    $cuestionarios = $db
      ->where('Id_mad', $_REQUEST['cu'])
      ->objectBuilder()->get('modulo_actividades_cuestionario');

    if ($db->count > 0) {

      $preguntas = $db
        ->where('Id_mac', $cuestionarios[0]->Id_mac)
        ->objectBuilder()->get('modulo_actividades_cuestionario_preguntas');

      foreach ($preguntas as $pregunta) {

        $tipo_pregunta = '';

        $tpreguntas = $db
          ->where('Id_tp', $pregunta->tipo_macp)
          ->objectBuilder()->get('cuestionario_tipo_pregunta');

        if ($db->count > 0) {
          $tipo_pregunta = $tpreguntas[0]->nombre_tp;
        }

        $ls_respuestas = '';

        if ($pregunta->tipo_macp == 1 || $pregunta->tipo_macp == 2) {
          $respuestas = $db
            ->where('Id_macp', $pregunta->Id_macp)
            ->where('Id_ma', $_REQUEST['es'])
            ->objectBuilder()->get('modulo_actividades_cuestionario_calificaciones');

          if ($db->count > 0) {
            $nota = 0;

            $valor_respuesta = $pregunta->valor_macp / $db->count;

            foreach ($respuestas as $respuesta) {
              $fecha_respuesta = '';
              $n_respuesta = '';
              $fecha_respuesta = $respuesta->fecha_macc;

              switch ($respuesta->respuesta_macc) {
                case 'V':
                  $n_respuesta = 'Verdadero';
                  break;
                case 'F':
                  $n_respuesta = 'Falso';
                  break;
              }

              if ($respuesta->respuesta_macc == 'V') {
                $nota += $valor_respuesta;
                $ls_respuestas .= '<li class="Rp-bien">' . $n_respuesta . '<input type="hidden" name="cuestionario[nota][]" class="Nota-respuesta" value="' . $valor_respuesta . '"></li>';
              } else {
                $ls_respuestas .= '<li class="Rp-mal">' . $n_respuesta . '<input type="hidden" name="cuestionario[nota][]" class="Nota-respuesta" value="0"></li>';
              }

              // $ls_respuestas .= '<li class="Rp-bien">' . $n_respuesta . '</li>';
            }
          }

          $ls_preguntas .= '<div class="Conten-secc-pregunta">
                            <div class="Contenedor-texto-pregunta">
                              <h6>Pregunta:</h6>
                              ' . $pregunta->pregunta_macp . '
                            </div>
                            <div class="Contenedor-texto-tarea">
                              <h6>Respuesta</h6>
                              <p><strong>Tipo de pregunta: </strong> ' . $tipo_pregunta . ' </p>
                              <div class="Contenedor-respuestas-lista">
                                <ul>
                                  ' . $ls_respuestas . '
                                </ul>
                              </div>
                              <div class="Cont-calificacion"><p><strong>Calificación:</strong> ' . $nota . '</p></div>
                              <p><strong>Ultima actualización:</strong> ' . $fecha_respuesta . '</p>
                            </div>
                          </div>';
        } else if ($pregunta->tipo_macp == 3) {
          $n_respuesta = '';
          $fecha_respuesta = '';

          $respuestas = $db
            ->where('Id_macp', $pregunta->tipo_macp)
            ->where('Id_ma', $_REQUEST['es'])
            ->objectBuilder()->get('modulo_actividades_cuestionario_calificaciones');

          if ($db->count > 0) {
            $n_respuesta = $respuestas[0]->respuesta_macc;
            $fecha_respuesta = $respuestas[0]->fecha_macc;
          }

          $ls_preguntas .= '<div class="Conten-secc-pregunta">
                              <div class="Contenedor-texto-pregunta">
                                <h6>Pregunta:</h6>
                              ' . $pregunta->pregunta_macp . '
                              </div>
                              <div class="Contenedor-texto-tarea">
                                <h6>Respuesta</h6>
                                <p><strong>Tipo de pregunta: </strong> Texto</p>
                                <div class="Contenedor-respuestas-lista">
                                  <ul>
                                    ' . $n_respuesta . '
                                  </ul>
                                </div>
                                <div class="Contenedor-formularios-bloque">
                                  <div class="Colum-cuatro">
                                    <div class="input-field">
                                      <input type="number" name="cuestionario[nota][]" value="" placeholder="Ingresa la nota" class="validate Nota-respuesta" required="">
                                      <label for="hora_cierre">Nota para la actividad </label>
                                    </div>
                                  </div>
                                </div>
                                <p><strong>Ultima actualización:</strong> ' . $fecha_respuesta . ' </p>
                              </div>
                            </div>';
        } else if ($pregunta->tipo_macp == 4) {
          $n_respuesta = '';
          $fecha_respuesta = '';

          $respuestas = $db
            ->where('Id_macp', $pregunta->tipo_macp)
            ->where('Id_ma', $_REQUEST['es'])
            ->objectBuilder()->get('modulo_actividades_cuestionario_calificaciones');

          if ($db->count > 0) {
            $n_respuesta = $respuestas[0]->respuesta_macc;
            $fecha_respuesta = $respuestas[0]->fecha_macc;
          }

          $nota = 0;

          $cuestionario_respuestas = $db
            ->where('Id_macp', $pregunta->Id_macp)
            ->objectBuilder()->get('modulo_actividades_cuestionario_respuestas');

          if ($db->count > 0) {
            if (limpiar($n_respuesta) == limpiar($cuestionario_respuestas[0]->palabra_macr)) {
              $nota = $pregunta->valor_macp;
            }
          }

          $ls_preguntas .= '<div class="Conten-secc-pregunta">
                              <div class="Contenedor-texto-pregunta">
                                <h6>Pregunta:</h6>
                              ' . $pregunta->pregunta_macp . '
                              </div>
                              <div class="Contenedor-texto-tarea">
                                <h6>Respuesta</h6>
                                <p><strong>Tipo de pregunta: </strong> Palabra clave (' . $cuestionario_respuestas[0]->palabra_macr . ')</p>
                                <div class="Contenedor-respuestas-lista">
                                  <ul>
                                    ' . $n_respuesta . '
                                  </ul>
                                </div>
                                <div class="Contenedor-formularios-bloque">
                                  <div class="Colum-cuatro">
                                    <div class="input-field">
                                      <input type="number" name="cuestionario[nota][]" value="' . $nota . '" placeholder="Ingresa la nota" class="validate Nota-respuesta" required="">
                                      <label for="hora_cierre">Nota para la actividad </label>
                                    </div>
                                  </div>
                                </div>
                                <p><strong>Ultima actualización:</strong> ' . $fecha_respuesta . ' </p>
                              </div>
                            </div>';
        }
      }
    }
  }
}

function limpiar($String)
{
  $String = preg_replace("/[^A-Za-z0-9\_\-]/", '', $String);
  $String = mb_strtolower($String);
  return $String;
}
?>
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Calificar cuestionarios</title>
  <link rel="stylesheet" type="text/css" href="dist/css/fonts.css">
  <?php include("dist/libs/cssvariable/css-variables.php") ?>
  <link rel="stylesheet" type="text/css" href="dist/css/materialize.css">
  <link rel="stylesheet" type="text/css" href="dist/css/load.css">
  <link rel="stylesheet" type="text/css" href="dist/css/noty.css">
  <link rel="stylesheet" type="text/css" href="dist/css/relax.css">
  <link rel="stylesheet" type="text/css" href="dist/css/jquery.modal.css" />
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>

<body>
  <header>
    <div class="Admin-top">
      <?php include("dist/libs/includes-seccion/top-header.php") ?>
    </div>
  </header>
  <section>
    <div class="Contenedor-principal">
      <div class="Contenedor-principal-izq Contenedor-principal-izq-min">
        <?php include("dist/libs/includes-seccion/menu-izq-docentes.php"); ?>
      </div>
      <div class="Contenedor-principal-der">
        <div class="Contenedor-principal-der-int">
          <div class="Contenedor-principal-titulo">
            <div class="Contenedor-principal-titulo-sec">
              <h2 class="Titulo-seccion">Calificar cuestionario</h2><br>
              <h6 class="Cont-nombre-estudiante"><?php echo $nombre_estudiante ?></h6>
            </div>
            <div class="Contenedor-principal-titulo-sec">
            </div>
          </div>
          <div class="Contenedor-desc">
            <div class="Contenedor-desc-int">
              <form id="Calificar-cuestionario">
                <div class="Contenedor-formularios-bloque">
                  <div class="Colum-uno">
                    <div class="input-field">
                      <div class="Titulo-tarea"><br>
                        <p><strong><?php echo $nombre_actividad ?></strong> </p>
                      </div>
                      <label for="Nombre_tarea">Nombre de la actividad - cuestionario</label>
                    </div>
                  </div>
                </div>
                <section>
                  <div class="Contenedor-admin-modulo">
                    <?php echo $ls_preguntas ?>

                    <div class="Contenedor-formularios-bloque">
                      <div class="Colum-cuatro">
                        <div class="Cont-calificacion">
                          <p>
                          <h6><strong>Nota final:</strong> <span class="Nota-final"></span></h6>
                          </p>
                        </div><br>
                      </div>
                    </div>

                    <!--  <div class="Contenedor-formularios-bloque">
                      <div class="Colum-uno">
                        <div class="input-field">
                          <textarea id="textarea1" name="cuestionario[observacion]" class="materialize-textarea"></textarea>
                          <label for="textarea1">Observaciones (opcional)</label>
                        </div>
                      </div>
                    </div> -->

                    <div class="Contenedor-admin-modulo">
                      <div class="Contenedor-editor-texto">
                        <br>
                        <p>Verifica la información ingresada, luego de hacer clic en guardar.</p>
                      </div>
                      <div class="Contenedor-formularios-bloque">
                        <div class="Colum-cuatro">
                          <input type="hidden" name="cuestionario[idcuestionario]" value="<?php echo $_REQUEST['cu']; ?>">
                          <input type="submit" class="Btn Btn-dark Bold-ro Btn-expand" value="Guardar nota">
                        </div>
                        <div class="Colum-cuatro">
                        </div>
                        <!--  <div class="Colum-cuatro Alinear-dere">
                          <a class="Btn grey lighten-2 Bold-ro Btn-expand Ant">Anterior</a>
                        </div>
                        <div class="Colum-cuatro Alinear-dere">
                          <a class="Btn blue darken-3 Bold-ro Btn-expand Sig">Siguiente</a>
                        </div> -->
                      </div>
                    </div>
                  </div>
                </section>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <script src="dist/js/jquery-1.11.1.min.js"></script>
  <script src="dist/js/noty.min.js"></script>
  <script src="dist/js/inicializar.js"></script>
  <script src="dist/js/materialize.min.js"></script>
  <script src="dist/js/noty.min.js"></script>
  <script src="dist/js/jquery.modal.min.js"></script>
  <script src="dist/js/menu-slide.js?v<?php echo date('YmdHis') ?>"></script>
  <script src="dist/js/modulos-cuestionarios-calificar.js?v<?php echo date('YmdHis') ?>"></script>
</body>

</html>
