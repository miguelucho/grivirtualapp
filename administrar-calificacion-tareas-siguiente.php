<?php
session_start();
if (!isset($_SESSION['griapp_user'])) {
  header('Location: login');
}

require_once "dist/libs/conexion.php";


$descripcion_foro = '';

if (!isset($_REQUEST['ta']) || !isset($_REQUEST['ta'])) {
  header('Location: administrar-modulos');
} else {
  $detalles = $db
    ->where('Id_mad', $_REQUEST['ta'])
    ->objectBuilder()->get('modulos_actividades_detalle');

  if ($db->count > 0) {
    $nombre_actividad = $detalles[0]->nombre_mad;

    $tareas = $db
      ->where('Id_mad', $_REQUEST['ta'])
      ->objectBuilder()->get('modulo_actividades_tareas');

    if ($db->count > 0) {
      $descripcion = $tareas[0]->descripcion_mat;

      $ver_archivo = '';

      $documentos = $db
        ->where('Id_mat', $tareas[0]->Id_mat)
        ->objectBuilder()->get('modulo_actividades_tareas_archivos');

      if ($db->count > 0) {
        foreach ($documentos as $documento) {
          $ver_archivo .= '<p><a href="dist/' . $documento->archivo_mata . '" target="_blank">Ver Archivo Adjunto</a></p>';
        }
      }
    }

    $nombre_estudiante = '';
    $descripcion_respuesta = '';
    $fecha_respuesta = '';
    $archivo_respuesta = '';
    $idcalificacion = '';
    $estudiante = '';
    $anterior = '';
    $siguiente = '';

    if (isset($_REQUEST['es'])) {
      $estudiante = $_REQUEST['es'];
    }

    $respondieron = array();

    $respuestas = $db
      ->where('Id_mad', $_REQUEST['ta'])
      ->orderBy('Id_ma', 'ASC')
      ->objectBuilder()->get('modulo_actividades_tareas_calificaciones');


    if ($db->count > 0) {
      $existen = 1;

      foreach ($respuestas as $respuesta) {
        $respondieron[] = $respuesta->Id_ma;
      }

      if (count($respondieron) > 0) {
        if (count($respondieron) > 1) {
          if ($estudiante == '') {
            $siguiente = $respondieron[1];
          } else {
            $anterior = $respondieron[array_search($estudiante, $respondieron) - 1];
            $siguiente = $respondieron[array_search($estudiante, $respondieron) + 1];
          }
        }
      }

      /* print_r('<pre>');
      print_r($respondieron);
      print_r('</pre>'); */

      if ($estudiante == '') {
        $estudiante = $respondieron[0];
      }

      if ($estudiante == $anterior) {
        $anterior = '';
      }

      if ($estudiante == $siguiente) {
        $siguiente = '';
      }

      $matriculas = $db
        ->where('Id_ma', $estudiante, 'LIKE')
        ->objectBuilder()->get('matriculas');

      if ($db->count > 0) {
        $nombre_estudiante = $matriculas[0]->nombre_ma . ' ' . $matriculas[0]->apellido_ma;
      }

      $respuestas = $db
        ->where('Id_mad', $_REQUEST['ta'])
        ->where('Id_ma', $estudiante, 'LIKE')
        ->objectBuilder()->get('modulo_actividades_tareas_calificaciones');

      if ($db->count > 0) {
        foreach ($respuestas as $respuesta) {
          $descripcion_respuesta = $respuesta->descripcion_matc;
          $fecha_respuesta = $respuesta->fecha_matc;
          $archivo_respuesta = ' <li><a href="dist/' . $respuesta->archivo_matc . '" target="_blank"><i class="icon-attachment"></i> Archivo adjunto</a></li>';

          $idcalificacion = $respuesta->Id_matc;
        }
      }
    } else {
      $existen = 0;
    }
  } else {
    header('Location: administrar-modulos');
  }
}
?>
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Calificar tareas</title>
  <link rel="stylesheet" type="text/css" href="dist/css/fonts.css">
  <?php include("dist/libs/cssvariable/css-variables.php") ?>
  <link rel="stylesheet" type="text/css" href="dist/css/materialize.css">
  <link rel="stylesheet" type="text/css" href="dist/css/load.css">
  <link rel="stylesheet" type="text/css" href="dist/css/noty.css">
  <link rel="stylesheet" type="text/css" href="dist/css/relax.css">
  <link rel="stylesheet" type="text/css" href="dist/css/croppie.css">
  <link rel="stylesheet" type="text/css" href="dist/css/jquery.modal.css" />
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>

<body>
  <header>
    <div class="Admin-top">
      <?php include("dist/libs/includes-seccion/top-header.php") ?>
    </div>
  </header>
  <section>
    <div class="Contenedor-principal">
      <div class="Contenedor-principal-izq Contenedor-principal-izq-min">
        <?php include("dist/libs/includes-seccion/menu-izq-docentes.php"); ?>
      </div>
      <?php if ($existen == 1) { ?>
        <div class="Contenedor-principal-der">
          <div class="Contenedor-principal-der-int">
            <div class="Contenedor-principal-titulo">
              <div class="Contenedor-principal-titulo-sec">
                <h2 class="Titulo-seccion">Calificar tarea</h2><br>
                <h6 class="Cont-nombre-estudiante"><?php echo $nombre_estudiante ?></h6>
              </div>
              <div class="Contenedor-principal-titulo-sec">
              </div>
            </div>
            <div class="Contenedor-desc">
              <div class="Contenedor-desc-int">
                <form id="Calificar-tarea">
                  <div class="Contenedor-formularios-bloque">
                    <div class="Colum-uno">
                      <div class="input-field">
                        <div class="Titulo-tarea"><br>
                          <p><strong><?php echo $nombre_actividad ?></strong> </p>
                        </div>
                        <label for="Nombre_tarea">Nombre de la actividad - tarea</label>
                      </div>
                    </div>
                  </div>
                  <section>
                    <div class="Contenedor-admin-modulo">
                      <div class="Contenedor-texto-pregunta">
                        <h6>Descripción:</h6>
                        <?php echo $descripcion ?>
                        <br>
                        <?php echo $ver_archivo ?>
                      </div>
                      <div class="Contenedor-texto-tarea">
                        <h6>Respuesta</h6>
                        <?php echo $descripcion_respuesta ?>
                        <p><strong>Ultima actualización:</strong> <?php echo $fecha_respuesta ?></p>
                      </div>
                      <p>A continuación archivos adjuntos por el estudiente.</p>
                      <div class="Contenedor-formularios-bloque">
                        <div class="Colum-uno">
                          <ul class="Contenedor-archivos-lista">
                            <?php echo $archivo_respuesta ?>
                          </ul>
                        </div>
                      </div>
                      <div class="Contenedor-formularios-bloque">
                        <div class="Colum-cuatro">
                          <div class="input-field">
                            <input type="number" name="tarea[nota]" value="" placeholder="Ingresa la nota" class="validate" required="">
                            <label for="hora_cierre">Nota para la actividad </label>
                          </div>
                        </div>
                      </div>
                      <div class="Contenedor-formularios-bloque">
                        <div class="Colum-uno">
                          <div class="input-field">
                            <textarea id="textarea1" name="tarea[observacion]" class="materialize-textarea"></textarea>
                            <label for="textarea1">Observaciones (opcional)</label>
                          </div>
                        </div>
                      </div>

                      <div class="Contenedor-admin-modulo">
                        <div class="Contenedor-editor-texto">
                          <br>
                          <p>Verifica la información ingresada, luego de hacer clic en guardar.</p>
                        </div>
                        <div class="Contenedor-formularios-bloque">
                          <div class="Colum-cuatro">
                            <input type="hidden" name="tarea[idtarea]" value="<?php echo $idcalificacion; ?>">
                            <input type="submit" class="Btn Btn-dark Bold-ro Btn-expand" value="Guardar nota">
                          </div>
                          <div class="Colum-cuatro">
                          </div>
                          <?php
                          if ($anterior != '') {
                          ?>
                            <div class="Colum-cuatro Alinear-dere">
                              <a href="administrar-calificacion-tareas-siguiente?ta=<?php echo $_REQUEST['ta'] . '&gr=' . $_REQUEST['gr'] . '&mo=' . $_REQUEST['mo'] . '&es=' . $anterior ?>" class="Btn grey lighten-2 Bold-ro Btn-expand Ant">Anterior</a>
                            </div>
                          <?php
                          }
                          ?>
                          <?php
                          if ($siguiente != '') {
                          ?>
                            <div class="Colum-cuatro Alinear-dere">
                              <a href="administrar-calificacion-tareas-siguiente?ta=<?php echo $_REQUEST['ta'] . '&gr=' . $_REQUEST['gr'] . '&mo=' . $_REQUEST['mo'] . '&es=' . $siguiente ?>" class="Btn blue darken-3 Bold-ro Btn-expand Sig">Siguiente</a>
                            </div>
                          <?php
                          }
                          ?>
                        </div>
                      </div>
                    </div>
                  </section>
                </form>
              </div>
            </div>
          </div>
        </div>
      <?php } else { ?>
        <div class="Contenedor-principal-der">
          <div class="Contenedor-principal-der-int">
            <div class="Contenedor-principal-titulo">
              <div class="Contenedor-principal-titulo-sec">
                <h2 class="Titulo-seccion">No hay tareas por calificar.</h2><br>
              </div>
              <div class="Contenedor-principal-titulo-sec">
              </div>
            </div>
          </div>
        </div>
      <?php } ?>
    </div>
  </section>
  <script src="dist/js/jquery-1.11.1.min.js"></script>
  <script src="dist/js/noty.min.js"></script>
  <script src="dist/js/inicializar.js"></script>
  <script src="dist/js/materialize.min.js"></script>
  <script src="dist/js/noty.min.js"></script>
  <script src="dist/js/jquery.modal.min.js"></script>
  <script src="dist/js/menu-slide.js?v<?php echo date('YmdHis') ?>"></script>
  <script src="dist/js/modulos-tareas-calificar.js?v<?php echo date('YmdHis') ?>"></script>
</body>

</html>
