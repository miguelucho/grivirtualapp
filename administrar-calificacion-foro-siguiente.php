<?php
session_start();
if (!isset($_SESSION['griapp_user'])) {
  header('Location: login');
}

require_once "dist/libs/conexion.php";


$descripcion_foro = '';

if (!isset($_REQUEST['fr']) || !isset($_REQUEST['fr'])) {
  header('Location: administrar-modulos');
} else {
  $detalles = $db
    ->where('Id_mad', $_REQUEST['fr'])
    ->objectBuilder()->get('modulos_actividades_detalle');

  if ($db->count > 0) {
    $nombre_actividad = $detalles[0]->nombre_mad;

    $foros = $db
      ->where('Id_mad', $_REQUEST['fr'])
      ->objectBuilder()->get('modulo_actividades_foros');

    if ($db->count > 0) {
      $descripcion = $foros[0]->descripcion_maf;

      $ver_archivo = '';

      $documentos = $db
        ->where('Id_maf', $foros[0]->Id_maf)
        ->objectBuilder()->get('modulo_actividades_foros_archivos');

      if ($db->count > 0) {
        foreach ($documentos as $documento) {
          $ver_archivo .= '<p><a href="dist/' . $documento->archivo_mafa . '" target="_blank">Ver Archivo Adjunto</a></p>';
        }
      }
    }

    $nombre_estudiante = '';
    $descripcion_respuesta = '';
    $fecha_respuesta = '';
    $idcalificacion = '';
    $estudiante = '';
    $anterior = '';
    $siguiente = '';

    if (isset($_REQUEST['es'])) {
      $estudiante = $_REQUEST['es'];
    }

    $respondieron = array();

    $respuestas = $db
      ->where('Id_mad', $_REQUEST['fr'])
      ->orderBy('Id_ma', 'ASC')
      ->objectBuilder()->get('modulo_actividades_foros_calificaciones');

    if ($db->count > 0) {
      $existen = 1;

      foreach ($respuestas as $respuesta) {
        $respondieron[] = $respuesta->Id_ma;
      }

      if (count($respondieron) > 0) {
        if (count($respondieron) > 1) {
          if ($estudiante == '') {
            $siguiente = $respondieron[1];
          } else {
            $anterior = $respondieron[array_search($estudiante, $respondieron) - 1];
            $siguiente = $respondieron[array_search($estudiante, $respondieron) + 1];
          }
        }
      }

      if ($estudiante == '') {
        $estudiante = $respondieron[0];
      }

      if ($estudiante == $anterior) {
        $anterior = '';
      }

      if ($estudiante == $siguiente) {
        $siguiente = '';
      }

      $matriculas = $db
        ->where('Id_ma', $estudiante, 'LIKE')
        ->objectBuilder()->get('matriculas');

      if ($db->count > 0) {
        $nombre_estudiante = $matriculas[0]->nombre_ma . ' ' . $matriculas[0]->apellido_ma;
      }

      $respuestas = $db
        ->where('Id_mad', $_REQUEST['fr'])
        ->where('Id_ma', $estudiante, 'LIKE')
        ->objectBuilder()->get('modulo_actividades_foros_calificaciones');

      if ($db->count > 0) {
        foreach ($respuestas as $respuesta) {
          $descripcion_respuesta = $respuesta->descripcion_mafc;
          $fecha_respuesta = $respuesta->fecha_mafc;
          $idcalificacion = $respuesta->Id_mafc;
        }
      }
    } else {
      $existen = 0;
    }
  } else {
    header('Location: administrar-modulos');
  }
}
?>

<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Calificar foros</title>
  <link rel="stylesheet" type="text/css" href="dist/css/fonts.css">
  <?php include("dist/libs/cssvariable/css-variables.php") ?>
  <link rel="stylesheet" type="text/css" href="dist/css/materialize.css">
  <link rel="stylesheet" type="text/css" href="dist/css/load.css">
  <link rel="stylesheet" type="text/css" href="dist/css/noty.css">
  <link rel="stylesheet" type="text/css" href="dist/css/relax.css">
  <link rel="stylesheet" type="text/css" href="dist/css/jquery.modal.css" />
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>

<body>
  <header>
    <div class="Admin-top">
      <?php include("dist/libs/includes-seccion/top-header.php") ?>
    </div>
  </header>
  <section>
    <div class="Contenedor-principal">
      <div class="Contenedor-principal-izq Contenedor-principal-izq-min">
        <?php include("dist/libs/includes-seccion/menu-izq-docentes.php"); ?>
      </div>
      <?php if ($existen == 1) { ?>
        <div class="Contenedor-principal-der">
          <div class="Contenedor-principal-der-int">
            <div class="Contenedor-principal-titulo">
              <div class="Contenedor-principal-titulo-sec">
                <h2 class="Titulo-seccion">Calificar foro</h2><br>
                <h6 class="Cont-nombre-estudiante"><?php echo $nombre_estudiante ?></h6>
              </div>
              <div class="Contenedor-principal-titulo-sec">
              </div>
            </div>
            <div class="Contenedor-desc">

              <div class="Contenedor-desc-int">
                <form id="Calificar-foro">
                  <div class="Contenedor-formularios-bloque">
                    <div class="Colum-uno">
                      <div class="input-field">
                        <div class="Titulo-tarea"><br>
                          <p><strong><?php echo $nombre_actividad ?></strong> </p>
                        </div>
                        <label for="Nombre_tarea">Nombre de la actividad - foro</label>
                      </div>
                    </div>
                  </div>
                  <section>
                    <div class="Contenedor-admin-modulo">
                      <div class="Contenedor-texto-pregunta">
                        <h6>Descripción:</h6>
                        <?php echo $descripcion ?>
                        <br>
                        <?php echo $ver_archivo ?>
                      </div>
                      <div class="Contenedor-texto-tarea">
                        <h6>Respuesta</h6>
                        <?php echo $descripcion_respuesta ?>
                        <p><strong>Ultima actualización:</strong> <?php echo $fecha_respuesta ?></p>
                      </div>
                      <div class="Contenedor-formularios-bloque">
                        <div class="Colum-cuatro">
                          <div class="input-field">
                            <input type="number" name="foro[nota]" value="" placeholder="Ingresa la nota" class="validate" required="">
                            <label for="hora_cierre">Nota para la actividad </label>
                          </div>
                        </div>
                      </div>
                      <div class="Contenedor-formularios-bloque">
                        <div class="Colum-uno">
                          <div class="input-field">
                            <textarea id="textarea1" name="foro[observacion]" class="materialize-textarea"></textarea>
                            <label for="textarea1">Observaciones (opcional)</label>
                          </div>
                        </div>
                      </div>

                      <div class="Contenedor-admin-modulo">
                        <div class="Contenedor-editor-texto">
                          <br>
                          <p>Verifica la información ingresada, luego de hacer clic en guardar.</p>
                        </div>
                        <div class="Contenedor-formularios-bloque">
                          <div class="Colum-cuatro">
                            <input type="hidden" name="foro[idforo]" value="<?php echo $idcalificacion; ?>">
                            <input type="submit" class="Btn Btn-dark Bold-ro Btn-expand" value="Guardar nota">
                          </div>
                          <div class="Colum-cuatro">
                          </div>
                          <?php
                          if ($anterior != '') {
                          ?>
                            <div class="Colum-cuatro Alinear-dere">
                              <a href="administrar-calificacion-foro-siguiente?fr=<?php echo $_REQUEST['fr'] . '&gr=' . $_REQUEST['gr'] . '&mo=' . $_REQUEST['mo'] . '&es=' . $anterior ?>" class="Btn grey lighten-2 Bold-ro Btn-expand Ant">Anterior</a>
                            </div>
                          <?php
                          }
                          ?>
                          <?php
                          if ($siguiente != '') {
                          ?>
                            <div class="Colum-cuatro Alinear-dere">
                              <a href="administrar-calificacion-foro-siguiente?fr=<?php echo $_REQUEST['fr'] . '&gr=' . $_REQUEST['gr'] . '&mo=' . $_REQUEST['mo'] . '&es=' . $siguiente ?>" class="Btn blue darken-3 Bold-ro Btn-expand Sig">Siguiente</a>
                            </div>
                          <?php
                          }
                          ?>
                        </div>
                      </div>
                    </div>
                  </section>
                </form>
              </div>
            </div>
          </div>
        </div>
      <?php } else { ?>
        <div class="Contenedor-principal-der">
          <div class="Contenedor-principal-der-int">
            <div class="Contenedor-principal-titulo">
              <div class="Contenedor-principal-titulo-sec">
                <h2 class="Titulo-seccion">No hay foros por calificar.</h2><br>
              </div>
              <div class="Contenedor-principal-titulo-sec">
              </div>
            </div>
          </div>
        </div>
      <?php } ?>
    </div>
  </section>
  <script src="dist/js/jquery-1.11.1.min.js"></script>
  <script src="dist/js/noty.min.js"></script>
  <script src="dist/js/inicializar.js"></script>
  <script src="dist/js/materialize.min.js"></script>
  <script src="dist/js/noty.min.js"></script>
  <script src="dist/js/jquery.modal.min.js"></script>
  <script src="dist/js/menu-slide.js?v<?php echo date('YmdHis') ?>"></script>
  <script src="dist/js/modulos-foros-calificar.js?v<?php echo date('YmdHis') ?>"></script>
</body>

</html>
