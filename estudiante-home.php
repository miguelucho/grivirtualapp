<?php
session_start();
if (!isset($_SESSION['griapp_user'])) {
  header('Location: login');
}

require_once "dist/libs/conexion.php";

$ls_cursos = '';

$matriculas = $db
  ->where('Id_ma', $_SESSION['griapp_IDtipo'])
  ->objectBuilder()->get('matriculas');

if ($db->count > 0) {
  $programas = $db
    ->where('Id_pr', $matriculas[0]->programa_ma)
    ->objectBuilder()->get('programas');

  if ($db->count > 0) {
    $detalles_matricula = $db
      ->where('Id_ma', $matriculas[0]->Id_ma)
      ->objectBuilder()->get('matriculas_detalles');

    if ($db->count > 0) {
      $cursos = $db
        ->where('Id_cu', $detalles_matricula[0]->curso_md)
        ->objectBuilder()->get('cursos');

      $nombre_docente = '';

      $grupos = $db
        ->where('Id_gr', $detalles_matricula[0]->grupo_md)
        ->objectBuilder()->get('grupos');

      if ($db->count > 0) {
        $docentes = $db
          ->where('Id_do', $grupos[0]->Id_do)
          ->objectBuilder()->get('docentes');

        if ($db->count > 0) {
          $nombre_docente = $docentes[0]->nombre_do . ' ' . $docentes[0]->apellido_do;
        }
      }

      if ($db->count > 0) {
        $totales = 0;
        $completos = 0;

        $actividades = $db
          ->where('Id_gr', $detalles_matricula[0]->grupo_md)
          ->objectBuilder()->get('modulos_actividades');

        if ($db->count > 0) {
          foreach ($actividades as $actividad) {
            $detalles = $db
              ->where('Id_ma', $actividad->Id_ma)
              ->objectBuilder()->get('modulos_actividades_detalle');

            if ($db->count > 0) {
              foreach ($detalles as $detalle) {
                $totales++;
                switch ($detalle->tipo_mad) {
                  case '1':
                    $respuesta = $db
                      ->where('Id_mad', $detalle->Id_mad)
                      ->where('Id_ma', $_SESSION['griapp_IDtipo'])
                      ->objectBuilder()->get('modulo_actividades_tareas_calificaciones');

                    if ($db->count > 0) {
                      $completos++;
                    }
                    break;
                  case '3':
                    $respuesta = $db
                      ->where('Id_mad', $detalle->Id_mad)
                      ->where('Id_ma', $_SESSION['griapp_IDtipo'])
                      ->objectBuilder()->get('modulo_actividades_foros_calificaciones');

                    if ($db->count > 0) {
                      $completos++;
                    }
                    break;
                  case '5':
                    $respuesta = $db
                      ->where('Id_mad', $detalle->Id_mad)
                      ->where('Id_ma', $_SESSION['griapp_IDtipo'])
                      ->objectBuilder()->get('modulo_actividades_cuestionario_calificaciones');

                    if ($db->count > 0) {
                      $completos++;
                    }
                    break;
                }
              }
            }
          }
        }

        // echo $completos . ' / ' . $totales;

        $porcentaje = round(($completos / $totales * 100), 2);

        $ls_cursos .= '<div class="Contenedor-dash-bloque">
                        <a href="estudiante-curso?cu=' . $detalles_matricula[0]->Id_md . '">
                          <div class="Contenedor-dash-bloque-sup">
                            <div class="Contenedor-dash-bloque-infosup">
                              <span class="Cont-dash-nmcurso">' . $programas[0]->nombre_pr . ' - ' . $cursos[0]->nombre_cu . '</span>
                            </div>
                              <span class="Cont-dash-nmprofe">Por: ' . $nombre_docente . '</span>
                              <span class="Cont-dash-nprogreso"><strong>Progreso:</strong> ' . $porcentaje . '%</span>
                          </div>
                          <div class="Contenedor-dash-bloque-inf">
                            <div class="progress">
                                <div class="determinate" style="width: ' . $porcentaje . '%"></div>
                            </div>
                          </div>
                        </a>
                      </div>';
      }
    }
  }
}

?>
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Dashboard Estudiante</title>
  <link rel="stylesheet" type="text/css" href="dist/css/fonts.css">
  <?php include("dist/libs/cssvariable/css-variables.php") ?>
  <link rel="stylesheet" type="text/css" href="dist/css/materialize.css">
  <link rel="stylesheet" type="text/css" href="dist/css/load.css">
  <link rel="stylesheet" type="text/css" href="dist/css/noty.css">
  <link rel="stylesheet" type="text/css" href="dist/css/relax.css">
  <link rel="stylesheet" type="text/css" href="dist/css/jquery.modal.css" />
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>

<body>
  <header>
    <div class="Admin-top">
      <?php include("dist/libs/includes-seccion/top-header.php") ?>
    </div>
  </header>
  <section>
    <div class="Contenedor-principal">
      <div class="Contenedor-principal-izq Contenedor-principal-izq-min">
        <?php include("dist/libs/includes-seccion/menu-izq-estudiantes.php"); ?>
      </div>
      <div class="Contenedor-principal-der">
        <div class="Contenedor-principal-der-int">

          <div class="Contenedor-dash">
            <div class="Contenedor-principal-titulo">
              <div class="Contenedor-principal-titulo-sec">
                <h2 class="Titulo-seccion">Apoyando a construir tu futuro Gricompany</h2><br>
              </div>
            </div>
            <div class="Contenedor-dash-int">
              <div class="Contenedor-dash-int-filtro">
                <div class="Contenedor-dash-int-filtro-celda">
                  <div class="input-field">
                    <select>
                      <option value="1">En progreso</option>
                      <option value="2">Completados</option>
                      <option value="3">Archivados</option>
                      <option value="4">Todos</option>
                    </select>
                    <label>Selecciona</label>
                  </div>
                </div>
              </div>
              <div class="Contenedor-dash-int-contenido">
                <div class="Contenedor-dash-interno">
                  <?php echo $ls_cursos ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <script src="dist/js/jquery-1.11.1.min.js"></script>
  <script src="dist/js/noty.min.js"></script>
  <script src="dist/js/inicializar.js"></script>
  <script src="dist/js/materialize.min.js"></script>
  <script src="dist/js/noty.min.js"></script>
  <script src="dist/js/croppie.min.js"></script>
  <script src="dist/js/jquery.modal.min.js"></script>
  <script src="dist/js/menu-slide.js?v<?php echo date('YmdHis') ?>"></script>
  <!-- <script src="dist/js/modulos-tareas-administrar.js?v<?php echo date('YmdHis') ?>"></script> -->
</body>

</html>
