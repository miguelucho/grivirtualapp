<?php
session_start();
if (!isset($_SESSION['griapp_user'])) {
  header('Location: login');
}

require_once "dist/libs/conexion.php";

$nombre = '';
$login = '';
$nm_programa = '';
$nm_curso = '';
$codigo = '';
$dsc_modulo = '';
$ls_cortes = '';
$actividades_creadas = 'data-existente="0"';


$usuarios = $db
  ->where('Id_us', $_SESSION['griapp_user'])
  ->objectBuilder()->get('usuarios_app');

if ($db->count > 0) {
  $nombre = $usuarios[0]->nombre_us;
  $login = $usuarios[0]->login_us;
}

if (!isset($_REQUEST['gr']) || !isset($_REQUEST['mo'])) {
  header('Location: administrar-modulos');
} else {
  $grupos = $db
    ->where('Id_gr', $_REQUEST['gr'])
    ->objectBuilder()->get('grupos');

  if ($db->count == 0) {
    header('Location: administrar-modulos');
  } else {
    $programas = $db
      ->where('Id_pr', $grupos[0]->Id_pr)
      ->objectBuilder()->get('programas');

    if ($db->count > 0) {
      $nm_programa = $programas[0]->nombre_pr;

      $cortes = $db
        ->where('Id_pr', $programas[0]->Id_pr)
        // ->where('periodo_co', date('Y'))
        ->orderBy('Id_co', 'ASC')
        ->objectBuilder()->get('cortes');

      if ($db->count > 0) {
        $txt_corte = ['Primer', 'Segundo', 'Tercer', 'Cuarto'];

        $i = 0;
        foreach ($cortes as $corte) {
          $ls_cortes .= '<option value="' . $corte->Id_co . '" data-porcentaje="' . $corte->porcentaje_co . '">' . $txt_corte[$i] . '</option>';
          $i++;
        }
      }
    }

    $cursos = $db
      ->where('Id_cu', $grupos[0]->Id_cu)
      ->objectBuilder()->get('cursos');

    if ($db->count > 0) {
      $nm_curso = $cursos[0]->nombre_cu;
    }

    $codigo = $grupos[0]->codigo_gr;

    $modulos = $db
      ->where('Id_mo', $_REQUEST['mo'])
      ->objectBuilder()->get('modulos');

    if ($db->count > 0) {
      $dsc_modulo = $modulos[0]->descripcion_mo;
    }

    $actividades = $db
      ->where('Id_gr', $_REQUEST['gr'])
      ->where('Id_mo', $_REQUEST['mo'])
      ->objectBuilder()->get('modulos_actividades');

    if ($db->count > 0) {
      $periodo = $actividades[0]->periodo_ma;
      $idma = $actividades[0]->Id_ma;
      $dsc_modulo = $actividades[0]->descripcion_ma;

      $detalles = $db
        ->where('Id_ma', $actividades[0]->Id_ma)
        ->objectBuilder()->get('modulos_actividades_detalle');


      $total_actividades = $db->count;
      $actividades_creadas = 'data-existente="' . $db->count . '"';

      foreach ($detalles as $detalle) {
        $ls_tipo = '';

        $tipos = $db
          ->orderBy('Id_am', 'ASC')
          ->objectBuilder()->get('actividades_modulos');

        foreach ($tipos as $tipo) {
          $ls_tipo .= ' <option value="' . $tipo->Id_am . '" ' . ($tipo->Id_am == $detalle->tipo_mad ? "selected" : "") . '>' . $tipo->nombre_am . '</option>';
        }

        $cortes = $db
          ->where('Id_pr', $programas[0]->Id_pr)
          ->where('periodo_co', $actividades[0]->periodo_ma)
          ->orderBy('Id_co', 'ASC')
          ->objectBuilder()->get('cortes');

        if ($db->count > 0) {
          $txt_corte = ['Primer', 'Segundo', 'Tercer', 'Cuarto'];

          $i = 0;
          $ls_cortes2 = '';
          foreach ($cortes as $corte) {
            $ls_cortes2 .= '<option value="' . $corte->Id_co . '" ' . ($corte->Id_co == $detalle->Id_co ? "selected" : "") . '  data-porcentaje="' . $corte->porcentaje_co . '">' . $txt_corte[$i] . '</option>';
            $i++;
          }
        }

        switch ($detalle->tipo_mad) {
          case '1':
            $link_administrar = 'administrar-modulos-actividades-tarea?ta=' . $detalle->Id_mad . '&gr=' . $_REQUEST['gr'] . '&mo=' . $_REQUEST['mo'];
            break;
          case '3':
            $link_administrar = 'administrar-modulos-actividades-foro?fo=' . $detalle->Id_mad . '&gr=' . $_REQUEST['gr'] . '&mo=' . $_REQUEST['mo'];
            break;
          case '5':
            $link_administrar = 'administrar-modulos-actividades-cuestionario?cu=' . $detalle->Id_mad . '&gr=' . $_REQUEST['gr'] . '&mo=' . $_REQUEST['mo'];
            break;
         /*  case '4':
            $link_administrar = 'administrar-modulos-actividades-cuestionario?cu=' . $detalle->Id_mad . '&gr=' . $_REQUEST['gr'] . '&mo=' . $_REQUEST['mo'];
            break; */
        }

        $ls_actividades .= '<tr class="Temp-actividad">
                          <td>
                            <div class="input-field">
                              <select name="actividad[tipo][]" class="Select-tipo" required>
                                <option value="" disabled selected>Seleccionar</option>
                                ' . $ls_tipo . '
                              </select>
                              <label>Tipo</label>
                            </div>
                          </td>
                          <td>
                            <div class="input-field Ig-abajo">
                              <input placeholder="%" id="" name="actividad[valor][]" type="number" class="Actividad-porcentaje validate" value="' . $detalle->valor_mad . '">
                              <label for="">Valor</label>
                            </div>
                          </td>
                          <td>
                            <div class="input-field Ig-abajo">
                              <input type="hidden" name="actividad[actividad][]" value="' . $detalle->Id_mad . '">
                              <input placeholder="Nombre de la actividad" id="first_name" name="actividad[nombre][]" type="text" class="validate" value="' . $detalle->nombre_mad . '">
                              <label for="first_name">Nombre</label>
                            </div>
                          </td>
                          <td>
                            <div class="input-field">
                              <select name="actividad[corte][]" class="Select-corte">
                                <option value="" disabled>Seleccionar</option>
                                ' . $ls_cortes2 . '
                              </select>
                              <label>Corte</label>
                            </div>
                          </td>
                          <td>
                            <a href="javascript:void(0)" class="Btn-table-rojo Btn-bloquear Eliminar"><span><i class="icon-blocked"></i>Eliminar</span></a>
                          </td>
                          <td>
                            <a href="' . $link_administrar . '" data-target="editar" class="Btn-ver Btn-table-verde"><i class="icon-pencil"></i>Administrar</a>
                          </td>
                        </tr>';
      }
    } else {
      $idma = 0;
      $total_actividades = 1;
      $periodo = date('Y');

      $ls_tipo = '';

      $tipos = $db
        ->orderBy('Id_am', 'ASC')
        ->objectBuilder()->get('actividades_modulos');

      foreach ($tipos as $tipo) {
        $ls_tipo .= ' <option value="' . $tipo->Id_am . '" >' . $tipo->nombre_am . '</option>';
      }

      $ls_actividades = '<tr>
                          <td>
                            <div class="input-field">
                              <select name="actividad[tipo][]" class="Select-tipo" required>
                                <option value="" disabled selected>Seleccionar</option>
                                ' . $ls_tipo . '
                              </select>
                              <label>Tipo</label>
                            </div>
                          </td>
                          <td>
                            <div class="input-field Ig-abajo">
                              <input placeholder="%" id="" name="actividad[valor][]" type="number" class="Actividad-porcentaje validate">
                              <label for="">Valor</label>
                            </div>
                          </td>
                          <td>
                            <div class="input-field Ig-abajo">
                              <input placeholder="Nombre de la actividad" id="first_name" name="actividad[nombre][]" type="text" class="validate">
                              <label for="first_name">Nombre</label>
                            </div>
                          </td>
                          <td>
                            <div class="input-field">
                              <select name="actividad[corte][]" class="Select-corte">
                                <option value="" disabled selected>Seleccionar</option>
                                ' . $ls_cortes . '
                              </select>
                              <label>Corte</label>
                            </div>
                          </td>
                          <td>
                          </td>
                          <td>
                          </td>
                        </tr>';
    }
  }
}
?>
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Administrador Modulos</title>
  <link rel="stylesheet" type="text/css" href="dist/css/fonts.css">
  <?php include("dist/libs/cssvariable/css-variables.php") ?>
  <link rel="stylesheet" type="text/css" href="dist/css/materialize.css">
  <link rel="stylesheet" type="text/css" href="dist/css/load.css">
  <link rel="stylesheet" type="text/css" href="dist/css/noty.css">
  <link rel="stylesheet" type="text/css" href="dist/css/relax.css">
  <link rel="stylesheet" type="text/css" href="dist/css/croppie.css">
  <link rel="stylesheet" type="text/css" href="dist/css/jquery.modal.css" />
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>

<body>
  <header>
    <div class="Admin-top">
      <?php include("dist/libs/includes-seccion/top-header.php") ?>
    </div>
  </header>
  <section>
    <div class="Contenedor-principal">
      <div class="Contenedor-principal-izq Contenedor-principal-izq-min">
        <?php include("dist/libs/includes-seccion/menu-izq-docentes.php"); ?>
      </div>
      <div class="Contenedor-principal-der">
        <div class="Contenedor-principal-der-int">
          <div class="Contenedor-principal-titulo">
            <div class="Contenedor-principal-titulo-sec">
              <h2 class="Titulo-seccion">Administrar actividades: <?php echo $nm_programa; ?><br><?php echo $nm_curso; ?> - <?php echo $codigo; ?></h2>
            </div>
            <div class="Contenedor-principal-titulo-sec">
              <div class="Btn-flotante-crear">
                <a href="administrar-modulos-interno?gr=<?php echo $_REQUEST['gr']; ?>" data-target="crear" data-position="left" data-tooltip="Volver atras" class="modal-trigger tooltipped btn-floating btn-large waves-effect waves-light blue-grey"><i class="material-icons">keyboard_backspace</i></a>
              </div>
            </div>
          </div>
          <div class="Contenedor-desc">
            <p>Administra el contenido y actividades.</p>
            <div class="Contenedor-desc-int">
              <section>
                <div class="Contenedor-admin-modulo">
                  <p>Ingrese a continuación la descripción del modulo:</p>
                  <form id="Nueva-actividad">
                    <div class="Contenedor-editor-texto">
                      <textarea id="descripcion-modulo" name="actividad[modulodescripcion]"><?php echo $dsc_modulo; ?></textarea>
                    </div>
                    <p>Ingresa la cantidad de actividades para este modulo.</p>
                    <div class="Contenedor-formularios-bloque">
                      <div class="Colum-cuatro">
                        <div class="input-field col m6">
                          <a class="waves-effect waves-light btn Menos-actividad blue darken-3"><i class="material-icons left"></i>-</a>
                          <input name="actividad[cantidad]" type="number" value="<?php echo $total_actividades; ?>" min="1" step="1" readonly="" class="Cant-actividades" required="" <?php echo $actividades_creadas; ?> style="width: 40%; text-align:center;">
                          <label for="cantidad" class="active">Cantidad</label>
                          <a class="waves-effect waves-light btn Mas-actividad blue darken-3"><i class="material-icons left"></i>+</a>
                        </div>
                      </div>
                    </div>
                    <div class="Contenedor-admin-modulo">
                      <table>
                        <thead>
                          <tr>
                            <th class="Ancho-tres">Tipo</th>
                            <th class="Ancho-dos">Procentaje</th>
                            <th class="Ancho-cuatro">Nombre</th>
                            <th class="Ancho-dos">Corte</th>
                            <th>Eliminar</th>
                            <th>Acción</th>
                          </tr>
                        </thead>
                        <tbody class="Lista-actividades">
                          <?php echo $ls_actividades; ?>
                        </tbody>
                      </table>
                      <div class="Contenedor-editor-texto">
                        <br>
                        <p>Verifica la información ingresada, luego de hacer clic en guadar se habilitará la opción de administar cada una de las actividades segun el tipo seleccionado.</p>
                      </div>
                      <div class="Contenedor-editor-texto">
                        <div class="Colum-uno">
                          <input type="hidden" name="actividad[grupo]" value="<?php echo $_REQUEST['gr']; ?>" class="Grupo-actividad">
                          <input type="hidden" name="actividad[modulo]" value="<?php echo $_REQUEST['mo']; ?>">
                          <input type="hidden" name="actividad[periodo]" value="<?php echo date('Y'); ?>">
                          <input type="hidden" name="actividad[idactividad]" value="<?php echo $idma; ?>">
                          <input type="submit" class="Btn Btn-dark Bold-ro Btn-expand" value="Guardar">
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </section>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div style="display: none;">
      <select class="Hide-cortes">
        <?php echo $ls_cortes; ?>
      </select>
    </div>

    <div id="copiar" class="modal modal-form">
      <div class="modal-content">
        <div class="modal-header">
          <div class="row">
            <div class="col s12">
              <button type="button" class="btn-floating modal-close waves-effect waves-light btn right blue-grey darken-4">
                &times;
              </button>
              <h3 class="center-align Titulo-grande"></h3>
            </div>
          </div>
        </div>
        <div class="modal-body">
          <form id="Nueva-copia">
            <div class="row">
              <div class="col s12">
                <div class="row">
                  <div class="input-field col m12">
                    <select name="" class="Sel-existente" required>
                      <option value="" selected>Seleccionar</option>
                    </select>
                    <label class="Sel-copia">Seleccionar</label>
                  </div>
                  <div class="row Div-pocentajes">
                    <div class="input-field col m12">
                      <input id="nombre-existente" name="" type="text" class="validate" required="">
                      <label for="nombre-existente">Nombre</label>
                    </div>
                  </div>
                </div>
                <div class="row center-align">
                  <button class="btn waves-effect waves-light btn blue-grey darken-4" type="submit" name="action">Guardar
                  </button>
                  <br />
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>
  <script src="dist/js/jquery-1.11.1.min.js"></script>
  <script src="dist/js/noty.min.js"></script>
  <script src="dist/js/inicializar.js"></script>
  <script src="dist/js/materialize.min.js"></script>
  <script src="dist/js/noty.min.js"></script>
  <script src="dist/js/ckeditor/ckeditor.js"></script>
  <script src="dist/js/menu-slide.js?v<?php echo date('YmdHis') ?>"></script>
  <script src="dist/js/modulos-actividades.js?v<?php echo date('YmdHis') ?>"></script>
</body>

</html>
