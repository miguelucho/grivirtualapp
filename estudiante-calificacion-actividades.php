<?php
session_start();
if (!isset($_SESSION['griapp_user'])) {
  header('Location: login');
}

require_once "dist/libs/conexion.php";

$nombre = '';
$login = '';
$nm_programa = '';
$nm_curso = '';
$codigo = '';
$dsc_modulo = '';
$ls_cortes = '';
$actividades_creadas = 'data-existente="0"';


$usuarios = $db
  ->where('Id_us', $_SESSION['griapp_user'])
  ->objectBuilder()->get('usuarios_app');

if ($db->count > 0) {
  $nombre = $usuarios[0]->nombre_us;
  $login = $usuarios[0]->login_us;
}

if (!isset($_REQUEST['gr']) || !isset($_REQUEST['mo'])) {
  header('Location: estudiante-home');
} else {
  $grupos = $db
    ->where('Id_gr', $_REQUEST['gr'])
    ->objectBuilder()->get('grupos');

  if ($db->count == 0) {
    header('Location: estudiante-home');
  } else {
    $programas = $db
      ->where('Id_pr', $grupos[0]->Id_pr)
      ->objectBuilder()->get('programas');

    if ($db->count > 0) {
      $nm_programa = $programas[0]->nombre_pr;

      $cortes = $db
        ->where('Id_pr', $programas[0]->Id_pr)
        ->orderBy('Id_co', 'ASC')
        ->objectBuilder()->get('cortes');

      if ($db->count > 0) {
        $txt_corte = ['Primer', 'Segundo', 'Tercer', 'Cuarto'];

        $i = 0;
        foreach ($cortes as $corte) {
          $ls_cortes .= '<option value="' . $corte->Id_co . '" data-porcentaje="' . $corte->porcentaje_co . '">' . $txt_corte[$i] . '</option>';
          $i++;
        }
      }
    }

    $cursos = $db
      ->where('Id_cu', $grupos[0]->Id_cu)
      ->objectBuilder()->get('cursos');

    if ($db->count > 0) {
      $nm_curso = $cursos[0]->nombre_cu;
    }

    $codigo = $grupos[0]->codigo_gr;

    $modulos = $db
      ->where('Id_mo', $_REQUEST['mo'])
      ->objectBuilder()->get('modulos');

    if ($db->count > 0) {
      $dsc_modulo = $modulos[0]->descripcion_mo;
    }

    $actividades = $db
      ->where('Id_gr', $_REQUEST['gr'])
      ->where('Id_mo', $_REQUEST['mo'])
      ->objectBuilder()->get('modulos_actividades');

    if ($db->count > 0) {
      $periodo = $actividades[0]->periodo_ma;
      $idma = $actividades[0]->Id_ma;
      $dsc_modulo = $actividades[0]->descripcion_ma;

      $detalles = $db
        ->where('Id_ma', $actividades[0]->Id_ma)
        ->objectBuilder()->get('modulos_actividades_detalle');


      $total_actividades = $db->count;
      $actividades_creadas = 'data-existente="' . $db->count . '"';

      foreach ($detalles as $detalle) {
        $ls_tipo = '';

        $tipos = $db
          ->where('Id_am', $detalle->tipo_mad)
          ->objectBuilder()->get('actividades_modulos');

        foreach ($tipos as $tipo) {
          $ls_tipo = $tipo->nombre_am;
        }

        $cortes = $db
          ->where('Id_co', $detalle->Id_co)
          ->objectBuilder()->get('cortes');

        if ($db->count > 0) {
          $txt_corte = ['Primer', 'Segundo', 'Tercer', 'Cuarto'];

          $ls_cortes2 = $txt_corte[$cortes[0]->Id_co - 1];
        }

        $calificacion = '';

        switch ($detalle->tipo_mad) {
          case '1':
            $link_administrar = 'administrar-calificacion-tareas?ta=' . $detalle->Id_mad . '&gr=' . $_REQUEST['gr'] . '&mo=' . $_REQUEST['mo'] . '&es=' . $_SESSION['griapp_IDtipo'];

            $tareas = $db
              ->where('Id_mad', $detalle->Id_mad)
              ->where('Id_ma', $_SESSION['griapp_IDtipo'])
              ->objectBuilder()->get('modulo_actividades_tareas_calificaciones');

            if ($db->count > 0) {
              $calificacion = $tareas[0]->calificacion_matc;
            }

            break;
          case '3':
            $link_administrar = 'administrar-calificacion-foro?fr=' . $detalle->Id_mad . '&gr=' . $_REQUEST['gr'] . '&mo=' . $_REQUEST['mo'] . '&es=' . $_SESSION['griapp_IDtipo'];

            $foros = $db
              ->where('Id_mad', $detalle->Id_mad)
              ->where('Id_ma', $_SESSION['griapp_IDtipo'])
              ->objectBuilder()->get('modulo_actividades_foros_calificaciones');

            if ($db->count > 0) {
              $calificacion = $foros[0]->calificacion_mafc;
            }

            break;
          case '5':
            $link_administrar = 'administrar-calificacion-cuestionario?cu=' . $detalle->Id_mad . '&gr=' . $_REQUEST['gr'] . '&mo=' . $_REQUEST['mo'] . '&es=' . $_SESSION['griapp_IDtipo'];

            $cuestionarios = $db
              ->where('Id_mad', $detalle->Id_mad)
              ->where('Id_ma', $_SESSION['griapp_IDtipo'])
              ->objectBuilder()->get('modulo_actividades_cuestionario_calificaciones');

            if ($db->count > 0) {
              $calificacion = 0;

              foreach ($cuestionarios as $cuestionario) {
                $calificacion += $cuestionario->calificacion_macc;
              }
            }

            break;
        }

        $ls_actividades .= '<tr class="Temp-actividad">
                          <td>
                            <div class="input-field">
                                ' . $ls_tipo . '
                            </div>
                          </td>
                          <td>
                            <div class="input-field Ig-abajo">
                              ' . $detalle->valor_mad . '
                            </div>
                          </td>
                          <td>
                            <div class="input-field Ig-abajo">
                             ' . $detalle->nombre_mad . '
                            </div>
                          </td>
                          <td>
                            <div class="input-field">
                               ' . $ls_cortes2 . '
                            </div>
                          </td>
                          <td>
                            <div class="input-field">
                               ' . $calificacion . '
                            </div>
                          </td>
                        </tr>';
      }
    } else {
      $ls_actividades = '';
    }
  }
}
?>
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Administrador Modulos</title>
  <link rel="stylesheet" type="text/css" href="dist/css/fonts.css">
  <?php include("dist/libs/cssvariable/css-variables.php") ?>
  <link rel="stylesheet" type="text/css" href="dist/css/materialize.css">
  <link rel="stylesheet" type="text/css" href="dist/css/load.css">
  <link rel="stylesheet" type="text/css" href="dist/css/noty.css">
  <link rel="stylesheet" type="text/css" href="dist/css/relax.css">
  <link rel="stylesheet" type="text/css" href="dist/css/jquery.modal.css" />
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>

<body>
  <header>
    <div class="Admin-top">
      <?php include("dist/libs/includes-seccion/top-header.php") ?>
    </div>
  </header>
  <section>
    <div class="Contenedor-principal">
      <div class="Contenedor-principal-izq Contenedor-principal-izq-min">
        <?php include("dist/libs/includes-seccion/menu-izq-estudiantes.php"); ?>
      </div>
      <div class="Contenedor-principal-der">
        <div class="Contenedor-principal-der-int">
          <div class="Contenedor-principal-titulo">
            <div class="Contenedor-principal-titulo-sec">
              <h2 class="Titulo-seccion">Ver actividades: <?php echo $nm_programa; ?><br><?php echo $nm_curso; ?> - <?php echo $codigo; ?></h2>
            </div>
            <div class="Contenedor-principal-titulo-sec">
              <div class="Btn-flotante-crear">
                <a href="estudiante-modulos-calificaciones?gr=<?php echo $_REQUEST['gr'] ?>" data-target="crear" data-position="left" data-tooltip="Volver atras" class="modal-trigger tooltipped btn-floating btn-large waves-effect waves-light blue-grey"><i class="material-icons">keyboard_backspace</i></a>
              </div>
            </div>
          </div>
          <div class="Contenedor-desc">
            <p>Calificaciones de las actividades.</p>
            <div class="Contenedor-desc-int">
              <section>
                <div class="Contenedor-admin-modulo">
                  <div class="Contenedor-admin-modulo">
                    <table>
                      <thead>
                        <tr>
                          <th class="Ancho-tres">Tipo</th>
                          <th class="Ancho-dos">Procentaje</th>
                          <th class="Ancho-cuatro">Nombre</th>
                          <th class="Ancho-dos">Corte</th>
                          <th class="Ancho-dos">Calificación</th>
                        </tr>
                      </thead>
                      <tbody class="Lista-actividades">
                        <?php echo $ls_actividades; ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <script src="dist/js/jquery-1.11.1.min.js"></script>
  <script src="dist/js/noty.min.js"></script>
  <script src="dist/js/inicializar.js"></script>
  <script src="dist/js/materialize.min.js"></script>
  <script src="dist/js/noty.min.js"></script>
  <script src="dist/js/menu-slide.js?v<?php echo date('YmdHis') ?>"></script>
</body>

</html>
