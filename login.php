<?php
session_start();
if (isset($_SESSION['griapp_user'])) {
	header('Location: perfil-usuario');
}
?>
<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
	<!-- <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" /> -->
	<meta name="keywords" lang="es" content="">
	<meta name="robots" content="All">
	<meta name="description" lang="es" content="">
	<title>Iniciar sesión | Gricompany Virtual</title>
	<link rel="stylesheet" type="text/css" href="dist/css/fonts.css">
	<?php include("dist/libs/cssvariable/css-variables.php") ?>
	<link rel="stylesheet" type="text/css" href="dist/css/load.css">
	<link rel="stylesheet" type="text/css" href="dist/css/materialize.css">
</head>

<body>
	<div class="Cont-globalpantalla">
		<div class="Cont-globalpantalla-login-izq">
			<div class="Cont-login">
				<div class="Cont-login-int">
					<div class="Login Forms">
						<div class="Login-logo">
							<img src="https://appgrivirtual.gricompany.co/dist/assets/images/logo.png">
						</div>
						<h5 class="Alineado-centro">Iniciar Sesión</h5>
						<p class="Alineado-centro" id="Usuario-error"><span class="Login-texto-rojo"></span></p>
						<form class="Login-form col s12" id="login">
							<div class="input-field">
								<input id="Usuario" type="text" name="login[usuario]" class="validate" required="">
								<label for="Usuario">Usuario</label>
							</div>
							<div class="input-field">
								<input type="password" id="Contraseña" name="login[contrasena]" class="validate" required="">
								<label for="Contraseña">Contraseña</label>
							</div>
							<div>
								<input class="Btn Btn-dark Fuente-bold Max-ancho" value="Iniciar Sesión" type="submit">
							</div>
						</form>
						<div class="Login-footer">
							<p class="Alineado-centro"><a href="restablecer"><span>Olvide mi contraseña</span></a></p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="Cont-globalpantalla-login-der"></div>
	</div>
	<script src="dist/js/jquery-1.11.1.min.js"></script>
	<script src="dist/js/materialize.min.js"></script>
	<script src="dist/js/login.js?v<?php echo date('YmdHis') ?>"></script>
</body>

</html>