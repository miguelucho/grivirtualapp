<?php
session_start();
if (!isset($_SESSION['griapp_user'])) {
	header('Location: login');
}

require_once "dist/libs/conexion.php";

$nombre = '';
$login = '';

$usuarios = $db
	->where('Id_us', $_SESSION['griapp_user'])
	->objectBuilder()->get('usuarios_app');

if ($db->count > 0) {
	$nombre = $usuarios[0]->nombre_us;
	$login = $usuarios[0]->login_us;
}
?>
<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
	<!-- <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" /> -->
	<meta name="keywords" lang="es" content="">
	<meta name="robots" content="All">
	<meta name="description" lang="es" content="">
	<title>Perfil | Gricompany Virtual</title>
	<link rel="stylesheet" type="text/css" href="dist/css/fonts.css">
	<?php include("dist/libs/cssvariable/css-variables.php") ?>
	<link rel="stylesheet" type="text/css" href="dist/css/materialize.css">
	<link rel="stylesheet" type="text/css" href="dist/css/material-icons.css">
	<link rel="stylesheet" type="text/css" href="dist/css/load.css">
	<link rel="stylesheet" type="text/css" href="dist/css/noty.css">
	<link rel="stylesheet" type="text/css" href="dist/css/relax.css">
	<link rel="stylesheet" type="text/css" href="dist/css/croppie.css">
	<link rel="stylesheet" type="text/css" href="dist/css/jquery.modal.css" />
	<style type="text/css" media="screen">
		#upload-demo {
			width: 100%;
			height: 60vh;
		}

		.modal-title {
			padding: 5px !important;
		}

		.modal-text {
			padding: 5px !important;
		}
	</style>
</head>

<body>
	<header>
		<div class="Admin-top">
			<?php include("dist/libs/includes-seccion/top-header.php") ?>
		</div>
	</header>

	<div class="Contenedor-principal">
		<div class="Contenedor-principal-izq Contenedor-principal-izq-min">
			<?php
				if($_SESSION['griapp_tipo'] == 1){
					include("dist/libs/includes-seccion/menu-izq-docentes.php");
				}else{
					include("dist/libs/includes-seccion/menu-izq-estudiantes.php");
				}
			?>
		</div>
		<div class="Contenedor-principal-der">
			<div class="Contenedor-principal-der-int">
				<div class="Contenedor-principal-titulo">
					<h2 class="Perfil-nombre">Hola <?php echo $nombre ?></h2>
				</div>
				<div class="Contenedor-desc">
					<p>Mantén actualizada la información y actualiza la contraseña cuando requieras.</p><br>
					<div class="Contenedor-desc-int">
						<form id="Perfil">
							<div class="Contenedor-perfil">
								<div class="Contenedor-perfil-sec">
									<div class="Perfil-imagen">
										<img src="dist/assets/images/usuarios/<?php echo $imagen ?>">
									</div>
									<div>
										<div class="file-field input-field">
											<div class="btn indigo darken-3">
												<span>Seleccionar</span>
												<input type="file" id="imagen_perfil" accept="image/*">
											</div>
											<div class="file-path-wrapper">
												<input class="file-path validate" placeholder="Imagen" type="text">
											</div>
										</div>
									</div>
								</div>
								<div class="Contenedor-perfil-sec">
									<div class="Contenedor-formularios-bloque">
										<div class="Colum-uno">
											<div class="input-field">
												<input id="Nombre" type="text" name="usuario[Nombreusu]" class="validate" value="<?php echo $nombre ?>" required="">
												<label for="Nombre">Nombre completo</label>
											</div>
										</div>
									</div>
									<div class="Contenedor-formularios-bloque">
										<div class="Colum-uno">
											<div class="input-field">
												<input id="Correo" type="text" name="usuario[Correo]" value="<?php echo $login ?>">
												<label for="Correo">Correo electrónico / usuario</label>
											</div>
										</div>
									</div>
									<p>Actualizar contraseña:</p>
									<div class="Contenedor-formularios-bloque">
										<div class="Colum-uno">
											<div class="input-field">
												<input id="Contrasena" type="password" name="usuario[Contrasena]" class="">
												<label for="Contrasena">Contraseña</label>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="Contenedor-perfil">
								<div class="Contenedor-perfil-sec">
									<div class="Colum-uno">
										<input type="submit" class="Btn Btn-azul Bold-ro Btn-expand" value="Guardar">
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="dist/js/jquery-1.11.1.min.js"></script>
	<script src="dist/js/noty.min.js"></script>
	<script src="dist/js/inicializar.js"></script>
	<script src="dist/js/materialize.min.js"></script>
	<script src="dist/js/noty.min.js"></script>
	<script src="dist/js/croppie.min.js"></script>
	<script src="dist/js/jquery.modal.min.js"></script>
	<script src="dist/js/menu-slide.js?v<?php echo date('YmdHis') ?>"></script>
	<script src="dist/js/perfil.js?v<?php echo date('YmdHis') ?>"></script>
</body>

</html>
