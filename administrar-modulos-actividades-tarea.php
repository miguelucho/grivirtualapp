<?php
session_start();
if (!isset($_SESSION['griapp_user'])) {
  header('Location: login');
}

require_once "dist/libs/conexion.php";

$nombre = '';
$login = '';
$nombre_actividad = '';
$descripcion = '';
$inicio_fecha = '';
$inicio_hora = '';
$fin_fecha = '';
$fin_hora = '';
$ls_documentos = '';

$usuarios = $db
  ->where('Id_us', $_SESSION['griapp_user'])
  ->objectBuilder()->get('usuarios_app');

if ($db->count > 0) {
  $nombre = $usuarios[0]->nombre_us;
  $login = $usuarios[0]->login_us;
}


if (!isset($_REQUEST['ta']) || !isset($_REQUEST['ta'])) {
  header('Location: administrar-modulos');
}else{
  $detalles = $db
    ->where('Id_mad', $_REQUEST['ta'])
    ->objectBuilder()->get('modulos_actividades_detalle');

  if($db->count > 0){
    $nombre_actividad = $detalles[0]->nombre_mad;

    $tareas = $db
      ->where('Id_mad', $_REQUEST['ta'])
      ->objectBuilder()->get('modulo_actividades_tareas');

    if($db->count > 0){
      $descripcion = $tareas[0]->descripcion_mat;
      $inicio_fecha = $tareas[0]->fecha_inicio_mat;
      $inicio_hora = date('h:i:s A', strtotime($tareas[0]->hora_inicio_mat));
      $fin_fecha = $tareas[0]->fecha_fin_mat;
      $fin_hora = date('h:i:s A', strtotime($tareas[0]->hora_fin_mat));


      $documentos = $db
        ->where('Id_mat', $tareas[0]->Id_mat)
        ->objectBuilder()->get('modulo_actividades_tareas_archivos');

      if($db->count > 0){

        foreach ($documentos as $documento) {
          $ls_documentos .= '<div class="Contenedor-formularios-bloque"><div class="Colum-dos"><div class="input-field"><a href="dist/' . $documento->archivo_mata . '" target="_blank">' . $documento->nombre_mata . '</a></div></div><div class="Colum-dos"><div class="input-field"><a href="javascript:void(0)" class="Btn-table-rojo Btn-bloquear Eliminar-documento" id="Doc-' . $documento->Id_mata . '"><span><i class="icon-blocked"></i>Eliminar</span></a></div></div></div>';
        }

        $ls_documentos .= '<br><br>';
      }
    }

  }else{
    header('Location: administrar-modulos');
  }
}


?>
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Administrador Modulos</title>
  <link rel="stylesheet" type="text/css" href="dist/css/fonts.css">
  <?php include("dist/libs/cssvariable/css-variables.php") ?>
  <link rel="stylesheet" type="text/css" href="dist/css/materialize.css">
  <link rel="stylesheet" type="text/css" href="dist/css/load.css">
  <link rel="stylesheet" type="text/css" href="dist/css/noty.css">
  <link rel="stylesheet" type="text/css" href="dist/css/relax.css">
  <link rel="stylesheet" type="text/css" href="dist/css/croppie.css">
  <link rel="stylesheet" type="text/css" href="dist/css/jquery.modal.css" />
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>

<body>
  <header>
    <div class="Admin-top">
      <?php include("dist/libs/includes-seccion/top-header.php") ?>
    </div>
  </header>
  <section>
    <div class="Contenedor-principal">
      <div class="Contenedor-principal-izq Contenedor-principal-izq-min">
        <?php include("dist/libs/includes-seccion/menu-izq-docentes.php"); ?>
      </div>
      <div class="Contenedor-principal-der">
        <div class="Contenedor-principal-der-int">
          <div class="Contenedor-principal-titulo">
            <div class="Contenedor-principal-titulo-sec">
              <h2 class="Titulo-seccion">Administrar tarea</h2>
              <p>
                <a href="administrar-calificacion-tareas-siguiente?ta=<?php echo $_REQUEST['ta'] ?>&gr=<?php echo $_REQUEST['gr'] ?>&mo=<?php echo $_REQUEST['mo'] ?>" target="_blank" class="Btn-verde-oscuro"><i class="icon-list"> </i> Calificar actividad</a>
              </p>
            </div>
            <div class="Contenedor-principal-titulo-sec">
              <div class="Btn-flotante-crear">
                <a href="administrar-modulos-actividades?gr=<?php echo $_REQUEST['gr'].'&mo='.$_REQUEST['mo'] ?>" data-target="crear" data-position="left" data-tooltip="Volver atras" class="modal-trigger tooltipped btn-floating btn-large waves-effect waves-light blue-grey"><i class="material-icons">keyboard_backspace</i></a>
              </div>
            </div>
          </div>
          <div class="Contenedor-desc">
            <p>Administra el contenido de la tarea.<br><br></p>
            <div class="Contenedor-desc-int">
              <form id="Administra-tarea">
                <div class="Contenedor-formularios-bloque">
                  <div class="Colum-uno">
                    <div class="input-field">
                      <input placeholder="Escriba el nombre del archivo" name="tarea[nombre]" id="Nombre_tarea" value="<?php echo $nombre_actividad; ?>" type="text">
                      <label for="Nombre_tarea">Nombre de la actividad - tarea</label>
                    </div>
                  </div>
                </div>
                <section>
                  <div class="Contenedor-admin-modulo">
                    <div class="Contenedor-editor-texto">
                      <textarea id="descripcion" name="tarea[descripcion]"><?php echo $descripcion; ?></textarea>
                    </div>
                    <p>Ingresa la cantidad de actividades para este modulo.</p>
                    <div class="Contenedor-formularios-bloque">
                      <div class="Colum-cuatro">
                        <div class="input-field">
                          <input type="text" class="datepicker" name="tarea[apertura_fecha]" value="<?php echo $inicio_fecha; ?>" placeholder="Haz clic para ingresar">
                          <label for="fecha_apertura">Fecha de apertura</label>
                        </div>
                      </div>
                      <div class="Colum-cuatro">
                        <div class="input-field">
                          <input type="text" class="timepicker" name="tarea[apertura_hora]" value="<?php echo $inicio_hora; ?>" placeholder="Haz clic para ingresar">
                          <label for="hora_apertura">Hora de apertura</label>
                        </div>
                      </div>
                      <div class="Colum-cuatro">
                        <div class="input-field">
                          <input type="text" class="datepicker" name="tarea[cierre_fecha]" value="<?php echo $fin_fecha; ?>" placeholder="Haz clic para ingresar">
                          <label for="fecha_cierre">Fecha de cierre</label>
                        </div>
                      </div>
                      <div class="Colum-cuatro">
                        <div class="input-field">
                          <input type="text" class="timepicker" name="tarea[cierre_hora]" value="<?php echo $fin_hora; ?>" placeholder="Haz clic para ingresar">
                          <label for="hora_cierre">Hora de cierre</label>
                        </div>
                      </div>
                    </div>
                    <?php echo $ls_documentos; ?>
                    <div class="Contenedor-formularios-bloque">
                      <div class="Colum-tres">
                        <div class="input-field">
                          <input placeholder="Escriba el nombre del archivo" name="tarea[archivo_nombre][]" id="Nombre_Archivo" type="text" class="validate">
                          <label for="Nombre_Archivo">Nombre del archivo</label>
                        </div>
                      </div>
                      <div class="Colum-cuatro">
                        <div class="file-field input-field">
                          <div class="btn blue darken-3">
                            <span>Archivo</span>
                            <input type="file">
                          </div>
                          <div class="file-path-wrapper">
                            <input class="file-path validate" type="text">
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="Temp-archivos"></div>

                    <div class="Contenedor-formularios-bloque">
                      <div class="Colum-cuatro">
                        <a href="javascript:void(0)" data-position="right" data-tooltip="Archivo adicional" class="Agregar-archivo tooltipped btn-floating btn-large waves-effect waves-light blue-grey darken-4"><i class="material-icons">add</i></a>
                      </div>
                    </div>

                    <div class="Contenedor-admin-modulo">
                      <div class="Contenedor-editor-texto">
                        <br>
                        <p>Verifica la información ingresada, luego de hacer clic en guadar.</p>
                      </div>
                      <div class="Contenedor-editor-texto">
                        <div class="Colum-uno">
                          <input type="hidden" name="tarea[idtarea]" value="<?php echo $_REQUEST['ta']; ?>">
                          <input type="submit" class="Btn Btn-dark Bold-ro Btn-expand" value="Guardar">
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <script src="dist/js/jquery-1.11.1.min.js"></script>
  <script src="dist/js/noty.min.js"></script>
  <script src="dist/js/inicializar.js"></script>
  <script src="dist/js/materialize.min.js"></script>
  <script src="dist/js/noty.min.js"></script>
  <script src="dist/js/croppie.min.js"></script>
  <script src="dist/js/jquery.modal.min.js"></script>
  <script src="dist/js/ckeditor/ckeditor.js"></script>
  <script src="dist/js/menu-slide.js?v<?php echo date('YmdHis') ?>"></script>
  <script src="dist/js/modulos-tareas-administrar.js?v<?php echo date('YmdHis') ?>"></script>
</body>

</html>
