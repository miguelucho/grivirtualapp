<?php
session_start();
if (!isset($_SESSION['griapp_user'])) {
  header('Location: login');
}

require_once "dist/libs/conexion.php";

$nombre = '';
$login = '';
$nm_programa = '';
$nm_curso = '';
$codigo = '';
$dsc_modulo = '';
$ls_cortes = '';
$actividades_creadas = 'data-existente="0"';


$usuarios = $db
  ->where('Id_us', $_SESSION['griapp_user'])
  ->objectBuilder()->get('usuarios_app');

if ($db->count > 0) {
  $nombre = $usuarios[0]->nombre_us;
  $login = $usuarios[0]->login_us;
}

if (!isset($_REQUEST['gr']) || !isset($_REQUEST['mo'])) {
  header('Location: administrar-modulos');
}else{
  $grupos = $db
    ->where('Id_gr', $_REQUEST['gr'])
    ->objectBuilder()->get('grupos');

  if($db->count == 0){
    header('Location: administrar-modulos');
  }else{
    $programas = $db
      ->where('Id_pr', $grupos[0]->Id_pr)
      ->objectBuilder()->get('programas');

    if ($db->count > 0) {
        $nm_programa = $programas[0]->nombre_pr;
    }

    $cursos = $db
      ->where('Id_cu', $grupos[0]->Id_cu)
      ->objectBuilder()->get('cursos');

    if ($db->count > 0) {
        $nm_curso = $cursos[0]->nombre_cu;
    }

    $codigo = $grupos[0]->codigo_gr;

  }
}
?>
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Administrador Modulos</title>
  <link rel="stylesheet" type="text/css" href="dist/css/fonts.css">
  <?php include("dist/libs/cssvariable/css-variables.php") ?>
  <link rel="stylesheet" type="text/css" href="dist/css/materialize.css">
  <link rel="stylesheet" type="text/css" href="dist/css/load.css">
  <link rel="stylesheet" type="text/css" href="dist/css/noty.css">
  <link rel="stylesheet" type="text/css" href="dist/css/relax.css">
  <link rel="stylesheet" type="text/css" href="dist/css/croppie.css">
  <link rel="stylesheet" type="text/css" href="dist/css/jquery.modal.css" />
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>

<body>
  <header>
    <div class="Admin-top">
      <?php include("dist/libs/includes-seccion/top-header.php") ?>
    </div>
  </header>
  <section>
    <div class="Contenedor-principal">
      <div class="Contenedor-principal-izq Contenedor-principal-izq-min">
        <?php include("dist/libs/includes-seccion/menu-izq-docentes.php"); ?>
      </div>
      <div class="Contenedor-principal-der">
        <div class="Contenedor-principal-der-int">
          <div class="Contenedor-principal-titulo">
            <div class="Contenedor-principal-titulo-sec">
              <h2 class="Titulo-seccion">Contenido Curso: <?php echo $nm_programa; ?><br><?php echo $nm_curso; ?> - <?php echo $codigo; ?></h2>
            </div>
            <div class="Contenedor-principal-titulo-sec">
              <div class="Btn-flotante-crear">
                <a href="administrar-modulos-interno?gr=<?php echo $_REQUEST['gr']; ?>" data-target="crear" data-position="left" data-tooltip="Volver atras" class="modal-trigger tooltipped btn-floating btn-large waves-effect waves-light blue-grey"><i class="material-icons">keyboard_backspace</i></a>
              </div>
            </div>
          </div>
          <div class="Contenedor-desc">
            <p></p>
            <div class="Contenedor-desc-int">
              <section>
                <div class="Contenedor-admin-modulo">
                  <p>Ingrese a continuación la descripción:</p>
                  <form id="Nuevo-contenido">
                    <div class="Contenedor-editor-texto">
                      <textarea id="descripcion-modulo" name="actividad[descripcion]"></textarea>
                    </div>
                    <div class="Contenedor-admin-modulo">
                      <div class="Contenedor-editor-texto">
                        <br>
                      </div>
                      <div class="Contenedor-editor-texto">
                        <div class="Colum-uno">
                          <input type="hidden" name="actividad[grupo]" value="<?php echo $_REQUEST['gr']; ?>" class="Grupo-actividad">
                          <input type="hidden" name="actividad[modulo]" value="<?php echo $_REQUEST['mo']; ?>">
                          <input type="submit" class="Btn Btn-dark Bold-ro Btn-expand" value="Guardar">
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </section>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <script src="dist/js/jquery-1.11.1.min.js"></script>
  <script src="dist/js/noty.min.js"></script>
  <script src="dist/js/inicializar.js"></script>
  <script src="dist/js/materialize.min.js"></script>
  <script src="dist/js/noty.min.js"></script>
  <script src="dist/js/ckeditor/ckeditor.js"></script>
  <script src="dist/js/menu-slide.js?v<?php echo date('YmdHis') ?>"></script>
  <script src="dist/js/modulos-contenido.js?v<?php echo date('YmdHis') ?>"></script>
</body>

</html>
