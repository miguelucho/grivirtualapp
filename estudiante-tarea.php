<?php
session_start();
if (!isset($_SESSION['griapp_user'])) {
  header('Location: login');
}

require_once "dist/libs/conexion.php";

$curso_nombre = '';
$foro_nombre = '';
$foro_descripcion = '';
$curso_nombre = '';
$nombre_docente = '';

$tareas = $db
  ->where('Id_mad', $_REQUEST['tr'])
  ->objectBuilder()->get('modulo_actividades_tareas');

if ($db->count > 0) {
  $actividades_detalles = $db
    ->where('Id_mad', $_REQUEST['tr'])
    ->objectBuilder()->get('modulos_actividades_detalle');

  $notificaciones = $db
    ->where('Id_us', $_SESSION['griapp_user'])
    ->where('tipoid_ea', $_REQUEST['tr'])
    ->objectBuilder()->get('usuarios_notificaciones');

  if ($db->count == 0) {
    $dats = [
      'Id_us' => $_SESSION['griapp_user'],
      'tipo_ea' => 'tarea',
      'tipoid_ea' => $_REQUEST['tr'],
      'visto_ea' => 1
    ];

    $db
      ->insert('usuarios_notificaciones', $dats);
  }

  $contenido_respuesta = '';
  $archivo_respuesta = '';

  $calificaciones = $db
    ->where('Id_ma', $_SESSION['griapp_IDtipo'])
    ->where('Id_mad', $_REQUEST['tr'])
    ->objectBuilder()->get('modulo_actividades_tareas_calificaciones');

  if ($db->count > 0) {
    $contenido_respuesta = $calificaciones[0]->descripcion_matc;
    if($calificaciones[0]->archivo_matc != ''){
      $archivo_respuesta = '<p><a href="dist/' . $calificaciones[0]->archivo_matc . '"ac target="_blank">Ver Archivo Adjunto</a></p>';
    }
  }


  $foro_nombre = $actividades_detalles[0]->nombre_mad;

  $foro_descripcion = $tareas[0]->descripcion_mat;

  $ver_archivo = '';

  $archivos = $db
    ->where('Id_mat',  $tareas[0]->Id_mat)
    ->objectBuilder()->get('modulo_actividades_tareas_archivos');

  if ($db->count > 0) {
    $ver_archivo = '<p><a href="dist/' . $archivos[0]->archivo_mata . '" target="_blank">Ver Archivo Adjunto</a></p>';
  }

  $matriculas = $db
    ->where('Id_ma',  $actividades_detalles[0]->Id_ma)
    ->objectBuilder()->get('matriculas');

  if ($db->count > 0) {
    $programas = $db
      ->where('Id_pr', $matriculas[0]->programa_ma)
      ->objectBuilder()->get('programas');

    if ($db->count > 0) {
      $detalles_matricula = $db
        ->where('Id_ma', $matriculas[0]->Id_ma)
        ->objectBuilder()->get('matriculas_detalles');

      if ($db->count > 0) {
        $grupos = $db
          ->where('Id_gr', $detalles_matricula[0]->grupo_md)
          ->objectBuilder()->get('grupos');

        if ($db->count > 0) {
          $docentes = $db
            ->where('Id_do', $grupos[0]->Id_do)
            ->objectBuilder()->get('docentes');

          if ($db->count > 0) {
            $nombre_docente = $docentes[0]->nombre_do . ' ' . $docentes[0]->apellido_do;
          }
        }

        $cursos = $db
          ->where('Id_cu', $detalles_matricula[0]->curso_md)
          ->objectBuilder()->get('cursos');

        if ($db->count > 0) {
          $curso_nombre = $cursos[0]->nombre_cu;
        }
      }
    }
  }
}
?>
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Dashboard Estudiante</title>
  <link rel="stylesheet" type="text/css" href="dist/css/fonts.css">
  <?php include("dist/libs/cssvariable/css-variables.php") ?>
  <link rel="stylesheet" type="text/css" href="dist/css/materialize.css">
  <link rel="stylesheet" type="text/css" href="dist/css/load.css">
  <link rel="stylesheet" type="text/css" href="dist/css/noty.css">
  <link rel="stylesheet" type="text/css" href="dist/css/relax.css">
  <link rel="stylesheet" type="text/css" href="dist/css/croppie.css">
  <link rel="stylesheet" type="text/css" href="dist/css/jquery.modal.css" />
  <link rel="stylesheet" type="text/css" href="dist/css/paginacion.css">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>

<body>
  <header>
    <div class="Admin-top">
      <?php include("dist/libs/includes-seccion/top-header.php") ?>
    </div>
  </header>
  <section>
    <div class="Contenedor-principal">
      <div class="Contenedor-principal-izq Contenedor-principal-izq-min">
        <?php include("dist/libs/includes-seccion/menu-izq-estudiantes.php"); ?>
      </div>
      <div class="Contenedor-principal-der">
        <div class="Contenedor-principal-der-int">

          <div class="Contenedor-dash">
            <div class="Contenedor-principal-titulo">
              <div class="Contenedor-principal-titulo-sec">
                <h2 class="Titulo-seccion"><?php echo $foro_nombre ?></h2>
                <h3 class="Titulo-seccion"><?php echo $curso_nombre ?></h3><br>
              </div>
            </div>
            <div class="Contenedor-dash-int">
              <div class="Contenedor-dash-int-contenido">
                <div class="Contenedor-inf-docente">
                  <div class="Contenedor-inf-docente-int">
                    <div class="Contenedor-bloque-docente">
                      <div class="Contenedor-bloque-imagen">
                        <img src="dist/assets/images/usuarios/demo.jpg" alt="">
                      </div>
                      <figcaption><?php echo $nombre_docente ?></figcaption>
                      <div class="Contenedor-bloque-texto">
                        <?php echo $foro_descripcion ?>
                      </div>
                      <?php echo $ver_archivo ?>
                    </div>
                  </div>
                </div>
                <div class="Contenedor-repuestaforo">
                  <div class="Contenedor-repuestaforo-int">
                    <div class="Contenedor-repuestaforo-editor">
                      <form id="Tarea-comentar">
                        <textarea name="estudiante[descripcion]" id="descripcion"><?php echo $contenido_respuesta ?></textarea>
                        <input type="hidden" name="estudiante[tarea]" value="<?php echo $_REQUEST['tr'] ?>">
                        <br>
                        <?php echo  $archivo_respuesta ?>
                        <div class="Colum-cuatro">
                          <p>Adjunto:</p>
                          <div class="file-field input-field">
                            <div class="btn blue darken-3">
                              <span>Archivo</span>
                              <input type="file">
                            </div>
                            <div class="file-path-wrapper">
                              <input class="file-path validate" type="text">
                            </div>
                          </div>
                        </div>
                        <div class="Contenedor-repuestaforo-editor-btn">
                          <input type="submit" class="Btn Btn-azul Bold-ro Btn-expand" value="Completar">
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <script src="dist/js/jquery-1.11.1.min.js"></script>
  <script src="dist/js/noty.min.js"></script>
  <script src="dist/js/inicializar.js"></script>
  <script src="dist/js/materialize.min.js"></script>
  <script src="dist/js/noty.min.js"></script>
  <script src="dist/js/croppie.min.js"></script>
  <script src="dist/js/jquery.modal.min.js"></script>
  <script src="dist/js/ckeditor/ckeditor.js"></script>
  <script src="dist/js/menu-slide.js?v<?php echo date('YmdHis') ?>"></script>
  <script src="dist/js/estudiante-tarea.js?v<?php echo date('YmdHis') ?>"></script>
</body>

</html>
