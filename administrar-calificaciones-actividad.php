<?php
session_start();
if (!isset($_SESSION['griapp_user'])) {
  header('Location: login');
}

require_once "dist/libs/conexion.php";

?>
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Administrador de Calificaciones</title>
  <link rel="stylesheet" type="text/css" href="dist/css/fonts.css">
  <?php include("dist/libs/cssvariable/css-variables.php") ?>
  <link rel="stylesheet" type="text/css" href="dist/css/materialize.css">
  <link rel="stylesheet" type="text/css" href="dist/css/load.css">
  <link rel="stylesheet" type="text/css" href="dist/css/noty.css">
  <link rel="stylesheet" type="text/css" href="dist/css/relax.css">
  <link rel="stylesheet" type="text/css" href="dist/css/croppie.css">
  <link rel="stylesheet" type="text/css" href="dist/css/paginacion.css">
  <link rel="stylesheet" type="text/css" href="dist/css/jquery.modal.css" />
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>

<body>
  <header>
    <div class="Admin-top">
      <?php include("dist/libs/includes-seccion/top-header.php") ?>
    </div>
  </header>
  <section>
    <div class="Contenedor-principal">
      <div class="Contenedor-principal-izq Contenedor-principal-izq-min">
        <?php include("dist/libs/includes-seccion/menu-izq-docentes.php"); ?>
      </div>
      <div class="Contenedor-principal-der">
        <div class="Contenedor-principal-der-int">
          <div class="Contenedor-principal-titulo">
            <div class="Contenedor-principal-titulo-sec">
              <h2 class="Titulo-seccion">Calificaciones por actividad</h2>
              <p><strong>Grupo:</strong> 2-1-1</p>
              <p><strong>Modulo:</strong>  UNIDAD TEMATICA Nº 1: Reconocer e interpretar la norma técnica</p>
            </div>

           <div class="Contenedor-principal-titulo-sec">
              <div class="Btn-flotante-crear">
                <a href="#" data-target="crear" data-position="left" data-tooltip="Volver atras" class="modal-trigger tooltipped btn-floating btn-large waves-effect waves-light blue-grey"><i class="material-icons">keyboard_backspace</i></a>
              </div>
            </div>
            </div>
          <div class="Contenedor-desc">
            <p>Administra las calificaciones por actividad según el grupo.</p><br>
            <div class="Contenedor-filtros">
              <form id="busqueda">
                <div class="Contenedor-filtros-int">
                  <div class="Colum-cuatro">
                    <div class="input-field">
                      <input id="Actividad-bus" type="text">
                      <label for="Actividad-bus">Nombre de la actividad</label>
                    </div>
                  </div>
                  <div class="Colum-cuatro">
                    <div class="input-field">
                      <input id="Estudiante-bus" type="text">
                      <label for="Estudiante-bus">Nombre del estudiante</label>
                    </div>
                  </div>
                  <div class="Colum-cuatro">
                    <div class="input-field">
                      <input type="submit" class="Btn Btn-azul Bold-ro Btn-expand" value="Buscar">
                    </div>
                  </div>
                </div>
              </form>
            </div>
            <div class="Contenedor-desc-int">
              <section>
                <table class="striped Table-virtual">
                  <thead>
                    <tr>
                      <th>Actividad / Tipo</th>
                      <th>Estudiante</th>
                      <th class="center">Nota</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody id="">
                    <tr>
                      <td>
                        <span><strong>Tarea</strong></span><br>
                        <span>aca va el nombre de la tarea</span>
                      </td>
                      <td class="">Fernando Hernandez Martinez</td>
                      <td class="center">0</td>
                      <td><a href="administrar-modulos-interno" data-target="editar" class="Btn-ver Btn-table-verde"><i class="icon-list"></i>Calificar</a></td>
                    </tr>
                    <tr>
                      <td>
                        <span><strong>Cuestionario</strong></span><br>
                        <span>Aca va el nombre de la tarea</span>
                      </td>
                      <td class="">Fernando Hernandez Martinez</td>
                      <td class="center">0</td>
                      <td><a href="administrar-modulos-interno" data-target="editar" class="Btn-ver Btn-table-verde"><i class="icon-list"></i>Calificar</a></td>
                    </tr>
                  </tbody>
                </table>
                <div class="listado-loader" style="text-align: center; margin-top: 50px;">
                    <div class="preloader-wrapper big active">
                        <div class="spinner-layer">
                            <div class="circle-clipper left">
                                <div class="circle"></div>
                            </div>
                            <div class="gap-patch">
                                <div class="circle"></div>
                            </div>
                            <div class="circle-clipper right">
                                <div class="circle"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="listado-paginacion">
                    <ul class="pagination">
                    </ul>
                </div>
              </section>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div style="display: none" id="cursos-oculto">
      <select>
          <?php echo $ls_cursos; ?>
      </select>
    </div>
  </section>

  <section>
    <!-- se agrega esta clase Tablero-emergente-cap -->
    <div class="Tablero-emergente ">
            <div class="Tablero-emergente-int">
                <div class="Tablero-top">
                    <div class="Tablero-top-btn">
                        <a class="btn-floating btn-large waves-effect waves-light grey darken-3"><i class="material-icons">clear</i></a>
                    </div>
                </div>
                <section>
                    <div class="Contenedor-principal">
                      <div class="Contenedor-califica-rapida">
                        <div class="Contenedor-principal-der-int">
                          <div class="Contenedor-principal-titulo">
                            <div class="Contenedor-principal-titulo-sec">
                              <h2 class="Titulo-seccion">Calificar tarea</h2><br>
                              <h6 class="Cont-nombre-estudiante">Nombre del estudiante</h6>
                            </div>
                            <div class="Contenedor-principal-titulo-sec">

                            </div>
                          </div>
                          <div class="Contenedor-desc">

                            <div class="Contenedor-desc-int">
                              <form id="Administra-tarea">
                                <div class="Contenedor-formularios-bloque">
                                  <div class="Colum-uno">
                                    <div class="input-field">
                                      <div class="Titulo-tarea"><br><p><strong>Aca va el nombre de la tarea</strong> </p></div>
                                      <label for="Nombre_tarea">Nombre de la actividad - tarea</label>
                                    </div>
                                  </div>
                                </div>
                                <section>
                                  <div class="Contenedor-admin-modulo">
                                    <div class="Contenedor-texto-pregunta">
                                      <h6>Pregunta:</h6>
                                      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                    </div>
                                    <div class="Contenedor-texto-tarea">
                                      <h6>Respuesta</h6>
                                      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                      <p><strong>Ultima actualización:</strong> 12 - Sep - 2020</p>
                                    </div>
                                    <p>A continuación archivos adjuntos por el estudiente.</p>
                                    <div class="Contenedor-formularios-bloque">
                                      <div class="Colum-uno">
                                        <ul class="Contenedor-archivos-lista">
                                          <li><a href="#" target="_blank"><i class="icon-attachment"></i> Nombre del archivo adjunto</a></li>
                                          <li><a href="#" target="_blank"><i class="icon-attachment"></i> Nombre del archivo adjunto</a></li>
                                        </ul>
                                      </div>
                                    </div>
                                    <div class="Contenedor-formularios-bloque">
                                      <div class="Colum-cuatro">
                                        <div class="input-field">
                                          <input type="number" name="nota[calificacion]" value="" placeholder="Ingresa la nota" class="validate" required="">
                                          <label for="hora_cierre">Nota para la actividad </label>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="Contenedor-formularios-bloque">
                                      <div class="Colum-uno">
                                        <div class="input-field">
                                          <textarea id="textarea1" class="materialize-textarea"></textarea>
                                          <label for="textarea1">Observaciones (opcional)</label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="Contenedor-admin-modulo">
                                      <div class="Contenedor-editor-texto">
                                        <br>
                                        <p>Verifica la información ingresada, luego de hacer clic en guardar.</p>
                                      </div>
                                      <div class="Contenedor-formularios-bloque">
                                        <div class="Colum-cuatro">
                                          <input type="hidden" name="tarea[idtarea]" value="<?php echo $_REQUEST['ta']; ?>">
                                          <input type="submit" class="Btn Btn-dark Bold-ro Btn-expand" value="Guardar nota">
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </section>
                              </form>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </section>
            </div>
      </div>
  </section>
  <script src="dist/js/jquery-1.11.1.min.js"></script>
  <script src="dist/js/noty.min.js"></script>
  <script src="dist/js/inicializar.js"></script>
  <script src="dist/js/materialize.min.js"></script>
  <script src="dist/js/noty.min.js"></script>
  <script src="dist/js/croppie.min.js"></script>
  <script src="dist/js/jquery.modal.min.js"></script>
  <script src="dist/js/menu-slide.js?v<?php echo date('YmdHis') ?>"></script>
  <script src="dist/js/asignados-lista.js?v<?php echo date('YmdHis') ?>"></script>
</body>

</html>
